.. index::
   pair: Grèves; Créativité
   pair: Grèves; Appel à la grevilla

.. _grevilla_2023_04_20:

=======================================
2023-04-20 **Appel à la grevilla**
=======================================

- https://solidairesinformatique.org/2023/04/20/appel-a-la-grevilla/


Solidaires Informatique appelle à la grève les travailleurs·euses du
numérique, des bureaux d’études et du jeu vidéo, jusqu’au retrait de la
réforme des retraites dictée par le gouvernement Borne, sous la volonté
de Macron.

Cet appel à la grève commence dès aujourd’hui (2023-04-20) et vaut
jusqu’au 31 décembre 2027.

**Le gouvernement s’oppose à la démocratie sociale, nous forçant à adopter
des stratégies syndicales innovantes, des stratégies de rupture**.

Cet appel n’est pas en soit un appel à une grève reconductible sur plusieurs
années, mais vise à soutenir les grévistes isolé·es ou les salarié·es
militants·es solitaires dans leurs entreprises.

Il s’appuie sur ces principes garantis par le Code du Travail :

- il n’y a pas de durée minimale pour la grève
- il n’y a aucune obligation à prévenir l’entreprise en avance
- c’est aux services des Ressources Humaines de tenir à jour les temps de grève
- il n’y a aucune sanction à la grève, autre que la réduction du salaire
- l’entreprise ne peut pas vous empêcher de faire grève

Par cet appel, nous voulons ouvrir de nouvelles perspectives aux employés·es,
tant défensives qu’offensives :

- Vous avez besoin d’une excuse pour une absence à raison personnelle ?
  Vous êtes en grève.
- L’entreprise veut vous sanctionner pour un retard ?
  Elle ne savait pas, mais vous étiez en grève, cette sanction est inapplicable.
- Il vous manque un jour de congés pour vos vacances ?
  C’est un jour de grève.
- Besoin d’une pause plus longue, de quitter le travail plus tôt ?
  Une grève s’impose.
- Vous avez une présentation importante à un client ? un voyage d’affaire ?
  Manque de chances pour l’entreprise, c’est jour de grève le jour du départ.
  Ou, plus offensif, vous étiez en grève au moment du départ de l’avion.
- Vous voulez éviter des réunions pénibles ;
  vous êtes en grève à ce moment là.
- Vous n’avez pas communiqué une information importante à temps ?
  Vous étiez en grève au moment où elle devait être communiquée.
- Les délais de traitement de vos tickets urgents ont été dépassés ?
  Vous étiez en grève tout le temps de la SLA.
- Vous voulez bloquer la production ?
  Il suffit de travailler par intermittence, 1h sur 2.
  Inemployable, mais avec toujours une partie de salaire.
  Si c’est l’équipe entière qui grève en alternance, la production est
  ralentie d’autant.
- L’employeur vous reproche une insuffisance professionnelle, de ne pas
  avoir atteint vos objectifs à temps ?
  A-t-il seulement les preuves que vous n’étiez pas en grève, ce qui
  expliquerait vos retards ?

  - À ce titre, vous pourriez même l’attaquer en lui reprochant de faire
    de la discrimination anti-gréviste.

Le télétravail, largement répandu dans notre secteur, renforce ces mécanismes.
Une sieste qui s’allonge un peu ? Vous êtes en grève.

Les patrons usent et abusent des constats d’huissiers pour attaquer les
mouvements de grève ; le télétravail est un obstacle redoutable contre
ces pratiques.

Le numérique est un secteur pivot dans notre monde moderne, qui sous une
apparence robuste requiert une armée de travailleur·euses fortement
coordonné·es pour ne pas pourrir sur pied.

Il faut éclater les cadres traditionnels.

Au lieu de se dissoudre, l’Exécutif national change la nature même du
rapport de force.

Aujourd’hui nous sommes porteur·euses de l’intérêt général.

**Cela doit alors nous pousser à être inventifs·es.**

BORDELISONS LE TRAVAIL OUVRONS NOS IMAGINAIRES DE LUTTES SYNDICALES
FAISONS LA SOCIETE QUE NOUS SOMMES IMPOSONS UN AUTRE AVENIR

