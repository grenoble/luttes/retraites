

.. _100jours_de_zbeul_jour_11:

==========================================================================================================================================================================
2023-04-28 **On ne les lâche pas ! La carte des mobilisations (jour 011)**
==========================================================================================================================================================================

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations


- Istres : Sébastien Lecornu, Ministre des Armées, sera en visite à la
  base aérienne 125 d’Istres à partir de 16h20 le vendredi 28 avril.
- Morcenx-la-nouvelle : Geneviève Darrieussecq, Ministre déléguée auprès
  du ministre des Solidarités, de l’Autonomie et des Personnes handicapées,
  chargée des Personnes handicapé, sera a la cérémonie de remise du prix
  Meilleurs ouvriers de France en charpente le 28/04 à Morcenx-la-nouvelle,
  dans les Landes, à partir de 12h00.
- Metz : François Braun sera présent vendredi à la préfecture de Metz
  en Moselle. Le comité de non-accueil est déjà prévu.
- Montmirail : François Braun, ministre de la Santé et de la Prévention,
  se rendra ce vendredi 28 avril 2023 au sein de l’entreprise de câblage
  Axon’Câble, dont le siège international se situe à Montmirail, dans
  le sud-ouest de la Marne.
- Un comité de non-accueil est organisé, à vos casseroles !
- Vesoul : Christophe Béchu, ministre de la transition écologique, attendu
  à Vesoul pour l’assemblée générale de l’association des maires de France du département.
- Gap : Marc Fesneau sera à Gap le vendredi 28 avril au salon de l’élevage.
- La Réunion : Visite de Dominique Faure, secrétaire d’État en charge
  de la Ruralité au ministère de la Transition écologique, prévue vendredi 28 avril à La Réunion.
- Haute-Savoie : Isabelle Lonvis-Rome, ministre déléguée chargée de l’égalité
  entre les femmes et les hommes, de la diversité et de l’égalité des chances,
  poursuit sa visite en Haute-Savoie, elle sera à 9h00 à Thonon-les-Bains
  (au Théâtre de la Maison des Arts du Léman) et à 11h15 à Evian-les-Bains
  à l’association Women Safe, 20 rue de la Touvière, 74500 Evian-les-Bains.



