.. index::
   pair: Zbeul; Stade de France (2023-04-29)

.. _100jours_de_zbeul_jour_12_2023_04_29:

==========================================================================================================================================================================
2023-04-29 |jo| **Stade de France à Saint-Denis : à 20h au Stade de France, finale de la Coupe de France de football (jour 012)**
==========================================================================================================================================================================

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations


Stade de France à Saint-Denis : à 20h au Stade de France, finale de la
Coupe de France de football.

Emmanuel Macron sera au Stade de France pour assister à cette finale.

Un appel aux supporters est lancé. L’intersyndicale régionale propose une
distribution d’un « carton rouge » et de sifflets au public pour signifier
la volonté populaire du retrait de la loi retraites.

RDV dès 17h aux abords des sorties des RER B, D et ligne 13 du métro du
Stade de France.

Besoin de monde pour distribuer les matériels !


.. figure:: images/saint_denis.jpeg
   :align: center

   https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations

   Pour proposer des nouveaux rassemblements à ajouter à la carte, vous
   pouvez nous contacter sur l’email : carte@attac.org


Articles
==========

Mediapart
------------

- https://www.mediapart.fr/journal/france/290423/pas-de-sifflets-pas-de-cartons-pas-d-images-le-stade-potemkine-d-emmanuel-macron

Pas de sifflets, pas de cartons, pas d’images : le stade Potemkine d’Emmanuel Macron

Le président de la République foule aux pieds les libertés, mais plus la
pelouse du Stade de France.
Alors que la préfecture puis les stadiers ont tenté d’empêcher les manifestations
d’hostilité, il a renoncé samedi soir à saluer les joueurs sur le terrain
lors de la finale de la Coupe de France, par crainte des huées et de la colère du public.

DesDes poignées de main furtives dans un couloir du Stade de France.

Un protocole organisé en catimini, pour éviter au président de la République
d’avoir à pénétrer sur la pelouse. La finale de la Coupe de France de
football aura donné à voir, en plus de la large victoire de Toulouse
contre Nantes (5-1), l’étendue de l’impopularité d’Emmanuel Macron.

Les cartons rouges confisqués par la sécurité
++++++++++++++++++++++++++++++++++++++++++++++++=

L’énergie répressive des pouvoirs publics ne s’est cependant pas arrêtée là.

Les agents de sécurité du Stade de France ont très vite confisqué les
sifflets syndicaux, une mesure compréhensible au vu des risques d’interférence
avec l’arbitrage de la rencontre, et – plus surprenant – les cartons
rouges que se préparait à brandir une partie du public.

« Ça montre bien que Macron entend la colère des Français mais qu’il
s’en fout, peste Boukary, un supporter nantais de 32 ans, fonctionnaire
territorial en région parisienne. Une nouvelle fois, il se protège.
C’est un retour à la monarchie. »



