
.. _100jours_de_zbeul_jour_7:

==========================================================================================================================================================================
2023-04-24 **100 jours de zbeul  (jour 007)**
==========================================================================================================================================================================

- https://100joursdezbeul.fr/
- https://38.demosphere.net/rv/1117


|jo| Casserolade Générale (appel Attac)
===========================================

Source: https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/casserolade-generale-du-24-avril-la-carte-des-rassemblements

A 20h devant les mairies de vos villages (sauf à Grenoble : place notre dame à 19h)

Appel à taper sur des casseroles

pensez à prévenir de votre rassemblement à carte@attac.org pour que les
articles de journaux se rendent compte du nombre de rassemblements

420 lieux la semaine dernière


.. _zbeul_grenoble_007_2023_04_24:

2023-04-24 Grenoble , réactions du régime illibérale, #Grenoble aussi à droit à son ouverture d'enquête pour avoir brulé un mannequin à l'effigie de Macron
===============================================================================================================================================================

- https://www.huffingtonpost.fr/justice/article/a-grenoble-un-mannequin-a-l-effigie-de-macron-incendie-une-enquete-ouverte_217085.html?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter#Echobox=1682433295


Ça y est, #Grenoble aussi à droit à son ouverture d'enquête pour avoir
brulé un mannequin à l'effigie de Macron:

#100joursdezbeul
