.. index::
   pair: Zbeul; Annecy (2023-04-27)

.. _100jours_de_zbeul_jour_10:

==========================================================================================================================================================================
2023-04-27 |jo| **Casserolades : le mouvement social joue à « Intervilles », l’exécutif perd l’épreuve de l’apaisement (jour 010)**
==========================================================================================================================================================================


- https://www.mediapart.fr/journal/france/270423/casserolades-le-mouvement-social-joue-intervilles-l-executif-perd-l-epreuve-de-l-apaisement

Depuis la promulgation de la réforme des retraites, le mouvement social
se mue en poil à gratter de l’exécutif, poursuivant partout le chef de
l’État et les ministres à coups de concert de casseroles, de burlesque
et de parodie.

Les opposants se challengent et irritent au plus haut point le pouvoir.


Un site internet `« 100 jours de zbeul » <https://100joursdezbeul.fr/>`_ (désordre) compte les points
« des départements qui zbeulent le plus ».

Le chahut vaut un point, l’annulation d’une visite en vaut cinq.

Un coefficient par personnalité est appliqué : les points sont multipliés
par six pour le président, par trois pour un·e ministre.

Un pastiche de logo des Jeux olympiques orne la une du site : des casseroles
y remplacent les anneaux.

100 jours de Zbeul a été imaginé par le syndicat Solidaires Informatique,
lui-même inspiré par la carte de France des mobilisations d’Attac, lancée
le jour de l’allocution d’Emmanuel Macron.

« On s’est dit que ce serait sympa de faire un classement », raconte
:ref:`Cédric Rossi, informaticien et syndiqué chez Solidaires Informatique <100jours_zbeul>`

L’idée germe le vendredi 21 avril, le site est en :ref:`ligne dès le lendemain <creation_100jours>`

Son succès est rapide. Le 25 avril, il est numéro un des « tendances Twitter »
et enregistre 160 000 vues et 53 000 visiteurs uniques.

« Plein de gens jouent sur la compète entre villes et je trouve ça génial !,
s’amuse Cédric Rossi.
Certains prennent ça très au sérieux et font des réclamations, pour récupérer
plus de points. »
À l’heure où sont rédigées ces lignes, l’Hérault est largement en tête,
avec 105 points, devant la Seine-Saint-Denis, 48 points.

Pour Cédric Rossi, la mobilisation a pris un nouveau tour, tournant en
dérision le « ridicule du gouvernement ».
L’informaticien y voit un « un durcissement créatif » du mouvement social.


2023-04-27 « On est là et pas vaincus » : ce que disent les casseroles et les mobilisations festives
=========================================================================================================


- https://www.mediapart.fr/journal/france/270423/est-la-et-pas-vaincus-ce-que-disent-les-casseroles-et-les-mobilisations-festives

Le mouvement social ne cesse de se réinventer.

Après les grèves et les manifestations d’ampleur, place aux « casserolades »
pour couvrir la voix de l’exécutif, qui n’apprécie guère.

Entretien avec Danielle Tartakowsky, historienne spécialiste des mouvements
sociaux, qui y voit la profondeur de l’ancrage du mouvement et sa dimension populaire.


.. _annecy_2023_04_27:

2023-04-27 **Une ministre accueillie par un concert de casseroles à Annecy**
================================================================================

- https://librinfo74.fr/une-ministre-accueillie-par-un-concert-de-casseroles-a-annecy/

Un diaporama pour découvrir l’ambiance ce jeudi matin 27 avril devant la
Préfecture d’Annecy pour accueillir la ministre Isabelle LONVIS-ROME,
chargée de l’Égalité entre les femmes et les hommes, de la Diversité et
de l’Égalité des chances.
