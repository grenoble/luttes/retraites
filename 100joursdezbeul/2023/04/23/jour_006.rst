.. index::
   pair: Les casseroles de Foutriquet; Jérôme Soldeville


.. _100jours_de_zbeul_jour_006:

==========================================================================================================================================================================
2023-04-23 |jo| **Les casseroles de Foutriquet (jour 006)** par Jérôme Soldeville
==========================================================================================================================================================================

- https://blogs.mediapart.fr/jerome-soldeville/blog/230423/les-casseroles-de-foutriquet


Historique
===========

À l'occasion de la crise de régime générée par la contre-réforme des
retraites, fait inédit, un préfet de la République a fait interdire le
port de casseroles lors d'une manifestation visant le chef de l'État.

C'est le moment de rappeler au gouvernement macroniste que le charivari
est une forme de contestation très républicaine, qui est née sous la
monarchie de Juillet.


En effet, au début de la monarchie de Juillet (1830), après l'abdication
du roi légitimiste Charles X et l'arrivée au pouvoir de Louis-Philippe,
des concerts de casseroles eurent lieu l'ensemble du territoire, visant
des députés proches du gouvernement, ainsi que des représentants de l'Etat.

Ainsi, une campagne nationale de charivaris eut lieu pendant l’année 1832,
notamment à Grenoble, héritière des traditions révolutionnaires.

À partir de 1831, le charivari était devenu un instrument de résistance
politique pour les militants républicains, après la formation du ministère
du banquier grenoblois Casimir Perier le 8 mars et l'élection d'une nouvelle
Chambre des députés le 5 juillet, ce qui signa la fin des espoirs des
démocrates.

Dans le contexte révolutionnaire de l'époque, les casserolades politiques
coïncidaient avec une revendication aiguë de la souveraineté populaire
et un rejet massif du dévoiement de la représentation politique, puisque
beaucoup d'anciens partisans de la Révolution de juillet s'étaient
ralliés à la politique « d'ordre » menée par la monarchie constitutionnelle.

Au bruit des casseroles, s’ajoutaient en 1832 comme aujourd’hui, des insultes
et des cris, des chansons satiriques et des chants de la Révolution,
qui désignaient l’adversaire politique, comme par exemple : « À bas le moine !
À bas le cafard ! » (visant le procureur général de Grenoble Moyne, jugé
trop proche des monarchistes catholiques légitimistes) ;
« À bas le sanguinaire ! », contre le préfet de l’Isère Duval ; ou encore
« À bas le patriote apostat, à bas l’écrivain vendu, l’orateur acheté !
À bas le traître, le traître à son pays, [...] », à destination du ministre
Adolphe Thiers !

Actualité" de Macron |macron|
================================

Macron, qui assume pleinement la devise « Travail, Ordre et Progrès »,
reproduit aujourd’hui assez exactement l'inféodation du pouvoir judiciaire
et administratif à la politique du gouvernement, qui existait sous
la monarchie de Juillet, à l'origine de ces charivaris bruyants et revendicatifs.

A l'occasion de cette contre-réforme des retraites, le retour des casserolades
est donc une réponse légitime du peuple à l'insulte permanente opposée
aux manifestants, au mépris déclaré à l'égard des corps intermédiaires
et au déni permanent de démocratie via l'utilisation du 49-3 !

Certains de ces charivaris étaient provoqués par des décisions administratives
attentatoires aux libertés les plus élémentaires.

Ce fut le cas à Grenoble, en mars 1832, à la suite d’une mascarade organisée
par une poignée d'étudiants en droit, hostiles à Casimir Perier et à
Louis-Philippe, lorsque le très autoritaire et très zélé préfet de l'Isère Duval
produisit un arrêté interdisant un innocent bal masqué, ce qui a conduit
les opposants à organiser un charivari de protestation (« la Révolte des masques »)
sous les fenêtres de la préfecture, alors située dans les murs de l'hôtel
de Lesdiguières.

La répression, à coups de baïonnettes, d'une foule inoffensive, composée
de femmes et d'hommes, fut menée par un régiment revenu d'Algérie, habitué
aux crimes et violences de la colonisation qui avait débutée en 1830 à la
fin du règne de Charles X.

La lutte à mort qui s'engagea entre la population grenobloise et le 35e
de ligne, à laquelle participa depuis Paris Casimir Perier, qui vivait
la révolte de sa ville natale comme une humiliation personnelle, vit
finalement la victoire des Grenobloises et des Grenoblois, qui infligèrent
à l'armée une seconde « Conduite de Grenoble », venant après celle de
juin 1788 contre les troupes de Louis XVI qui fut une des étincelles
menant à la Révolution de 1789.

Selon le procédé classique d'inversion accusatoire, les Grenoblois-es
furent alors accusé-es par le gouvernement d'être responsables de la
violence. Le retentissement fut énorme dans la presse nationale comme
locale et produisit l'effet inverse de celui qui était recherché, en
multipliant les charivaris dans le pays : « Le sang a coulé à Grenoble,
ville aux moeurs douces que jamais n'avaient ensanglanté les disputes
de parti, où les hommes des opinions les plus opposées se voient, s'estiment,
s'aiment, où l'on vit comme en famille » (Le Dauphinois).*

Si, au lieu de son habituelle condescendance, le Casimir Perier de notre
temps, faisait preuve d'un peu de sens commun, il comprendrait le sens
politique de ces manifestations sonores, qui sont une métaphore de la
révolte de l'opinion publique qu'il ne maîtrisera plus jusqu'à la fin
de son mandat.

Elles ne sont ni un outrage ni une offense à la personne du  « monarque républicain »,
ce qui justifie pour la macronie les nombreuses entraves à la liberté
constitutionnelle de manifester auxquelles nous sommes en train d'assister,
mais au contraire elles rentrent dans le cadre républicain de la liberté
d'expression, héritage de la Révolution ; et par la forme satirique
qu'elles prennent, elles sont l'expression d'une volonté du peuple
d'exister dans l'ordre du politique, malgré le mépris des Foutriquet
d'hier comme d'aujourd'hui !





