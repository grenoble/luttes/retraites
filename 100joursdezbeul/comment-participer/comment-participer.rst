.. index::
   pair: 100JoursDeZbeul; Comment participer

.. _comment_participer:

==========================================================================================================================================================================
**Comment participer**
==========================================================================================================================================================================

- https://100joursdezbeul.fr/comment-participer

Vous souhaitez participer à l’action ? Voici comment !
===========================================================

Chacune des actions suivantes contribue à l'apaisement voulu par notre
président de la République, en incitant le gouvernement à retirer la
réforme des retraites dans une ambiance festive et conviviale.


Les actions sont prises en compte à partir du 18/04.

Actions participants au classement
====================================

Chahut
-----------

Lors d'une visite, organiser casserolades, charivari, cacophonie ou huées.

Manifestation
--------------------

Tenue d'une manifestation sur le lieu de visite d'une personnalité.

Action créative
-----------------

Faire preuve d'originalité dans l'expression des revendications.

Par exemple les opérations escargot, banderoles originales citées dans
des articles, venir déguisé, etc.

Mise en sobriété énergétique
--------------------------------

Intervention de camarades afin de limiter la consommation énergétique et
préserver l'environnement par une transition énergétique accélérée et
locale des membres du gouvernement.

Action conduisant au départ précipité d’une personnalité
----------------------------------------------------------------

Lors de la visite d'une personnalité, celle-ci décide d'accélérer le pas
et de rentrer plus tôt que prévu.

Annulation d’une visite 🎉
-----------------------------

Lorsqu'une personnalité renonce à une partie ou la totalité de la visite prévue.

Actions non prises en compte
==================================

Certaines actions ne sont pas prises en compte dans le classement.

**Les violences physiques ne sont pas prises en compte dans le classement
et ne donnent pas de point bonus**.

Notre objectif est de soutenir la mobilisation contre la réforme des
retraites et d'obtenir son retrait.

Pour le moment les actions internationales ne sont pas prises en compte
**mais le sujet est en cours de discussion**.

Pour le moment les actions sur les personnes non membre du gouvernement
(députés de la majorité, caravane du SNU...) ne sont pas prises en compte
**mais le sujet est en cours de discussion**.

Nous mettons à jour les données une fois par jour, le soir.

Les actions du jour sont en général prises en compte le lendemain.


