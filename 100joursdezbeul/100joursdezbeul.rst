.. index::
   pair: Cédric Rossi; zbeul
   ! #100JoursDeZbeul

.. _100jours_zbeul:

==========================================================================================================================================================================
**100 jours de zbeul** #100JoursDeZbeul #IntervillesDuZbeul
==========================================================================================================================================================================

- https://100joursdezbeul.fr/
- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://100joursdezbeul.fr/nouveautes
- https://github.com/cedricr/100joursdezbeul/commits/main
- https://piaille.fr/@cedricr
- https://piaille.fr/@cedricr.rss
- https://syndicat.solidaires.org/@SolInfoNat
- https://syndicat.solidaires.org/@SolInfoNat.rss


::


    100joursdezbeul AT solidairesinformatique DOT org


.. figure:: images/saint_denis.jpeg
   :align: center

   https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations

   Pour proposer des nouveaux rassemblements à ajouter à la carte, vous
   pouvez nous contacter sur l’email : carte@attac.org


.. toctree::
   :maxdepth: 5

   regles-du-jeu/regles-du-jeu
   proposer-un-evenement/proposer-un-evenement
   comment-participer/comment-participer
   2023/2023
