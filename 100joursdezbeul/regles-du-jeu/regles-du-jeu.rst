.. index::
   pair: 100JoursDeZbeul; Règles du Jeu

.. _regles_du_jeu:

==========================================================================================================================================================================
**Règles du jeu**
==========================================================================================================================================================================

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://github.com/cedricr/100joursdezbeul


Préambule
===========

En partant des `données compilées par Attac <https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations>`_,
on assigne manuellement à chaque événement un type de personne ciblée et un type d’action.

Les données finales sont accessibles dans le `code source <https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations>`_

Les vérifications sont les bienvenues !

Chacune de ces actions attribue un certain nombre de points au département
où elles se passent ;
le type de personne ciblée permet de multiplierces points.

Là où plusieurs personnalités sont ciblées, les points sont comptés pour
chacune.

Barème
========

Types d’actions

- Chahut (casserolade, huée, ) — 1 pt
- Manifestation — 1 pt
- Action créative — 2 pts
- Mise en sobriété énergétique — 3 pts
- Action conduisant au départ précipité d’une personnalité — 4 pts
- Annulation d’une visite 🎉 — 5 pts


Personnalités
-------------

- Secrétaire d’État — x1
- Ministre délégué·e — x2
- Ministre — x3
- Ministre de l’intérieur — x4
- Présidente de l’Assemblée Nationale — x4
- Première ministre — x5
- Président de la République — x6
