
.. _actions_2023_04_01:

=======================================================================================================================
**Actions du samedi 01 avril 2023**
=======================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_01.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/portes-ouvertes-de-l-ouvre-aux-1000-jardins
- https://www.ici-grenoble.org/evenement/atelier-clown-e-s-en-mixite-choisie-1
- https://www.ici-grenoble.org/evenement/depart-collectif-a-velo-pour-la-manif-de-leau-pas-des-puces
- https://www.ici-grenoble.org/evenement/distribution-gratuite-de-drones-be-gone-campus
- https://www.ici-grenoble.org/evenement/manifestation-de-leau-pas-des-puces-contre-le-gaspillage-de-leau-par-les-industriels-high-tech
- https://www.ici-grenoble.org/evenement/atelier-comment-fabriquer-une-tartapulte-en-non-mixite
- https://www.ici-grenoble.org/evenement/conference-l-utilisation-des-cacatovs-a-travers-les-ages
- https://www.ici-grenoble.org/evenement/fin-de-la-treve-hivernale-rassemblement-contre-les-expulsions-et-les-coupures-denergie
- https://www.ici-grenoble.org/evenement/cine-debat-isere-2053-sur-la-plus-grande-cooperative-danticipation-des-crises-climatiques-pres-de-grenoble
- https://www.ici-grenoble.org/structure/la-cooperative-isere-2053
- https://www.ici-grenoble.org/evenement/cine-repas-concert-hommage-aux-femmes-battantes
- https://www.ici-grenoble.org/evenement/grande-soiree-sosies-deric-piolle-concerts-jeux-pierrade-vegan
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-et-la-liberation-des-prisonniers-politiques
- :ref:`iran_luttes:iran_grenoble_2023_04_01`


Demosphere 38
=================

.. figure:: images/demosphere38_2023_04_01.png
   :align: center

   https://38.demosphere.net/

