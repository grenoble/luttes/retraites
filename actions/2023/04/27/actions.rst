
.. _actions_2023_04_27:

=========================================================================================================================================
**Actions du jeudi 27 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #Casserolade #MalLogés #1erMai2023**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #Casserolade #MalLogés #1erMai2023


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_27.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- |important| |casserolade| 12h00 https://www.ici-grenoble.org/evenement/casserolade-des-mal-loge-e-s-et-pique-nique-revendicatif

  .. figure:: images/dal38.png
     :align: center

     https://www.ici-grenoble.org/evenement/casserolade-des-mal-loge-e-s-et-pique-nique-revendicatif

- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- |important| 17h30 https://www.ici-grenoble.org/evenement/velorution-de-lutte-contre-la-reforme-des-retraites
- 17h30 🎥 https://www.ici-grenoble.org/evenement/cine-caisse-de-greve-un-pays-qui-se-tient-sage-sur-la-repression-policiere-des-mouvement-sociaux-en-france
- |important| 17h45 🎥 https://www.ici-grenoble.org/evenement/cine-choc-a-pas-aveugles-sur-les-photos-clandestines-des-camps-de-la-mort-nazis
- 18h30 https://www.ici-grenoble.org/evenement/formation-aux-premiers-secours-en-manifestation-ou-en-action-politique
- 18h30 https://www.ici-grenoble.org/evenement/rencontre-avec-lautrice-de-le-cout-de-la-virilite-une-analyse-economique-du-patriarcat
- 19h00 https://www.ici-grenoble.org/evenement/rencontre-avec-les-auteurs-de-la-lutte-emprisonnee-sur-la-repression-des-luttes-en-italie-dans-les-annees-70
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cine-discussion-tout-le-monde-deteste-le-nucleaire-infotour-antinucleaire-du-bugey
- |important| 20h00 https://www.ici-grenoble.org/evenement/apero-vegan-et-cine-rencontres-dieu-merci-je-suis-lesbienne-sur-les-liens-entre-lesbianite-et-feminisme


Demosphere 38
=================

.. figure:: images/demo_2023_04_27.png
   :align: center

   https://38.demosphere.net/

- 10h https://38.demosphere.net/rv/1136 (Rassemblement devant le pole hydraulique Grenoblois)
- 11h58  👨‍🎓 https://38.demosphere.net/rv/1135 (Pour une université ouverte, libre et émancipatrice)
- 12h00 |casserolade| https://38.demosphere.net/rv/1110 (sous la Tour Perret : Casserolade des Mal-logéEs et pique-nique revendicatif! )

  .. figure:: images/dal38.png
     :align: center

     https://www.ici-grenoble.org/evenement/casserolade-des-mal-loge-e-s-et-pique-nique-revendicatif


- |important| 16h30 https://38.demosphere.net/rv/1064 (TRACTAGE AU MARCHÉ DE LA VILLENEUVE)
- |important| |important| 17h00 https://38.demosphere.net/rv/1108 (Vélorution de lutte )
- 17h30 🎥 https://38.demosphere.net/rv/1087 (Film "Un pays qui se tient sage" en soutien aux caisses de grève UGA)
- 17h30  👨‍🎓 https://38.demosphere.net/rv/1129 (Rassemblement éducation au rectorat)
  Pour soutenir les enseignants UPE2A devant " la perte de moyen est dramatique
  tant pour les élèves que pour l'ensemble de la communauté éducative :
  souffrance scolaire, suivi insuffisant faute de moyen, décrochage, dégradation
  des conditions d’enseignement/apprentissage, difficulté de coordination des équipes…

- 18h30 https://38.demosphere.net/rv/1091 (Formation aux Premiers Secours en Manif et Actions au 38, rue d'Alembert, dans la bibliothèque)
- 19h00 |car38| https://38.demosphere.net/rv/1042 (LIVRE - « LA LUTTE EMPRISONNEE » )

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme


100 jours de zbeul
========================

- :ref:`100jours_zbeul`


