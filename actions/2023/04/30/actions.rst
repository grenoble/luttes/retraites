
.. _actions_2023_04_30:

=========================================================================================================================================
**Actions du dimanche 30 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #zbeul


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_30.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/formation-construire-et-repartir-avec-une-parabole-solaire-cuisson-rapide-des-aliments
- https://www.ici-grenoble.org/evenement/vide-grenier-a-prix-libre-a-ahwahnee-fringues-ustensiles-objets-insolites
- 14h00 ANNULE https://www.ici-grenoble.org/evenement/attention-annule-rassemblement-de-soutien-au-bosquet-anti-cable
- 14h00 |car38| https://www.ici-grenoble.org/evenement/escape-game-anticarceral

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 17h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 19h00 https://www.ici-grenoble.org/evenement/festibaf-repas-vegan-et-concerts-pour-la-fin-de-la-baf
- 19h30 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines


Demosphere 38
=================

.. figure:: images/demo_2023_04_30.png
   :align: center

   https://38.demosphere.net/

- 14h00 |car38| https://38.demosphere.net/rv/1045 (ESCAPE GAME ANTI-CARCERAL )


100 jours de zbeul/on-ne-les-lache-pas
=========================================

- https://100joursdezbeul.fr/

Attac
------

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations

Pour proposer des nouveaux rassemblements à ajouter à la carte, vous
pouvez nous contacter sur l’email : carte@attac.org

100joursZbeul
---------------

- https://100joursdezbeul.fr/
- https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493


.. figure:: ../../../../100joursdezbeul/proposer-un-evenement/images/page_formulaire.png
   :align: center

   https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493

Vous pouvez utiliser ce formulaire pour soumettre un événement ayant eu
lieu dans votre département, ou bien pour faire une réclamation sur un
événement passé -- pensez à le préciser dans le champ de commentaire.

::


    100joursdezbeul AT solidairesinformatique DOT org
