
.. _actions_2023_04_10:

=========================================================================================================================================
**Actions du lundi 10 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_10.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- https://www.ici-grenoble.org/evenement/atelier-de-clown-activisme
- https://www.ici-grenoble.org/evenement/coup-de-coeur-rencontre-intime-et-politique-avec-virginie-despentes


Demosphere 38
=================

.. figure:: images/demo_2023_04_10.png
   :align: center

   https://38.demosphere.net/

