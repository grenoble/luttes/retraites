
.. _actions_2023_04_25:

=========================================================================================================================================
**Actions du mardi 25 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_25.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 12h:  https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- 12h: https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- **12h15**: https://www.ici-grenoble.org/evenement/assemblee-generale-du-secteurs-culture-en-lutte
- |important| |car38| **14h**: https://www.ici-grenoble.org/evenement/atelier-tout-savoir-sur-les-fichiers-de-la-police-fiches-s-fpr-fnaeg-fijait
- 🎥 **15h10**: https://www.ici-grenoble.org/evenement/cine-choc-a-pas-aveugles-sur-les-photos-clandestines-des-camps-de-la-mort-nazis
- 17h: https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- 🎥 17h15: https://www.ici-grenoble.org/evenement/cinema-relaxe-sur-laffaire-tarnac
- 18h: https://www.ici-grenoble.org/evenement/binary-is-for-computer-permanence-informatique-a-la-b-a-f-depannages-installation-de-linux-cryptage-de-donnees
- 18h30: https://www.ici-grenoble.org/evenement/rencontres-velo-comment-la-reussite-de-copenhague-pourrait-inspirer-grenoble
- **19h**: https://www.ici-grenoble.org/evenement/rencontres-laccueil-et-le-non-accueil-des-personnes-exilees-en-france
- 🎥 |JinaAmini| 🇮🇷 19h30: https://www.ici-grenoble.org/evenement/cine-debat-et-buffet-iranien-born-in-evin-une-enquete-sur-la-pire-prison-politique-diran

  - https://iran.frama.io/luttes/actions/2023/04/25/born-in-evin.html

- 19h30: 🎉 https://www.ici-grenoble.org/evenement/retour-joyeux-et-en-images-sur-le-carnaval-du-dragon
- 19h30: https://www.ici-grenoble.org/evenement/reunion-publique-57-millions-deuros-pour-la-rd1075-est-ce-pertinent-trieves
- 🎥 20h40: https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- |important| 🎥 20h45: https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 🎥 20h50 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- |car38| 21h30: https://www.ici-grenoble.org/evenement/soiree-de-reflexion-autour-de-la-justice-transformatrice


Demosphere 38
=================

.. figure:: images/demo_2023_04_25.png
   :align: center

   https://38.demosphere.net/

- 7h30 https://38.demosphere.net/rv/1132 (Piquet de grève, rencontre avec la direction générale et AG étudiante )
- 11h30: https://38.demosphere.net/rv/1063 (TRACTAGE AU CARREFOUR TESSEIRE/MALHERBE)
- 12h15: https://38.demosphere.net/rv/1088 (AG CULTURE en LUTTE)
- |car38| 14h: https://38.demosphere.net/rv/1038 (Tous·tes Fiché·es)
- |car38| https://38.demosphere.net/rv/1039 (Reflexions autour de la justice transformatrice 1/2 )


Extrait de l'article de bonpote du lundi 24 avril 2023
===========================================================

- https://bonpote.com/un-an-apres-la-reelection-demmanuel-macron-ce-quinquennat-nest-toujours-pas-ecologique/


Il y a exactement un an, le 24 avril 2022, Emmanuel Macron était réélu
président de la République. Une semaine avant sa réélection, le 16 avril 2022,
il déclarait lors d’un meeting d’entre-deux-tours à Marseille que son
second mandat serait “écologique ou ne sera pas”.

Un an après, quel est le bilan de l’action de la politique du gouvernement ?
Est-ce qu’Emmanuel a respecté sa promesse ?

Sans aucun suspense, la réponse est non.

Mais il ne suffit pas de dire que ce n’est pas assez.

Alors qu’il n’y a plus aucun doute sur l’urgence à agir, **il faut expliquer
pourquoi l’inaction climatique du gouvernement est aujourd’hui criminelle**.

...

**Que faire en tant que citoyen ? Cette conclusion devrait ponctuer chaque article**
---------------------------------------------------------------------------------------

**Tout. Absolument tout, selon ses moyens**. Agir à son échelle, et toujours
recentrer son action dans un objectif collectif.

**Soutenir la presse indépendante et les personnes qui alertent sur le
changement climatique et l’effondrement de la biodiversité**.

**Soutenir les scientifiques** qui, après des décennies de rapports scientifiques
occultés, font le choix délibéré d’aller s’allonger sur des routes ou
s’enchainer aux portes d’une banque climaticide pour alerter.

Le courage ne viendra certainement pas d’un gouvernement qui n’est toujours
pas capable de limiter la vitesse sur l’autoroute à 110 km/h.

**Il faudra une pression citoyenne et politique pour se faire entendre,
sous peine d’avoir les mêmes regrets à la fin de l’année 2023**.
