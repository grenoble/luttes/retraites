
.. _actions_2023_04_16:

=========================================================================================================================================
**Actions du dimanche 16 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_16.png
   :align: center

   https://www.ici-grenoble.org/agenda


Demosphere 38
=================

.. figure: images/demo_2023_04_16.png
   :align: center

   https://38.demosphere.net/

