
.. _actions_2023_04_24:

==========================================================================================================================================================================
**Actions du lundi 24 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites** rejoignez-nous pour fêter un **"mauvais anniversaire"** à Macron ! |macron| |jo|
==========================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`
- https://cric-grenoble.info/infos-locales/info-locales/article/suivi-du-mouvement-social-semaine-du-24-avril-2986

#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #ApaisementJour7 #foutonslezbeul #intervilles #casseroles #100jours


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_24.png
   :align: center

   https://www.ici-grenoble.org/agenda

- **6h15** https://www.ici-grenoble.org/evenement/appel-a-bloquer-le-campus-contre-la-reforme-des-retraites-et-les-violences-policieres
- **10h**: https://www.ici-grenoble.org/evenement/ag-etudiante-contre-la-reforme-des-retraites-et-son-monde
- 18h: https://www.ici-grenoble.org/evenement/cine-debat-compagnons-de-cordee-sur-lentraide-entre-habitant-e-s-et-migrant-e-s-a-la-frontiere-franco-italienne
- 18h30: https://www.ici-grenoble.org/evenement/atelier-comment-ne-pas-depasser-2-tonnes-demissions-de-co2-par-personne
- 18h30 https://www.ici-grenoble.org/evenement/rencontre-avec-les-autrices-de-foutues-pour-foutues-ultime-evenement-de-la-bibliotheque-feministe-a-la-baf
- 18h30 https://www.ici-grenoble.org/evenement/rencontres-la-repression-dans-le-milieu-syndical
- 18h40 https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- **19h**: https://www.ici-grenoble.org/evenement/rassemblement-festif-la-fete-a-macron
- 19h: https://www.ici-grenoble.org/evenement/conference-gesticulee-le-coup-de-poing-nest-pas-parti-sur-les-violences-dans-les-luttes


Demosphere 38
=================

.. figure:: images/demo_2023_04_24.png
   :align: center

   https://38.demosphere.net/

- https://38.demosphere.net/rv/1119 (Nous appelons ce lundi 24/04 au matin à une *grosse matinée d'action sur la fac* !)
- 6h30: https://38.demosphere.net/rv/1126 (Blocage de l'ENSAD)
- 18h30: https://38.demosphere.net/rv/1032 (Repression dans le milieu syndical )
- 19h: https://38.demosphere.net/rv/1096  (Rassemblement festif La fête à Macron)
- 20h: https://38.demosphere.net/rv/1116 ( Casserolade, Dé-fêtons l'anniversaire de Jupiter, Domène)
- 20h: https://38.demosphere.net/rv/1117 (Casserolade Générale (appel Attac))

|car38| Repression dans le milieu syndical**
---------------------------------------------------------

- https://www.ici-grenoble.org/structure/le-collectif-anti-repression-isere
- https://38.demosphere.net/rv/1032 (Repression dans le milieu syndical )
- https://cric-grenoble.info/infos-locales/article/quinzaine-contre-la-repression-2948
- https://www.ici-grenoble.org/article/la-quinzaine-anti-repression

.. figure:: ../23/images/quinzaine_anti_repression.png
   :align: center

   https://www.ici-grenoble.org/article/la-quinzaine-anti-repression


Pour la deuxième année consécutive, le CAR38 (Collectif Anti Répression)
organise quinze jours d'évènements pour mieux comprendre, se protéger
et s'organiser face à la répression d'Etat. Lors de ce festival, nous
questionnerons la manière dont la police, la justice et la prison y prennent
part tout en essayant de penser de réfléchir à des perspectives en dehors
de leur existence.

**Ça aura lieu du 22 avril au 7 mai**.

Dans ce cadre, nous vous proposons une présentation intitulée "Repression dans le milieu syndical"

Exemple de répression syndicale dans l'éducation et au sein de la poste.

Présentation et animation SUD-Solidaire.

Plus d'info sur les autres évènement de la quinzaine:

- https://cric-grenoble.info/infos-locales/article/quinzaine-contre-la-repression-2948
- https://www.ici-grenoble.org/article/la-quinzaine-anti-repression


.. _fete_a_macron_203_04_24:

|jo| **Rassemblement festif La fête à Macron**  Apaisement, jour 7 ! |macron|
==================================================================================

- https://www.ici-grenoble.org/evenement/rassemblement-festif-la-fete-a-macron
- https://38.demosphere.net/rv/1096
- :ref:`media_2023:macron_2023_04_24`
- https://www.openstreetmap.org/#map=19/45.19260/5.73169
- https://blogs.mediapart.fr/jerome-soldeville/blog/230423/les-casseroles-de-foutriquet

.. figure:: images/casserolade_macron_2023_04_24_grenoble_festif.jpg
   :align: center

1 an de ''réélection", c'est déjà trop, Macron démission ! Non à la résignation !

RDV Place Notre Dame à 19H lundi 24 avril 2023 !

A l'appel des AGs de luttes et interpro de Grenoble.

Rejoignez-nous pour fêter un "mauvais anniversaire" à Macron !
Ce rassemblement festif sera l'occasion de souffler les bougies
(rallumons les braises) et lui faire la fête comme il se doit avant le 1er mai 2023 !

Venez déguisé·es, masqué·es avec des casseroles, des bougies et de nombreux
cadeaux empoisonnés.

.. figure:: images/casserolade_macron_2023_04_24_grenoble.jpg
   :align: center


.. raw:: html

    <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
        src="https://www.openstreetmap.org/export/embed.html?bbox=5.729919970035554%2C45.19180269851303%2C5.733460485935212%2C45.193401826280905&amp;layer=mapnik" style="border: 1px solid black">
    </iframe><br/>
    <small><a href="https://www.openstreetmap.org/#map=19/45.19260/5.73169">Afficher une carte plus grande</a></small>


|jo| Casserolade Générale (appel Attac)
===========================================

- https://38.demosphere.net/rv/1117
- https://blogs.mediapart.fr/jerome-soldeville/blog/230423/les-casseroles-de-foutriquet

Source: https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/casserolade-generale-du-24-avril-la-carte-des-rassemblements

A 20h devant les mairies de vos villages (sauf à Grenoble : place notre dame à 19h)

Appel à taper sur des casseroles

pensez à prévenir de votre rassemblement à carte@attac.org pour que les
articles de journaux se rendent compte du nombre de rassemblements

420 lieux la semaine dernière

|jo| 100 jours de zbeul **Quels sont les départements qui zbeulent le plus ?**
================================================================================================

- https://100joursdezbeul.fr/
- https://blogs.mediapart.fr/jerome-soldeville/blog/230423/les-casseroles-de-foutriquet

.. figure::  images/jo_casseroles.png
   :align: center


Règles du jeu
--------------

- https://100joursdezbeul.fr/regles-du-jeu


En partant des données compilées par Attac, on assigne manuellement à
chaque événement un type de personne ciblée et un type d’action.

Les données finales sont accessibles dans le code source.
Vérifications sont les bienvenues !

Chacune de ces actions attribue un certain nombre de points au département
où elles se passent ; le type de personne ciblée permet de multiplier
ces points.

Là où plusieurs personnalités sont ciblées, les points sont comptés
pour chacune.

Articles
==========

.. _bon_pote_2023_04_24:

Un an après la réélection d’Emmanuel Macron, ce quinquennat n’est toujours pas écologique
-------------------------------------------------------------------------------------------

- https://bonpote.com/un-an-apres-la-reelection-demmanuel-macron-ce-quinquennat-nest-toujours-pas-ecologique/

Il y a exactement un an, le 24 avril 2022, Emmanuel Macron était réélu
président de la République. Une semaine avant sa réélection, le 16 avril 2022,
il déclarait lors d’un meeting d’entre-deux-tours à Marseille que son
second mandat serait “écologique ou ne sera pas”.

Un an après, quel est le bilan de l’action de la politique du gouvernement ?
Est-ce qu’Emmanuel a respecté sa promesse ?

Sans aucun suspense, la réponse est non.

Mais il ne suffit pas de dire que ce n’est pas assez.

Alors qu’il n’y a plus aucun doute sur l’urgence à agir, **il faut expliquer
pourquoi l’inaction climatique du gouvernement est aujourd’hui criminelle**.

Des milliers de morts, une couverture médiatique quasi nulle
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

2022 fut l’année la plus chaude jamais enregistrée en France, avec le
2e été le plus chaud jamais connu.

33 jours de vagues de chaleur, accompagnées d’une sécheresse historique,
de méga-feux et de canicules océaniques en Méditerranée.

Les alertes n’ont jamais cessé, que cela concerne les pertes agricoles
dues aux sécheresses jusqu’aux glaciers déclarés officiellement morts.

Les canicules ont fait des milliers de morts dans une indifférence quasi
générale.
Aucun membre du gouvernement n’a été interpellé par un journaliste dans
une matinale ou une interview.
Aucun compte à rendre. Vous avez bien lu : des milliers de morts en France
à cause des canicules et le gouvernement n’a jamais été interrogé dessus.

La probabilité de vivre un été similaire voire pire est non nulle, mais
rien n’a été fait pour empêcher un nouveau scénario catastrophe.

Cela résulte d’une faillite politique, mais pas seulement.

A l’instar de Sébastien Julian (Rédacteur en chef adjoint climat et
transitions à L’Express) qui confond “état des connaissances scientifiques”
et “discours” du GIEC”, les médias ont leur part de responsabilité.

Tant que l’urgence climatique ne fera pas partie des priorités de tous
les médias, que les journalistes seront formés correctement, et tant
que les politiques pourront continuer à raconter n’importe quoi sur les
sujets énergies et climat sans être réfuté(e)s, nous serons tous perdant(e)s.

Qui aurait pu prédire que le Président |macron| ignore les scientifiques ?
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ce nouveau quinquennat est aussi victime d’une multiplication d’erreurs
de communication du gouvernement sur le climat.

Des choix désastreux ont été faits, comme celui du président Macron de
se rendre deux fois au Qatar lors du Mondial de football 2022, alors
qu’en même temps se jouait l’avenir de l’humanité à la COP15 sur la
biodiversité à Montréal.

Mais peut-être que l’apothéose fut lors de son discours du 31 décembre 2022,
où le président Macron déclara “qui aurait pu prédire la crise climatique”.

Qui aurait pu prédire la crise climatique, demande Emmanuel Macron…

– 27 COP
– 6 rapports du GIEC
– Des milliers de rapports scientifiques
– Le Haut Conseil pour le Climat
– Etc.

**Tout le monde finalement. Sauf un gouvernement incompétent.**

Une phrase qui a non seulement agacé les personnes engagées dans la lutte
contre le réchauffement climatique, mais aussi certains scientifiques et
auteurs du GIEC qui ont “cru à une boutade, une phrase sortie de son contexte“.

**Ce n’est malheureusement pas le cas**.

C’est une phrase relue par une équipe de communication et totalement
assumée par le Président qui cherchait peut-être un échappatoire au cas
où un jour, il aurait à rendre des comptes sur son inaction climatique
et les milliers de morts.


Que faire en tant que citoyen ? **Tout, absolument tout**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

La France avait vécu un été très difficile, avec des canicules à répétition
et une sécheresse historique.

Une étude du CNRS a relié la sécheresse 2022 aux activités humaines.

Inutile d’attendre une étude pour 2023 pour savoir que la situation sera
critique.

Si l’information est clef, il serait utopique de croire qu’il suffit de
savoir pour changer.

Le changement climatique est politique et se battre politiquement sera
indispensable pour avancer, ou du moins ne pas reculer.

Plus facile à dire qu’à faire quand on observe l’inaction du gouvernement
depuis un an, bien aidée par la droite et l’extrême-droite, qui ne
s’embarrassent pas de savoir si ce qu’ils disent est vrai ou faux et
sabotent depuis le début de leur mandat les lois qui pourraient nous
aider à atteindre nos objectifs climatiques.

**Que faire en tant que citoyen ? Cette conclusion devrait ponctuer chaque article**

**Tout. Absolument tout, selon ses moyens**. Agir à son échelle, et toujours
recentrer son action dans un objectif collectif.

**Soutenir la presse indépendante et les personnes qui alertent sur le
changement climatique et l’effondrement de la biodiversité**.

**Soutenir les scientifiques** qui, après des décennies de rapports scientifiques
occultés, font le choix délibéré d’aller s’allonger sur des routes ou
s’enchainer aux portes d’une banque climaticide pour alerter.

Le courage ne viendra certainement pas d’un gouvernement qui n’est toujours
pas capable de limiter la vitesse sur l’autoroute à 110 km/h.

**Il faudra une pression citoyenne et politique pour se faire entendre,
sous peine d’avoir les mêmes regrets à la fin de l’année 2023**.


.. _reaction_au_daube_2023_04_24:

Réaction à un article illiberal |macron| du Dauphiné Libéré
==============================================================

Bonjour,

Quand j'ai lu `ces articles hier <https://www.ledauphine.com/faits-divers-justice/2023/04/24/nouveau-concert-de-casseroles-place-notre-dame>`_,
je me suis plutôt marré parce que j'avais pas l'impression d'avoir
participé à un cérémonie d'ultra violence gauchisto-vaudoue
lundi soir place Notre Dame, mais plutôt à un charivari carnavalesque
avec piñata à l'effigie de Macron Premier.

L’article de ce matin me fait moins rire, parce que ce sont sans doute
les affabulations paranoïaques du Dauphiné Libéré qui motivent ces
enquêtes... et les poursuites à venir.

Il me semblerait utile et important que les organisations syndicales/politiques/mouvement
social (je doute que le daubé réagisse à un communiqué de ma pomme) contre-attaquent
en publiant un communiqué pour rappeler notamment que:

- les casserolades et autres carnavals ont toujours existé et sont justement
  des alternatives rigolotes à la violence lorsque la colère est immense.
- La macronie passe son temps à répéter que les pas content.es "refusent le dialogue"
  en oubliant qu'elle s'est essuyé les pieds sur le dialogue social,
  les négociations et les protestations pacifiques de 12 journées de manifs
  à plusieurs millions.
  Et quand ils "dialoguent", c'est pour mieux humilier et mépriser
- Comme ça a déjà été dit, la violence (et pas symbolique: yeux crevés,
  mains arrachées, testicules éclaté...etc) est de leur côté, les victimes
  du nôtre: voir images (innombrables) des violences policières + salutaire
  travail de l’observatoire des street medics: https://obs-medics.org/
- "C'est pas avec des casseroles qu'on fera avancer la France": nous
  prenons Macron au mot et réclamons de virer les ministres et autres
  larbins mis en examen voire condamnées (voir ce `tract <https://nuage.gresille.org/index.php/s/8dEAdGeFstBo96F>`_,
  à diffuser sans modération).

