
.. _actions_2023_04_26:

=========================================================================================================================================
**Actions du mercredi 26 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #ExtinctionRebellion**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #ExtinctionRebellion


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_26.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-26-avril-1986-l-explosion-de-la-centrale-nucleaire-de-tchernobyl
- |car38| https://www.ici-grenoble.org/evenement/journee-dintroduction-a-la-securite-informatique-sur-inscription

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 14h: https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 18h30: https://www.ici-grenoble.org/evenement/expose-guerre-aux-frontieres-une-bataille-coloniale
- |important| **18h30**: ⚖️ https://www.ici-grenoble.org/evenement/reunion-publique-dinformation-sur-les-lois-darmanin-et-kasbarian
- 18h30: https://www.ici-grenoble.org/evenement/aperomix-en-soutien-aux-caisses-de-greve
- |important| **18h30**: https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- 🎥 18h50: https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 19h00: https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-de-chants-revolutionnaires-les-barricades
- |important| |important| **19h30**: https://www.ici-grenoble.org/evenement/assemblee-dextinction-rebellion-grenoble |important|

  - https://www.ici-grenoble.org/structure/extinction-rebellion-grenoble

  **Le réchauffement climatique vous fait l'effet d'un film d'horreur au ralenti ?
  Face au désastre, vous n'avez aucune confiance dans les gouvernements ?
  Vous souhaitez rejoindre un mouvement écologiste radical, populaire et prêt à l'action directe ?**

  Créé en 2019, Extinction Rebellion Grenoble est l'antenne locale du
  mouvement `Extinction Rebellion <https://extinctionrebellion.fr/>`_

- |car38| 🎥 19h30: https://www.ici-grenoble.org/evenement/cine-rencontres-la-cite-de-lordre-sur-le-site-de-simulation-de-lecole-de-police
- 🎥 20h45: https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 🎥 20h45:https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines

Demosphere 38
=================

.. figure:: images/demo_2023_04_26.png
   :align: center

   https://38.demosphere.net/

- 9h |important| |car38|  https://38.demosphere.net/rv/1040 (Formation sécurité informatique 9H à 18H30 )
- 17h: |important| 👨‍🎓 https://38.demosphere.net/rv/1128 (Assemblée Générale Interlycée au 3 rue Federico Garcia Lorca 38100 grenoble)
- 17h30 https://38.demosphere.net/rv/1125 (Rassemblement du CMAP (collectif matheysin d'action populaire) contre la réforme des retraites )
- 18h30 |important| https://38.demosphere.net/rv/1092 (Assemblée Générale de Lutte )
- 18h30 ⚖️ https://38.demosphere.net/rv/1107 (réunion publique : contre les lois racistes et antisociales, contre kasbarian-bergé, contre darmanin papiers et logements pour tous et toutes)
- 19h30 |important| |important| |car38| 🎥 https://38.demosphere.net/rv/1041 (film - « la cite de l'ordre »)
