
.. _actions_2023_04_23:

======================================================================================================================================================================================
**Actions du dimanche 23 avril 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites** + |macron|  lundi 24 avril 2023 : à Grenoble la "fête à Macron" c'est place notre dame
======================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #casserolade #PlaceNotreDame


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_04_23.png
   :align: center

   https://www.ici-grenoble.org/agenda


- |car38| https://www.ici-grenoble.org/evenement/journee-de-formation-anti-repression-que-faire-face-aux-violences-de-la-police-et-aux-arrestations
- https://www.ici-grenoble.org/evenement/balade-botanique-dans-le-trieves-decouvrir-les-plantes-sauvages-et-leurs-usages
- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-du-bosquet-anti-cable-friche-occupee-et-autogeree-contre-le-metrocable
- https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-noise-au-102
- https://www.ici-grenoble.org/evenement/cine-discussion-les-depossedes-sur-le-capitalisme-vert-et-la-repression


Demosphere 38
=================

.. figure:: images/demo_2023_04_23.png
   :align: center

   https://38.demosphere.net/


- https://38.demosphere.net/rv/1030 (Quinzaine Antirep : Journée de formation)
- https://38.demosphere.net/rv/1062 (collage/scotchage d'affiches dans les quartiers Villeneuve Malherbe, V.O VIgny-Musset, RDV devant le patio )

|car38| Quinzaine contre la répression
-------------------------------------------------

- https://www.ici-grenoble.org/structure/le-collectif-anti-repression-isere
- https://www.ici-grenoble.org/article/la-quinzaine-anti-repression
- https://cric-grenoble.info/infos-locales/article/quinzaine-contre-la-repression-2948
- https://38.demosphere.net/rv/1030 (Quinzaine Antirep : Journée de formation)

.. figure:: images/quinzaine_anti_repression.png
   :align: center

   https://www.ici-grenoble.org/article/la-quinzaine-anti-repression


Pour la deuxième année consécutive, le CAR38 (Collectif Anti Répression)
organise quinze jours d'évènements pour mieux comprendre, se protéger
et s'organiser face à la répression d'Etat.

Lors de ce festival, nous questionneront la manière dont la police, la
justice et la prison y prennent part tout en essayant de penser de
réfléchir à des perspectives en dehors de leur existence.

Ça aura lieu du 22 avril au 7 mai 2023.

Dans ce cadre, nous vous proposons une Journée de formation :

- 10h : Formation autour de la répression (2 groupes de niveaux) pour se
  former aux bonnes pratiques face à la police et la justice.

- A partir de 14h grand jeu pédagogique sur la répression.

Repas végan à prix libre le midi et apéro festif à partir de 17h.

Plus d'info sur les autres évènement de la quinzaine :

- https://cric-grenoble.info/infos-locales/arti…/quinzaine-contre-la-repression-2948


.. _fete_a_macron_203_04_23:

|macron| Lundi 24 avril 2023 : à Grenoble la "fête à Macron" c'est place notre dame
======================================================================================

- :ref:`fete_a_macron_203_04_24`
- :ref:`media_2023:macron_2023`
- :ref:`media_2023:macron_2023_04_24`
- https://www.openstreetmap.org/#map=19/45.19260/5.73169

Pour Grenoble, **l'appel des AGs de luttes et interpro pour la "fête à Macron" est place Notre Dame.**

Pour les autres communes, les casserolades se passent devant les mairies.

.. raw:: html

    <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
        src="https://www.openstreetmap.org/export/embed.html?bbox=5.729919970035554%2C45.19180269851303%2C5.733460485935212%2C45.193401826280905&amp;layer=mapnik" style="border: 1px solid black">
    </iframe><br/>
    <small><a href="https://www.openstreetmap.org/#map=19/45.19260/5.73169">Afficher une carte plus grande</a></small>





