
.. _actions_2023_04_05:

=========================================================================================================================================
**Actions du mercredi 5 avril 2023** #Pas1JourDePlus #Retraites #64ansCestToujoursNon #NoBassaran #JeMeSoulève #Grenoble
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Pas1JourDePlus #Retraites #64ansCestToujoursNon #NoBassaran #JeMeSoulève #Grenoble


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_05.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/vernissage-exposition-nous-occupons
- https://www.ici-grenoble.org/evenement/le-5-avril-1971-la-publication-du-manifeste-des-343
- https://www.ici-grenoble.org/evenement/chantier-collectif-prenons-les-terres-dans-lagglo-grenobloise
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/atelier-de-deplacement-collectif-en-situation-de-manifestation-daction-militante-ou-de-blocage-2
- https://www.ici-grenoble.org/evenement/conference-quelles-sont-les-conclusions-du-nouveau-rapport-du-giec
- https://www.ici-grenoble.org/evenement/assemblee-de-lutte-contre-la-loi-kasbarian-criminalisation-des-sans-abris-des-squats-et-locataires-en-difficulte
- https://www.ici-grenoble.org/evenement/velorution-feministe-davril-le-retour
- https://www.ici-grenoble.org/evenement/conference-de-quoi-les-enquetes-publiques-sont-elles-le-nom
- https://www.ici-grenoble.org/evenement/special-retraites-un-pays-qui-se-souleve-frederic-lordon
- https://www.ici-grenoble.org/evenement/spectacle-face-aux-collines-du-rwanda


Conférence : Quelles sont les conclusions du nouveau rapport du GIEC ?
-----------------------------------------------------------------------------

- https://www.ici-grenoble.org/evenement/conference-quelles-sont-les-conclusions-du-nouveau-rapport-du-giec


.. figure:: images/gerhard_krinner.png
   :align: center

   https://www.ici-grenoble.org/evenement/conference-quelles-sont-les-conclusions-du-nouveau-rapport-du-giec


À l'occasion de la sortie du nouveau rapport de synthèse du Groupe d’Experts Intergouvernemental
sur l’évolution du Climat, conférence avec l’un des ses auteurs, Gerhard Krinner,
modélisateur du climat polaire à l’Institut des Géosciences et l’Environnement (IGE/OSUG).

La conférence sera divisée en deux temps : un premier avec l’explication
des conclusions du rapport et le deuxième, un temps de questions.

À 18h

À EVE
701 avenue centrale
Campus de Saint-Martin-d'Hères



Demosphere 38
=================

.. figure:: images/demo_2023_04_05.png
   :align: center

   https://38.demosphere.net/



Mercredi 5 avril 2023 à 10h Formation premiers secours en manif
---------------------------------------------------------------------

- https://38.demosphere.net/rv/981


Lieu : Campus SMH

ARSH (Campus de Grenoble)

Formation premiers secours en manif devant l'ARSH, mercredi 5 avril à 10h.

Attention : il ne s'agit pas d'une formation PSC1.

Cette formation est un échange autour des bonnes pratiques et des réflexes
à avoir face à la violence physique et psychologique en manifestation
et dans le cadre du militantisme.



Atelier de déplacement collectif en situation de manifestation, d'action militante ou de blocage # 2
------------------------------------------------------------------------------------------------------

- https://38.demosphere.net/rv/888

Mercredi 5 avril 2023 - 18h - Durée environ 2h30 - Prix Libre

Lieu: Centre Social Autogéré Tchoukar, 38 rue d'Alembert, Grenoble - Tram A et B - Arrêt : Saint Bruno

Cette formation consistera à partager des pratiques permettant, dans la
mesure du possible, de faire face à certaines problématiques auxquelles
nous sommes confrontés lors de manif, d'action ou de blocage (tensions
physiques et psychologique, inquiétude, frayeur, insécurité, manque ou
absence de solidarité, etc).

Nous apprendrons à se déplacer de manière solidaire, bienveillante et
en s'aidant mutuellement. Nous privilégierons l'organisation collective.

On vous proposera des techniques pour gagner en confiance en soi et dans
le groupe, pour apprendre à se détendre et à se sentir rassuré, etc.

On essaiera de sortir de notre zone de confort sans se mettre trop en
difficulté.

N'hésitez pas à venir nous voir en cas de problème.

Nous travaillerons beaucoup sur la respiration, la détente du corps, sur
l'aspect sensori-moteur (Qui concerne à la fois les phénomènes sensoriels
et l'activité motrice, ou leurs interactions), la psychomotricité
(Ensemble des fonctions motrices considérées sous l'angle de leurs relations
avec le psychisme).

Concrètement, l'apprentissage s'articulera autour d'outils pour :

• Faire baisser le stress et les tensions corporelles,

• Dépasser ses craintes et ses peurs,

• Créer de la cohésion et de l'entraide dans le groupe,

• Diffuser des pratiques de sécurité individuelle et collective,

• Dans la mesure du possible, d'essayer d'éviter les arrestations,

On s'efforcera de s'adapter à vos besoins et vos envies.

Nous n'avons pas la prétention de donner de solution clefs en main, libre
à toi d'utiliser ou pas ces techniques, de te les approprier, de les
améliorer.

Cette formation est probablement incomplète et certainement perfectible.

Nos connaissances dans ce domaine s'appuient sur l'autoformation et des
savoirs expérientiels.

Nous ne sommes pas des expert·es !

On vous demandera donc d'être indulgent·e. L'organisation de cet atelier
se base sur une participation active et responsable de chacun·e (autogérée).

La séance évolura progressivement de simple à difficile.

On vous demandera : de rester concentré tout au long de cet atelier,
d'écouter attentivement les directives, d'être bienveillant mutuellement,
de faire preuve d'entraide.

Nous n'accepterons pas les comportements compétitifs, ni agressifs.

Nous vous conseillons de venir avec une tenue adaptée, dans laquelle
vous vous sentez à l'aise et une bouteille d'eau pour vous désaltérer.

Attention :

Les postures autoritaires ne sont pas les bienvenues, ainsi que les attitudes
et les remarques à caractère raciste, sexiste, homophobe, transphobe, agiste, validiste, etc
