
.. _actions_2023_04_07:

=========================================================================================================================================
**Actions du vendredi 7 avril 2023**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_04_07.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/le-7-avril-1994-le-debut-du-genocide-des-tutsis-au-rwanda
- https://www.ici-grenoble.org/evenement/chantier-collectif-prenons-les-terres-dans-lagglo-grenobloise
- https://www.ici-grenoble.org/evenement/formation-antirepression-avancee
- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/rencontre-avec-lautrice-de-une-histoire-des-produits-menstruels-sur-le-dressage-des-corps-feminins
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-de-soutien-a-kontrosol-centre-social-autogere-lgbtqi-en-grece
- https://www.ici-grenoble.org/evenement/soiree-de-luttes-contre-la-destruction-des-terres-partout-en-france

Demosphere 38
=================

.. figure:: images/demo_2023_04_07.png
   :align: center

   https://38.demosphere.net/

