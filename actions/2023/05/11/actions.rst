
.. _actions_2023_05_11:

=========================================================================================================================================
**Actions du jeudi 11 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #GrevueDePresse**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-05-11 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #GrevueDePresse #100JoursDeZbeul #IntervillesDuZbeul


Grevue de presse : Les 10 infos de mai
===========================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai
- https://www.ici-grenoble.org/newsletter

.. figure:: images/grevue_de_presse_mai.png
   :align: center

   https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai, https://www.ici-grenoble.org/newsletter


Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous
intéressent ?

Chaque mois, le média ici Grenoble vous propose la ''Grevue de presse'',
une sélection d'infos courtes et percutantes glanées dans la presse
locale ou dans nos réseaux.

Au menu de mai : une mégabassine dans le Trièves, des mégapompeurs dans le
Grésivaudan, un drone russe "made in Isère", la clinique mutualiste dans
une pyramide de Ponzi, la fin de la BAF, le début du BOCAL et plein
d'autres infos.

Bonne lecture !



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_11.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/debat-la-militarisation-et-la-privatisation-de-lexploitation-de-lespace (Dans le cadre des Rencontres de Géopolitique)
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 12h00 https://www.ici-grenoble.org/evenement/fete-des-10-ans-de-la-permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- 14h00 https://www.ici-grenoble.org/evenement/permanence-pour-les-personnes-victimes-ou-temoins-de-discriminations-racistes-sexistes-lgbtqiphobes
- 14h30 https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- https://www.ici-grenoble.org/evenement/la-paix-un-processus-conflictuel-la-colombie-en-exemple-atelier
- 20h00 https://www.ici-grenoble.org/evenement/concert-de-megadef-en-soutien-a-la-remise-aux-normes-du-38-rue-dalembert
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 21h00 https://www.ici-grenoble.org/evenement/theatre-les-guerilleres

Demosphere 38
=================

.. figure:: images/demo_2023_05_11.png
   :align: center

   https://38.demosphere.net/

- |important| https://38.demosphere.net/rv/1173 (Soirée projection et scène ouverte en soutien à la caisse de grève de l'AG féministe, EVE, Espace vie étudiante - Campus SMH)


|rebellion| Extinction rebellion
=====================================

- https://mobilizon.chapril.org/events/40c78f49-cf02-4045-b2cb-9c6dec3a3496


Formation médiation en action par XR

Une initiation pour être Peace keeper / médiateur·ice organisée par
XR Grenoble et Alternatiba Grenoble aura lieu le jeudi 11 mai.

La médiation en action, c’est faire descendre la tension et permettre la
desescalade de la violence avec les personnes autour d'une action de
désobéissance civile.

Cette formation aborde tous les rudiments du rôle indispensable de
"Peace keeper / médiateur·ice" (attitude, discours, argumentation...)

Merci de remplir ce formulaire pour vous y inscrire :

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Le lieu et les horaires précis de la formation vous seront reconfirmés par email.



|casserolade| Annonce #zbeul du vendredi 12 mai 2023 #100JoursDeZbeul #IntervillesDuZbeul
===============================================================================================

- https://38.demosphere.net/rv/1172 (ACCUEIL DE VÉRAN Vendredi 12 Mai  2023, de 14h à 20h)
- https://100joursdezbeul.fr/
- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations

.. figure:: ../12/images/zbeul_2023_05_12.png
   :align: center

   https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations


