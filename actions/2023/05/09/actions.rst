.. index::
   pair: Ecologie; Anticapitalisme
   pair: Ukraine ; Solidarité

.. _actions_2023_05_09:

==========================================================================================================================================================
**Actions du mardi 9 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Ecologie #Anticapitalisme #SolidariteUkraine**
==========================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Ecologie #Anticapitalisme #SolidariteUkraine


|ici_grenoble| Page une ici-grenoble **L'AG écolo anticapitaliste**
=====================================================================

- https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste

.. figure:: images/page_une_ici_grenoble_2023_05_09.png
   :align: center

   https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_09.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- 18h00 https://www.ici-grenoble.org/evenement/binary-is-for-computer-permanence-informatique-a-la-b-a-f-depannages-installation-de-linux-cryptage-de-donnees
- |important| 18h30 https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste
- |important| |solidarite| 🇺🇦 19h00 https://www.ici-grenoble.org/evenement/rencontres-la-gauche-ukrainienne-face-a-la-guerre
- 19h30 🎥 https://www.ici-grenoble.org/evenement/cinema-ni-les-femmes-ni-la-terre-sur-les-luttes-ecofeministes-en-argentine-et-en-bolivie
- 19h30 https://www.ici-grenoble.org/evenement/mardi-contre-la-francafrique-la-situation-politique-en-algerie
- 19h30 https://www.ici-grenoble.org/evenement/rencontres-feministes-autour-de-lessai-faire-famille-autrement-sur-les-familles-queer-et-les-parentalites-trans
- 20h50 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 21h00 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines

Demosphere 38
=================

.. figure:: images/demo_2023_05_09.png
   :align: center

   https://38.demosphere.net/

- 19h00 🎥 https://38.demosphere.net/rv/1155 (Projection "DEBOUT LES FEMMES !" (de François Ruffin et Gilles Perret, Salle des Fêtes, 15 rue Vicor Hugo, 38610 Gières)
  Prix libre, au profit de la CAISSE DE GRÈVE de l'Éducation Nationale de l'Isère : www.caisse-solidarite-educ38.org
  Un documentaire qui met à l'honneur les travailleuses précaires de métiers essentiels à notre société :
  Deux députés partent à la rencontre de femmes de ménage, d'auxiliaires de vie, d'assistantes d'enfants en situation de handicap, etc.


.. _ag_ecologie_anticapitaliste_2023_05_09:

2023-05-09 Voici l'invitation à une Assemblée Générale Écolo Anticapitaliste à Grenoble
===========================================================================================

- https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste

"Cette assemblée générale a vu le jour à l'occasion d'une manifestation
d'opposition à la mascarade Grenoble Capitale Verte européenne 2022.

Nous sommes composé·es d'organisations, de collectifs, de partis politiques,
mais aussi d'individus, qui ensemble, souhaitons porter des revendications
écologiques anticapitalistes.

L'écologie et l'anticapitalisme sont pour nous indissociables : le capitalisme
se fonde sur l'accumulation, et donc l'exploitation, de ressources qui se
font de plus en plus rares, au profit d'entreprises ou de patrons qui
s'enrichissent de plus en plus sur le dos des travailleur·euses et des
personnes précaires.

Il est impossible de croire en un capitalisme vert pour nous sauver de la
crise écologique : les solutions techniques cherchent en réalité à ouvrir
de nouveaux marchés lucratifs sans remettre en question le principe-même
du capitalisme qui est à la racine des problèmes environnementaux.

Nous avons besoins de retrouver une autonomie et une souveraineté
collective pour pouvoir faire des choix de société cohérents avec les
limites planétaires.

Or cela est inconcevable si les décisions économiques et politiques sont
prises par une classe dont les intérêts divergent de ceux du bien commun.

Combattre le capitalisme c'est aussi faire la critique d'une idéologie
propagée par les classes dominantes qui promeut la consommation et un
mode de vie débridé comme marqueurs de réussite sociale.

Nous souhaitons lutter contre cette vision du monde et ces normes qui
doivent êtres changées pour aller vers une sobriété qui ne soit pas
subie mais désirée car profitable à tout le monde.

Nous avons fait le choix de nous organiser en AG pour permettre à des
individus aussi bien qu'à des personnes mandatées par leur collectif
de rejoindre l'organisation.

Ce cadre unitaire nous permet d'avoir plus de force pour mener cette lutte
transversale, et de rattacher et mêler des causes diverses qui ne se
croisent pas facilement dans d'autres espaces.

Le but n'est pas de remplacer les personnes déjà en lutte mais d'être un
outil de soutien et de renforcement.

Nos opinions peuvent diverger mais nous nous rejoignons sur ces revendications
que nous souhaitons porter à chacune de nos apparitions publiques.

Nous revendiquons :

- un accès mutualisé et inconditionnel à tous les besoins vitaux pour tous·tes.
  Ce sont les plus riches qui polluent et consomment le plus, impactant
  les personnes les plus précaires qui se retrouvent privées des ressources
  nécessaires à leur survie (eau, nourriture, logement, santé, transport, etc.).

- des transports en commun gratuits et la suppression des ZFE qui
  précarisent encore plus

- la réquisition des logements vides et la rénovation des logements sans
  hausse de loyer, notamment les moins bien isolés la fin de l’artificialisation des sols

- la fin des grands projets urbains écologiquement condamnables (l'industrie
  du bâtiment étant très polluante) et qui ne répondent qu'à des logiques de croissance et de rayonnement économiques. D'autant plus quand ces projets s'accompagnent d'une gentrification des quartiers.

Pour cela, nos moyens d'action sont :

- le renforcement et le soutien des luttes locales

- la construction d'un cadre unitaire large d'organisations, d'individus,
  de collectifs qui permet de partager, échanger, et construire des
  revendications communes et unies

- l'apparition lors de manifestations ou l'action conjointe pour construire
  un rapport de forces sur le territoire

- le relai d'informations

Rejoins-nous et viens nous aider à construire la lutte !"

À 18h30
Au Centre Social « Tchoukar »
38 rue d'Alembert
Grenoble
https://www.openstreetmap.org/#map=19/45.18799/5.71098



.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.7092079520225525%2C45.18721866129755%2C5.712748467922212%2C45.188766878453244&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.18799/5.71098">Afficher une carte plus grande</a></small>

.. _ukraine_2023_05_09:

2023-05-09 Rencontres : La gauche ukrainienne face à la guerre |solidarite|
==============================================================================

- https://www.ici-grenoble.org/evenement/rencontres-la-gauche-ukrainienne-face-a-la-guerre
- https://www.openstreetmap.org/#map=19/45.17938/5.70815

.. figure:: images/ukraine_solidarite.png
   :align: center

   https://www.ici-grenoble.org/evenement/rencontres-la-gauche-ukrainienne-face-a-la-guerre

Rencontre avec Anastasia Riabchuk, Académie Mohyla-Kyïv, chercheuse accueillie
en France dans le cadre du projet PAUSE, membre du comité de rédaction
de "Commons : Journal of Social Criticism" et militante.

Anastasia présentera les réactions des gauches ukrainiennes à la guerre
que la Russie mène contre l’Ukraine depuis 2014 et les initiatives
d’entraide et de solidarité |solidarite| qui ont été mises en place en Ukraine
depuis le début de l’agression en février 2022.

Elle évoquera en outre les débats intenses aujourd’hui en Ukraine sur
les enjeux entourant la reconstruction du pays. La discussion sera
animée par Perrine Poupin et Olga Bronnikova.

Ouverture : 19h
Présentation : 20h
Prix libre.

À Antigone
22 rue des violettes
Grenoble

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.706380903720857%2C45.17860912530213%2C5.709921419620515%2C45.18015757661131&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.17938/5.70815">Afficher une carte plus grande</a></small>


