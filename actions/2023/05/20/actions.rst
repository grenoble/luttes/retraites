
.. _actions_2023_05_20:

=========================================================================================================================================================
**Actions du samedi 20 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RassemblementDesGlieres #FemmeVieLiberté**
=========================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RassemblementDesGlieres #FemmeVieLiberté


|ici_grenoble| **Agenda ici-grenoble**
===========================================

.. figure:: images/ici_grenoble_2023_05_20.png
   :align: center

   https://www.ici-grenoble.org/agenda


- |crha| https://www.ici-grenoble.org/evenement/rassemblement-des-glieres-conferences-films-concerts-de-resistances
- 10h00 https://www.ici-grenoble.org/evenement/atelier-construire-une-remorque-a-velo
- |important| 14h30 |JinaAmini| https://www.ici-grenoble.org/evenement/rassemblement-contre-le-regime-meurtrier-du-gouvernement-islamique-diran
- |important| 15h00 |mriya| https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- 19h15 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 20h40 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 22h00 ❤️ https://www.ici-grenoble.org/evenement/coup-de-coeur-les-coulisses-de-la-revue-la-hulotte



.. _glieres_2023_05_20:

Samedi 20 mai 2023 **Rassemblement des Glières à Thorens-Glières**
====================================================================================================

- https://citoyens-resistants.fr/spip.php?article659
- https://citoyens-resistants.fr/IMG/pdf/tract_recto_verso_impression_crha_2023.pdf
- https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf

.. figure:: ../19/images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842



|JinaAmini| Samedi 20 mai 2023 toute la journéee à la MJC Exposition **Femme, Vie, Liberté** en soutien au peuple iranien #FemmeVieLiberté ♀️✊ 📣
-----------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.instagram.com/iranluttes/
- https://iran.frama.io/luttes/index.html
- https://iran.frama.io/exposition/
- :ref:`iran_luttes:iran_luttes`


En soutien au peuple iranien. Sélection d’affiches



- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
- 🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)


Samedi 20 mai 2023 toute la journéee à la MJC **Photos "enfant perdue de la psyché"**
-----------------------------------------------------------------------------------------------------------------

Photos "enfant perdue de la psyché".


Samedi 20 mai 2023 **10h00, Tom Morel : Les politiques de la gestion de l’eau**
----------------------------------------------------------------------------------

bilan des impasses actuelles- les choix indispensables pour l’avenir,
avec Julie Trottier ; Nicolas Girod ; Valérie Paumier ; Julien Le Guet


Samedi 20 mai 2023 **14h00, Tom Morel : L’écoféminisme, combat d’aujourd’hui** ️✊ ♀️
-----------------------------------------------------------------------------------------------

**L’écoféminisme, combat d’aujourd’hui** avec:

- Sandrine Rousseau (https://nitter.moomoo.me/sandrousseau/rss)
- Corinne Morel Darleux (https://piaille.fr/@cmoreldarleux)
- Pinar Selek  (https://pinarselek.fr/biographie/, https://nitter.cutelab.space/Pinar_Selek/rss)
- Camille Bajeux


Samedi 20 mai 2023 **14h00, MJC : Extrême droite et fascisation de la société, de quoi parle t-on, comment lutter ?**
---------------------------------------------------------------------------------------------------------------------------

Extrême droite et fascisation de la société, de quoi parle t-on, comment lutter ?


Avec :

- Sophie Djigo
- Ugo Palheta
- La Horde


Samedi 20 mai 2023  **14h00, Chapiteau :Palestine, apartheid et récit interdit**
------------------------------------------------------------------------------------------------------------------------

Palestine, apartheid et récit interdit avec Salah Hamouri ; Anne Tuaillon


|important| ✊🏼✊🏽✊🏾✊🏿 Samedi 20 mai 2023 **17h00, Tom Morel : FORUM : État des lieux des Résistances, pour construire demain !** ️✊ ♀️
-------------------------------------------------------------------------------------------------------------------------------------------------

Avec la participation de:

- Pinar Selek,
- Sandrine Rousseau,
- Julien Le Guet,
- Nicolas Girod,
- Julie Trottier,
- Nina Faure,
- Yannick Kergoat,
- Sophie Djigo,
- Nadia Salhi,
- etc...

Animé par Gilles Perret (https://uk.unofficialbird.com/Gilles_Perret/rss)


Samedi 20 mai 2023 **17h00, Chapiteau : Urgence climatique : pourquoi la politique de l’autruche ?**
----------------------------------------------------------------------------------------------------------

avec:

- Catherine Larrère
- Raphaël Larrère
- Stéphane Labranche
- (Dominique Bourg absent)




Samedi 20 mai 2023 **20h30  Salle Tom Morel Soirée festive engagée avec les Valientes Gracias**
-------------------------------------------------------------------------------------------------

- https://vimeo.com/795545511?embedded=true&source=vimeo_logo&owner=5215144

Le groupe Valientes Gracias propose une cuisine musicale aux épices
féministes, qui puise dans les rythmes afro-colombiens pour chanter les
luttes pour l’égalité et dénoncer les oppressions et les dominations.

Dans un univers scénique ou la présence des femmes reste minime, dans
cette société dont la perte de sens et de perspectives s’accélère, la
musique festive et les paroles du groupe font du bien aux corps, aux cœurs
et aux esprits.
