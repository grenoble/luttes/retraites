
.. _actions_2023_05_03:

=========================================================================================================================================
**Actions du mercredi 3 mai 2023 #Grenoble #64AnsCestToujoursNon #SeuleLaLuttePaie #ExtinctionRebellion #Solidarité**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie #ExtinctionRebellion #Solidarité

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_03.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-3-mai-1968-mai-68-demarre
- |important| |rebellion| https://www.ici-grenoble.org/evenement/action-dextinction-rebellion-pollution-secrete-a-grenoble
- 11h30 : https://www.ici-grenoble.org/evenement/pique-nique-revendicatif-et-festif-des-etudiant-es-docellia-en-lutte
- |important| 13h00 : https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-interpelle-e-s-du-1er-mai
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 14h30 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 16h00 : https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 17h00 |important| |car38| https://www.ici-grenoble.org/evenement/atelier-telephonie-mobile-et-activisme-peut-on-melanger-les-deux

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 18h20 🎥 : https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- |important| 19h00 : https://www.ici-grenoble.org/evenement/velorution-feministe-de-mai
- 19h00 https://www.ici-grenoble.org/evenement/vernissage-de-lexposition-patriarchie-magazine-faux-magazine-satirico-feministe
- 20h45 🎥 : https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative


|rebellion| Extinction Rebellion
===================================

- https://mobilizon.chapril.org/@xr_grenoble@mobilizon.extinctionrebellion.fr/feed/atom
- https://mobilizon.chapril.org/@xr_grenoble@mobilizon.extinctionrebellion.fr/events?showPassedEvents=false


.. figure:: images/mobilizon.png
   :align: center

- |important| |rebellion| https://www.ici-grenoble.org/evenement/action-dextinction-rebellion-pollution-secrete-a-grenoble


Demosphere 38
=================

.. figure:: images/demo_2023_05_03.png
   :align: center

   https://38.demosphere.net/

- |important| 11h30 https://38.demosphere.net/rv/1154 (Pique nique revendicatif et festif des étudiant-es d'Ocellia en lutte ! )
- 13h00 |car38| https://38.demosphere.net/rv/1157 (Rassemblement de soutien aux interpellé-es du 1er mai - Tribunal)
- 17h00 |car38| https://38.demosphere.net/rv/1047 (TELEPHONIE MOBILE ET ACTIVISME peut-on mélanger les deux ?)


Articles
===========

.. _xr_grenoble_2023_05_03:

2023-04-03 |rebellion| XR Grenoble bloque la Banque Nationale des Pollueurs
-------------------------------------------------------------------------------

- https://extinctionrebellion.fr/actions/2023/05/03/xr-grenoble-bloque-la-banque-nationale-des-pollueurs.html
- https://extinctionrebellion.fr/campagnes/carnage-total/
- https://www.ledauphine.com/environnement/2023/05/03/isere-grenoble-lutte-contre-l-inaction-climatique-qui-sont-les-scientifiques-en-rebellion
- https://nitter.cutelab.space/Xr38G/status/1653768249572741120#m


Action "Pollution secrète" face à la BNP sur #Grenoble avec @alternatibagre
@attac_isere @Greenpeace_Gre @SciRebFr @XRChambery | #CarnageTotal #BanqueNationaleDesPollueurs


🔴 Action "Pollution secrète" face à la BNP sur #Grenoble

Le mercredi 3 mai 2023, une soixantaine de militant·e·s de @xrgrenoble ,
@scientifiques_en_rebellion et plein d'autres mouvements sont devant
la BNP Paribas de la place Victor Hugo à #Grenoble

Les militant·e·s dénoncent le financement des énergies fossiles par la BNP 🛢️🤑🛢️

🎯 L'action se déroule dans le cadre de la campagne #carnageTotal / #StopEACOP
contre les financeurs du projet #EACOP

👩‍🔬 Cette campagne de @ScientistRebel1 / @SciRebFr plaide aussi en faveur
de mesures urgentes accompagnant une contraction de l'économie de façon
socialement juste, écologiquement et économiquement durable !

#UrgenceClimatique

🎯 L'action se déroule dans le cadre de la campagne #carnageTotal / #StopEACOP
contre les financeurs du projet #EACOP, en l’occurrence l’agence bancaire
@BNPParibas de #Grenoble place Victor Hugo 🗺️

Pour en savoir plus sur la campagne : https://extinctionrebellion.fr/campagnes/carnage-total/

- https://nitter.cutelab.space/Xr38G/status/1653775521459699713#m

⛓️ Les militant·e·s bloquent aussi l'entrée et occupent le croisement
routier à proximité pour dénoncer le financement des énergies fossiles
par la BNP = #BanqueNationaleDesPollueurs 🛢️🤑🛢️

#CarnageTotal

- https://nitter.cutelab.space/Xr38G/status/1653777941711470593#m

👩‍🔬 Pour @SciRebFr , cette action s'inscrit dans la campagne internationale
menée ce mois de mai par @ScientistRebel1 et ayant pour thème 👇
"THE SCIENCE IS CLEAR" (la science est claire)

- https://nitter.cutelab.space/Xr38G/status/1653778793616621568#m

👩‍🔬 Cette campagne de @ScientistRebel1 se base sur des éléments chiffrés
récents qui soulignent le poids des industries fossiles dans les émissions
de CO2, les impacts associés (limites planétaires), les très fortes
inégalités à la fois territoriales et en fonction des revenus.


- https://nitter.cutelab.space/Xr38G/status/1653793002169733122#m

Die-In face à la BNP sur #Grenoble avec @alternatibagre @attac_isere
@Greenpeace_Gre @SciRebFr @XRChambery | #CarnageTotal #BanqueNationaleDesPollueurs


- https://nitter.cutelab.space/Xr38G/status/1653802556257185796#m

Action terminée !

Merci d'avoir suivi ce [THREAD], et n'hésitez pas à suivre la campagne #CarnageTotal 🛢️

- https://nitter.cutelab.space/Xr38G/status/1653807902195429387#m

Épilogue avec @Engrainagemedia de l'action face à la BNP = #BanqueNationaleDesPollueurs 🛢️🤑🛢️
🪦 Die-In représentant les morts associés aux projets écocidaires comme #EACOP 👇

#CarnageTotal
#StopEACOP



.. solidarite_2023_05_03:

2023-05-03 Rassemblement de soutien aux deux personnes interpellées en marge de la manifestation du 1er mai à Grenoble
------------------------------------------------------------------------------------------------------------------------------

- https://www.placegrenet.fr/2023/05/03/rassemblement-de-soutien-aux-deux-personnes-interpellees-en-marge-de-la-manifestation-du-1er-mai-a-grenoble/601988

Un rassemblement s’est tenu devant le palais de justice de Grenoble mercredi 3 mai 2023.

Les deux personnes interpellées en marge de la manifestation du 1er mai
devaient en effet comparaître devant le juge des libertés et de la détention.

Mais pas en comparution immédiate, comme le pensait initialement le
collectif anti-répression Isère, venu les soutenir.
