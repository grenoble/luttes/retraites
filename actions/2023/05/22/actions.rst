
.. _actions_2023_05_22:

=========================================================================================================================================
**Actions du lundi 22 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Pause**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Pause


|ici_grenoble| A la une **Les 10 infos de mai**
=====================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai
- https://www.ici-grenoble.org/newsletter


.. figure:: ../10/images/page_une_ici_grenoble_2023_05_10.png
   :align: center

   https://www.ici-grenoble.org/article/les-10-infos-de-mai, https://www.ici-grenoble.org/newsletter

Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous
intéressent ?

Chaque mois, le média ici Grenoble vous propose la ''Grevue de presse'',
une sélection d'infos courtes et percutantes glanées dans la presse
locale ou dans nos réseaux.

Au menu de mai : une mégabassine dans le Trièves, des mégapompeurs dans le
Grésivaudan, un drone russe "made in Isère", la clinique mutualiste dans
une pyramide de Ponzi, la fin de la BAF, le début du BOCAL et plein
d'autres infos.

Bonne lecture !

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_22.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h00 https://www.ici-grenoble.org/evenement/permanence-du-garage-associatif-les-soupapes-reparer-sa-voiture
- 14h00 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 19h00 https://www.ici-grenoble.org/evenement/rencontres-les-classes-dominantes-sont-elles-vraiment-ecolo


