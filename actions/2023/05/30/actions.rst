.. index::
   pair: SoulevementDeLaTerre; 2023-05-30

.. _actions_2023_05_30:

=========================================================================================================================================================================================================
**Actions du mardi 30 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AGRetraites #PreparerLe6Juin #SoulevementsDeLaTerre #NoTAV #StopTELT #notav #lyonturin**
=========================================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-05-30 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AGRetraites #PreparerLe6Juin #SoulevementsDeLaTerre #NoTAV #StopTELT #notav #lyonturin
===============================================================================================================================================================================



|ici_grenoble| A la une **Préparer le 6 juin**
=====================================================================


.. figure:: images/page_une_ici_grenoble_2023_05_30.png
   :align: center

   https://www.ici-grenoble.org/article/preparer-le-6-juin, https://www.ici-grenoble.org/newsletter


Malgré la répression policière et les multiples contre-feux médiatiques,
la lutte contre la réforme des retraites continue.

L'intersyndicale appelle à une nouvelle journée d'actions mardi 6 juin 2023,
avec une manifestation à 10h au départ de la gare de Grenoble.

Quelles actions préparer ce jour-là ? Quelles leçons tirer de la répression
du 1er mai 2023 ? Comment relancer les luttes ?

Ce mardi 30 mai 2023 à 18h30 au 38 rue d'Alembert, c'est l'Assemblée Générale
de lutte contre la réforme des retraites.
Venez réfléchir et organiser la suite du mouvement.




**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_30.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h00 https://www.ici-grenoble.org/evenement/this-is-the-end-demenagement-de-la-baf
- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- 16h00 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- 18h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- |important| 19h00 |sdterre| https://www.ici-grenoble.org/evenement/reunion-des-soulevements-de-la-terre-grenoble
- 20h50 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques



2023-05-30 19h00 |sdterre| Réunion des Soulèvements de la Terre Grenoble
===========================================================================

- https://www.ici-grenoble.org/evenement/reunion-des-soulevements-de-la-terre-grenoble
- https://lessoulevementsdelaterre.org/
- https://www.youtube.com/@lessoulevementsdelaterre1099/videos
- https://www.youtube.com/watch?v=A6j8unxb0xs (#NoTAV DOCUMENTAIRE | Le 17 Juin 2023, la montagne se soulève)
- https://www.instagram.com/soulevements.de.la.terre/
- sdt-grenoble@proton.me

.. figure:: images/sdt_2023_05_30.png
   :align: center

Rejoignez les membres du comité grenoblois des Soulèvements de la Terre,
qui se retrouvent pour faire un point sur les actions à venir, en
particulier celle de juin contre le tunnel alpin du Lyon-Turin.

À propos du Lyon-Turin, nous vous recommandons ce nouveau reportage à
propos des ravages du projet : https://www.youtube.com/watch?v=A6j8unxb0xs

Cette réunion des Soulèvements de la Terre permettra :

- A nos groupes de travail de présenter leurs avancées, besoins et projets.
- Aux nouveaux.elles arrivant.e.s de s'informer sur notre fonctionnement
  et/ou de rejoindre le groupe de leur choix.

N'hésitez pas à nous contacter par mail pour toute question : sdt-grenoble@proton.me

À 19h

Au 38 rue d'Alembert
Grenoble

#NoTAV DOCUMENTAIRE | Le 17 Juin 2023, la montagne se soulève
--------------------------------------------------------------------

- https://www.youtube.com/watch?v=A6j8unxb0xs

💥17 & 18 JUIN : mobilisation internationale et populaire dans la vallée
de la Maurienne (73) pour l’arrêt du chantier du Lyon-Turin !

❓Depuis plus de 30 ans, un projet de chantier ferroviaire titanesque,
impliquant le forage de 260 km de galeries à travers les Massifs Alpins,
anime l'imaginaire mégalo et détraqué du consortium TELT, «Tunnel Euralpin
Lyon Turin» allié de décisionnaires politiques «visionnaires» et de groupes
tels que Vinci Bouygues ou Eiffage.

Bien que le transport de marchandises stagne depuis 1994, que la ligne
existante ne soit utilisée qu'à 20% de sa capacité de fret, TELT envisage
de creuser 11 tunnels, dont le plus grand d’Europe, le «Tunnel de Base»
de 57km.
Et tout cela pour faire gagner aux voyageur.se.s et aux marchandises
seulement 1h25 entre Paris et Milan…

🤝 Cette lutte dépasse les frontières !
En Italie, le mouvement populaire NO TAV @notav.info se bat depuis 30 ans
pour préserver sa vallée, ses montagnes et la vie qui y foisonne et ce
malgré une violente répression et une drastique militarisation des
territoires.

Aujourd'hui, dans la vallée de la Maurienne et en Val di Susa, les travaux
préparatoires du tunnel de base ont débuté.
Alors mettons un coup d'arrêt à ce projet, avant que travaux et dégâts
engendrés ne soient irrémédiables !

#notav #lyonturin #maurienne #valdisusa

