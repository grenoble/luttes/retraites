
.. _actions_2023_05_31:

=========================================================================================================================================================================================================
**Actions du mercredi 31 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #resistances #16IdeesDeRencontresEstivales #AlterTour2023**
=========================================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-05-31 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #resistances #16IdeesDeRencontresEstivales #AlterTour2023
===============================================================================================================================================================================



|ici_grenoble| A la une **16 idées de rencontres estivales**
=====================================================================

- https://festival-resistances.fr/
- https://campclimat.eu/camps-climat-2023/
- http://www.altercampagne.net/
- https://chantierduvielaudon.wordpress.com/venir-au-chantier-cet-ete/
- https://www.reprisesdesavoirs.org/chantiers-pluri%c2%b7versites/#chantiers2023
- http://lagrandemarche.org/
- http://amies.revuesilence.net/
- https://lasevefestival.fr/
- http://lesresistantes2023.fr/
- https://ladecroissance.xyz/2023/05/23/festives-2023/
- http://www.festival-douarnenez.com/fr/
- http://www.lussasdoc.org/
- https://www.uemss.org/
- https://bureburebure.info/events/event/terres-et-communs-rencontres-des-luttes-paysannes-et-rurales/
- https://nouveaupartianticapitaliste.org/ue2023
- https://blog.lesenfantsdabord.org/grande-rencontre-dete-leda-aout-2023/

.. figure:: images/page_une_ici_grenoble_2023_05_31.png
   :align: center

   https://www.ici-grenoble.org/article/16-idees-de-rencontres-estivales, https://www.ici-grenoble.org/newsletter

C'est bientôt l'été.. Une période idéale pour prendre du recul, faire de
nouvelles rencontres, explorer des lieux alternatifs, rejoindre des actions
collectives ou préparer la "rentrée sociale".

Chaque été, le média ici Grenoble répertorie de nombreuses rencontres
estivales un peu partout en France : des vélorutions, des manifestations,
des visites de lieux alternatifs, des festivals de films engagés...

Voici déjà 16 propositions :

- 🎥 Le `Festival de cinéma "Résistances" <https://festival-resistances.fr/>`_ du 7 au 15 juillet à Foix
- Le `Camp Climat Rhône <https://campclimat.eu/camps-climat-2023/>`_  les 7 et 8 juillet près de Lyon
- L'`Altertour à vélo <http://www.altercampagne.net/>`_ du 9 juillet au 19 août de Montluçon à Besançon
- Les `Chantiers écologiques d'été du Viel Audon <https://chantierduvielaudon.wordpress.com/venir-au-chantier-cet-ete/>`_ du 11 juillet au 26 août en Ardèche
- Le `Chantier éco-féministe Minervois <https://www.reprisesdesavoirs.org/chantiers-pluri%c2%b7versites/#chantiers2023>`_ du 12 au 16 juillet dans l'Hérault
- Les `Universités d'été antinucléaires  <http://lagrandemarche.org/>`_ du 13 au 29 juillet près de Dieppe
- Le `Campement autogéré et écolo des ami-e-s de la revue Silence <http://amies.revuesilence.net/>`_ du 22 juillet au 6 août en Dordogne
- Le `Festival écoféministe "La Sève" <https://lasevefestival.fr/>`_ du 27 au 30 juillet aux Tanneries de Dijon
- Les `Rencontres des luttes locales et globales "Les Résistantes" <http://lesresistantes2023.fr/>`_ du 3 au 6 août sur le plateau du Larzac
- Les `Festives de la décroissance <https://ladecroissance.xyz/2023/05/23/festives-2023/>`_ du 8 au 13 août dans le Gers
- 🎥 Le `Festival de cinéma de Douarnenez <http://www.festival-douarnenez.com/fr/>`_ sur les premiers peuples d'Amérique du Nord du 19 au 26 août en Bretagne
- 🎥 Le `Festival du film-documentaire de Lussas <http://www.lussasdoc.org/>`_ du 20 au 26 août en Ardèche
- L'`Université d'été des mouvements sociaux <https://www.uemss.org/>`_ et des solidarités du 23 au 27 août à Bobigny
- Les `Rencontres des luttes paysannes <https://bureburebure.info/events/event/terres-et-communs-rencontres-des-luttes-paysannes-et-rurales/>`_ du 26 août au 3 septembre prèsde Bure
- L'`Université d'été du NPA <https://nouveaupartianticapitaliste.org/ue2023>`_ du 27 au 30 août à Port-Leucate
- Les `Rencontres d'été des Enfants d'abord <https://blog.lesenfantsdabord.org/grande-rencontre-dete-leda-aout-2023/>`_ sur l'instruction en famille du 26 août au 2 septembre en Charente


Attention, la plupart de ces rencontres nécessitent des inscriptions
préalables, parfois plusieurs semaines voire plusieurs mois à l'avance...

Si vous avez connaissance d'autres rencontres estivales, contactez-nous ! (http://ici-grenoble.org/generalites/contact/)

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_31.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 10h00 https://www.ici-grenoble.org/evenement/this-is-the-end-demenagement-de-la-baf
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 15h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 18h30 https://www.ici-grenoble.org/evenement/appel-a-benevoles-pour-la-marche-des-fiertes-de-grenoble-prevue-le-1er-juillet
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 20h45 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative


Demosphere 38
=================

- https://38.demosphere.net/files/docs/f1475c37f470407.pdf


.. figure:: images/demo_2023_05_31.png
   :align: center

   https://38.demosphere.net/


- |important| 18h30 COLLAGE DE QUARTIER VILLENEUVE-MALHERBE, Maison des Habitant-es Le Patio 97 Gal de l'Arlequin, 38100 Grenoble



