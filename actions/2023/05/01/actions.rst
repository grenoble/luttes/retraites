
.. _actions_2023_05_01:

==============================================================================================================================================================
|rebellion| 🖤️❤️ |anar| **Actions du lundi 1er mai 2023 le 1er mai de la colère #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
==============================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`



#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


Définitions
============

Le #1erMai est la fête internationale des travailleurs et travailleuses,
une journée de lutte du mouvement social instaurée en 1889.

Le terme "Fête du travail" a été imposé par Pétain, sous Vichy, pour
effacer sa dimension sociale et conflictuelle avec le patronat.

A la base, le 1er mai est une journée de revendication salariale et syndicale,
en référence à 1886 : appel de syndicats ouvriers américains pour la journée
de 8 heures.

Cette journée a été sanglante et non "une fête" glorifiant le travail.


.. figure:: images/une_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/article/le-1er-mai-de-la-colere

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/article/le-1er-mai-de-la-colere
- https://www.ici-grenoble.org/evenement/le-1er-mai-1886-la-greve-generale-pour-la-journee-de-8-heures
- |important| 10h https://www.ici-grenoble.org/evenement/manifestation-unitaire-du-1er-mai-pour-le-retrait-de-la-reforme-des-retraites
- |important| 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 18h45 https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-autogeree-la-cagette
- 20h45 🎥  https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h45 🎥  https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines


|rebellion| Printemps des formations Extinction rebellion
==============================================================

.. figure:: images/extinction_rebellion_programme.png
   :align: center

   https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117

- |important| Lundi 1er mai (18h-21h) - Initiation à la désobéissance civile non-violente, https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117


Retrouvez ci-dessous accueils 🤗, formations 👩‍🏫, actions ✊ de #ExtinctionRebellion ⏳
📢 N'oublions pas les #manifestations du #PremierMai pour le retrait de
la #reformedeseetraites : Luttes écologiques & sociales sont liées 🤝

#Manif1ermai
#1erMai

.. figure:: images/extinction_rebellion.png
   :align: center

   https://social.rebellion.global/@xrfrance/110287575392922145


Demosphere 38
=================

.. figure:: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/

- 8h30 https://38.demosphere.net/rv/1142 (convoi trièvois pour rejoindre la manif de Grenoble)
- 9h30 https://38.demosphere.net/rv/1066 (DÉPART DU CORTÈGE DE QUARTIER POUR REJOINDRE LA MANIF DU 1ER MAI Villeneuve)
- 9h30 https://38.demosphere.net/rv/1147 (Départ en cortège de Fontaine pour la manif de Grenoble: zbeul jusqu'au retrait!)
- 10h00 https://38.demosphere.net/rv/1137 (Départ gares Alsace Lorraine Jaures Arrivée anneau de vitesse)

Tracts
=======

|anar| CNT38
---------------

- https://kolektiva.social/@cnt_38/110289383740118436
- https://ul38.cnt-f.org/2023/04/29/1er-mai-passons-le-capital-a-la-casserole/
- https://ul38.cnt-f.org/wp-content/uploads/20230501-1er-Mai-coul.pdf
- https://ul38.cnt-f.org/wp-content/uploads/2023.05.01-tract-educ-1er-mai-1.pdf

.. figure:: images/cnt38.png
   :align: center

   https://kolektiva.social/@cnt_38/110265115567316832

|anar| CNT SO
-------------------

- https://nitter.bird.froth.zone/cntso_fr/status/1652668302244577282#m
- https://cnt-so.org/unissons-nos-coleres-et-organisons-nous-pour-un-1er-mai-revolutionnaire-et-un-printemps-de-lutte/
- https://cnt-so.org/wp-content/uploads/2023/04/cnt_so_tract_conf_1mai_2.pdf

Notre affaire à tous, inégalité climatique
--------------------------------------------------

- https://notreaffaireatous.org/wp-content/uploads/2023/04/Retraites-climat-meme-combat-1.pdf
- https://notreaffaireatous.org/11-juin-2021-travail-et-changement-climatique/


Appel à la grévilla (Solidaires informatique)
-----------------------------------------------

- https://syndicat.solidaires.org/@SolInfoNat/110230520734705759
- https://solidairesinformatique.org/2023/04/20/appel-a-la-grevilla/

Solidaires Informatique appelle à la grève jusqu'au retrait de la réforme
des retraites, ou à défaut, jusqu'au 31 décembre 2027.

Le gouvernement nous force à adopter des stratégies de rupture.

Cet appel n’est pas en soit un appel à une grève reconductible sur
plusieurs années, mais vise à ouvrir de nouvelles perspectives de lutte
aux grévistes isolé·es dans leurs entreprises et aux militant·es solitaires.


100 jours de zbeul/on-ne-les-lache-pas
=========================================

- :ref:`zbeul_2023_05_01`
- https://100joursdezbeul.fr/
- https://mobilisations-en-france.cgt.fr/news/map
- https://solidaires.org/sinformer-et-agir/actualites-et-mobilisations/nationales/carte-des-manifestations-du-1er-mai-2023/
- https://www.lalutineduweb.fr/nous-allons-mettre-le-zbeul/


Attac
------

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations

Pour proposer des nouveaux rassemblements à ajouter à la carte, vous
pouvez nous contacter sur l’email : carte@attac.org

100joursZbeul
---------------

- https://100joursdezbeul.fr/
- https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493


.. figure:: ../../../../100joursdezbeul/proposer-un-evenement/images/page_formulaire.png
   :align: center

   https://framaforms.org/100-jours-de-zbeul-proposer-un-evenement-1682372493

Vous pouvez utiliser ce formulaire pour soumettre un événement ayant eu
lieu dans votre département, ou bien pour faire une réclamation sur un
événement passé -- pensez à le préciser dans le champ de commentaire.

::


    100joursdezbeul AT solidairesinformatique DOT org


Articles
===========

« Il n’y a aucun apaisement » : dans toute la France, un 1er-Mai de lutte
------------------------------------------------------------------------------

- https://www.mediapart.fr/journal/politique/010523/il-n-y-aucun-apaisement-dans-toute-la-france-un-1er-mai-de-lutte

Les manifestants se sont retrouvés en masse pour cette Journée internationale
des travailleurs et des travailleuses, où les syndicats ont affiché leur
unité pour la première fois depuis 2009. S’ils ne savent pas comment arracher
la victoire contre la réforme des retraites, ils sont encore des centaines
de milliers à tenter de le faire.


1er mai : regain de mobilisation pour un mouvement qui refuse de s’éteindre
--------------------------------------------------------------------------------

- https://rapportsdeforce.fr/classes-en-lutte/1er-mai-regain-de-mobilisation-pour-un-mouvement-qui-refuse-de-seteindre-050117867


1ER MAI : JOUR DE COLÈRE
----------------------------

- https://www.youtube.com/watch?v=9RgWdlgKJAo

Retour de flamme ou bien baroud d’honneur ?
C’était la question que posait ce 1er mai et 13ème journée de mobilisation
contre la réforme des retraites. Le cortège était massif et les heurts
très violents.


.. _obs_medics_2023_05_01:

Compte rendu de l'observatoire des Street-médics
------------------------------------------------------

- https://obs-medics.org/
- https://obs-medics.org/sabonner/
- https://obs-medics.org/a-propos

L'Observatoire des Street-médics publie son estimation corrigée des victimes
pour les manifestations du 1er Mai 2023.

- 590 victimes (blessées ou malaises)
- Dont 118 sévères (évacuées ou extraites vers les urgences)
- Un minimum de 6.000 fortes affections aux gaz lacrymogènes


.. figure:: images/compte_rendu_street_medic_2023_05_01.png
   :align: center


Ces estimation portent sur le nombre de manifestants et passants blessés
ou victimes de malaise, d'après les chiffres du Ministère de l'Intérieur
(prises en charge par les sapeurs-pompiers) corrigés pour une couverture
de 28% selon un échantillon de 165 victimes prises en charge par les secours
volontaires.

Si ces estimations ont normalement vocation à être exhaustives, elles
doivent en l'espèce être interprétées comme une limite basse, en ce
qu'elles dépendent de la fiabilité et de l'actualité du décompte communiqué
par le Ministère de l'Intérieur.

De même, l'estimation du nombre de personnes affectées par les gaz lacrymogènes
repose sur un décompte plafonnée par les capacités d'assistance des
secours volontaires présents.

Cordialement

Vincent VICTOR
Observatoire des Street-médics




.. _stics_38_2023_05_01:

Discours du syndicat CNT STICS38 le 1er mai 2023
---------------------------------------------------------

- https://panel.globenet.org/cgi-bin/mailman/listinfo/contacts.38

En ce premier mai 2023, et depuis 3 mois et demi, nous menons une lutte
massive contre la réforme des retraites et toutes les lois brutales,
régressives et répressives du pouvoir.


Ci-dessous, retrouvez la prise de parole de la CNT à l’issue de la manifestation.
Prise de parole finalement empêchée par les nuages de lacrymogènes et les
violences répressives entreprises à proximité directe de l’Anneau de
vitesse, dans le parc Paul-Mistral, où se finissait la manifestation et
où étaient installés de très nombreux stands d’organisations.

Aux blessé·es, aux arrêté·es, nous apportons ici tout notre soutien et
condamnons la répression d’une police qui par essence est aux ordres de
la Préfecture, représentante directe du gouvernement.


Et pourtant on en est encore là. Parce que toutes nos mobilisations de
témoignage, Macron s’en tamponne, et il nous crache même encore son mépris
au visage par-dessus le marché !

À l’écouter nous n’avons aucun droit sur nos propres vies !

Nous n’aurions qu’à courber la tête et tout accepter sans discuter !

Alors même que lui et sa classe creusent eux-mêmes leurs fameux trous
budgétaires et volant NOS cotisations pour financer LEURS profits.

Car salariés ou non, payés ou pas, qu’on se le dise, encore et encore :
c’est bien nous toutes et tous qui produisons tout !
La seule chose à quoi travaille un capitaliste, c’est à la reproduction
de son propre pouvoir sur nous. Nous n’avons pas besoin d’eux !

Aujourd’hui, force est de le constater : le pouvoir ne discute plus.

Il se contente d’imposer, et il s’en donne les moyens répressifs.
Quoi qu’on puisse en penser, s’il suffit que Macron ferme sa porte à
l’intersyndicale pour que cette dernière en soit réduite à l’impuissance
la plus totale, c’est que **la mascarade du dialogue social n’a que trop duré**.

Et de fait, la posture cogestionnaire ne pouvait mener qu’à une impasse,
car pour avoir un intérêt aux yeux du pouvoir, encore faudrait-il qu’elle
représente une alternative à une menace plus sérieuse.

Par ailleurs, il serait tout aussi vain d’espérer quoi que ce soit d’un
changement de dirigeants politiques.
Qu’on ne s’y trompe pas, ce que nous connaissons aujourd’hui n’est pas un
problème de personne, si abjects que soient Macron et ses sbires, leur
attitude n’est que la cristallisation visible de l’état du rapport de
force dans notre société.

Car l’État tout entier, avec ses règles, ses procédures et ses institutions,
n’est conçu que pour servir les intérêts du Capital.

Et espérer que cet État puisse servir d’autres buts, c’est au moins aussi
con qu’espérer qu’un bon mécano suffirait à faire tourner un moteur de
bagnole avec de l’eau.
Il est temps de se rendre compte que ça ne marche pas comme ça, et que
c’est tout le moteur de cette société de merde qu’il s’agit de changer,
une bonne fois pour toutes !

Ainsi, il nous faut donc compter sur nos propres forces, et affirmer nos
perspectives révolutionnaires.

Notre problème, ce n’est pas que Macron serait trop méchant, c’est que
nous, nous sommes trop gentils !

Il n’est que temps de nous souvenir, en ce premier mai 2023, que nos
acquis sociaux, qu’on achève à présent de liquider, n’ont été conquis
que par le rapport de force.

Comme ce fut le cas lors de l’application du programme du CNR, quand une
bourgeoisie massivement ralliée au fascisme, s’est trouvée dans le camp
des vaincus et dû céder face à une Résistance en grande partie révolutionnaire,
armée et consciente de sa force.

Soyons lucides ; si nous subissons à présent un risque de destruction
totale de la planète, de nos conditions de vie et de travail, nous le
devons à une logique d’ensemble, une logique de prédation généralisée
et aveugle : la logique capitaliste !

Trop longtemps, entraînés par les médias chiens de garde, nous en avons
été réduits à penser le monde à travers le prisme de cette logique, nous
aussi.
On nous a infligé l’hégémonie d’un vocabulaire répugnant, on a osé nous
dire que nous devions apprendre à nous « vendre », et hélas ce n’était
pas qu’une formule.

C’est seulement lorsque nous renonçons à affirmer nos propres valeurs,
c’est uniquement lorsque nous échouons à penser avec nos propres mots,
que le cadre posé par l’État capitaliste nous semble indépassable.

Mais il ne l’est pas ! Nous pouvons, nous devons, redéfinir ensemble la
manière dont nous voulons vivre.
Ne déléguons pas le soin de penser nos perspectives politiques à ceux
qui voudraient nous gouverner : car ce que nous voulons, c’est à nous
de le décider !
Et de le mettre en place ! Par l’autogestion ! Car personne, jamais, ne
le fera à notre place !

Et d’ici là, à nous de construire partout des espaces d’émancipation et
de solidarité, dans nos entreprises comme dans nos quartiers.

Des espaces où il n’y a pas de dogmes, pas de chefs, dans lesquels on peut
compter les uns et les unes sur les autres et où la lutte peut s’enraciner !

Tel est le véritable pouvoir du syndicalisme, lorsque celui-ci est révolutionnaire !

Il nous donne les outils pour gérer la société nous même, selon nos
propres valeurs et nos propres besoins !

Il nous ouvre la voie vers une révolution qui ne serait pas qu’un simple
changement de maîtres, mais qui serait une révolution sociale, capable
de refonder le monde SUR NOS PROPRES BASES !

La voilà la perspective de la lutte sociale et syndicale, la voilà
l’alternative à ce monde qui crève sous nos yeux et qui voudrait nous
entraîner avec lui.

Et bon sang, il nous semble qu’il y a là de quoi justifier toutes nos luttes
et toutes nos peines.


Pour l’abolition du capitalisme et de la république bourgeoise !
Révolution, réquisition, autogestion !

