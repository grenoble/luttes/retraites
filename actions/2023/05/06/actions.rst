.. index::
   pair: EACOP; 2023-05-06
   pair: EACOP; Stop

.. _actions_2023_05_06:

================================================================================================================================================================
**Actions du samedi 6 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #stopEACOP #CriminelsClimatiques #JusticeClimatique**
================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie #stopEACOP #CriminelsClimatiques #JusticeClimatique


.. figure:: images/page_une_ici_grenoble_2023_05_06.png
   :align: center

   https://www.ici-grenoble.org/article/un-oleoduc-sous-grenoble, https://www.openstreetmap.org/#map=19/45.19053/5.72935


.. raw:: html

   <iframe width="1000" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.7275810837745675%2C45.18973094844643%2C5.731121599674226%2C45.19133013442518&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19053/5.72935">Afficher une carte plus grande</a></small>


.. figure:: images/stopEACOP.png
   :align: center

   https://www.stopeacop.net/, https://nitter.dcs0.hu/stopEACOP


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_06.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/atelier-clown-e-s-en-mixite-choisie-1
- |solidarite| https://www.ici-grenoble.org/evenement/journee-modif-corporelles-et-soiree-concerts-en-soutien-a-des-caisses-de-solidarite
- |important| 10h00 https://www.ici-grenoble.org/evenement/rassemblement-contre-le-projet-eacop-mega-projet-petrolier-de-total-en-ouganda-et-tanzanie

      - https://www.stopeacop.net/, Rassemblement organisé par |alternatiba| Alternatiba/ANVCOP21 Grenoble, Attac 38 et Extinction Rebellion Grenoble |rebellion|.

- 13h00 |car38| https://www.ici-grenoble.org/evenement/cine-rencontres-violences-policieres-le-combat-des-familles-avec-des-proches-de-tue-e-s-par-la-police
- |important| 15h00 https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- 16h00 |car38| https://www.ici-grenoble.org/evenement/atelier-securite-et-soins-comment-devenir-plus-fort-e-s-dans-nos-emancipations-et-nos-luttes
- 17h50 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 18h50 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 21h40 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines


Demosphere 38
=================

.. figure:: images/demo_2023_05_06.png
   :align: center

   https://38.demosphere.net/


- |important| |important| 10h00 https://38.demosphere.net/rv/1146 (RASSEMBLEMENT CONTRE LE MÉGA PROJET PÉTROLIER DE TOTAL EN OUGANDA ET TANZANIE)
- 13h00 |car38| https://38.demosphere.net/rv/1102 (Film/Disscussion croisée - Violence policières, violences judiciaires : la voix des concerné.es)
- 16h00 |car38| https://38.demosphere.net/rv/1103 (Sécurité et Soin )

#StopEACOP
===========

- https://www.blast-info.fr/articles/2022/eacop-total-met-le-feu-a-la-planete-letat-complice-_WyvN-ZyRPqJWuAgdqdonQ
- https://www.youtube.com/watch?v=LgWPzY27jCU (EACOP : TOTAL met le feu à la planète, l'état complice)
- https://www.stopeacop.net/
- https://www.lemonde.fr/idees/article/2022/03/28/totalenergies-doit-stopper-ses-projets-eacop-et-tilenga-en-ouganda_6119509_3232.html
- https://nitter.dcs0.hu/stopEACOP
- https://nitter.dcs0.hu/stopEACOP/rss


.. figure:: images/blast_eacop.png
   :align: center

   https://www.youtube.com/watch?v=LgWPzY27jCU



.. figure:: images/stop_eacop.png
   :align: center

   https://www.stopeacop.net/, https://nitter.dcs0.hu/stopEACOP/rss



EACOP, ce nom est inconnu pourtant c’est l’un des projets les plus
climaticides en cours.

EACOP c’est “l’Oléoduc de pétrole brut d’Afrique de l’Est” : un méga projet
pétrolier porté principalement par Total Energies en Ouganda et en Tanzanie.

S’il voit le jour, ce sera le plus long oléoduc chauffé au monde : 1443 km
de long et il contribuera à émettre 34 millions de tonnes de CO2 par an,
soit 6 fois les émissions de l’Ouganda.

À l’heure où les scientifiques du GIEC mais aussi l’Agence Internationale
de l’Énergie demandent l’arrêt immédiat de tout nouveau projet fossile,
le projet EACOP, est non seulement une aberration qui rapproche le monde
d'une catastrophe climatique, mais aussi une bombe sociale dénoncée
partout à travers le monde.

Le mouvement #StopEACOP a déjà réuni près de 265 ONGs opposées au projet.

En France, on en parle peu et beaucoup pourraient ne pas se sentir concernés,
et se dire qu’après tout, cela se passe en Afrique de l’Est.

Mais ce projet va affecter le monde entier et pose une question essentielle :
celle de la justice climatique.

Alors que de nombreuses voix s’élèvent pour mettre fin au projet, les
répressions sont toujours plus importantes contre les opposants, tant en
Ouganda qu’en Tanzanie qui sont deux régimes autoritaires.

Les activistes n’ont donc d’autres choix que de venir en Europe pour
alerter les dirigeants et notamment la France.

Les multinationales fossiles sont-elles inarrêtables ?

Combien de temps encore leur avidité passera avant la vie, avant notre avenir ?

Décryptage.
