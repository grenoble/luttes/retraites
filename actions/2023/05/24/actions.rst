
.. _actions_2023_05_24:

============================================================================================================================================================================================================
**Actions du mercredi 24 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale #TechnoPolice #QuadratureDuNet #StreetMedic #BOCAL #ExtinctionRebellion**
============================================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-05-24 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale #TechnoPolice #QuadratureDuNet #StreetMedic #BOCAL #ExtinctionRebellion


|ici_grenoble| A la une **La Technopolice à Grenoble**
=====================================================================

- https://www.ici-grenoble.org/article/la-technopolice-a-grenoble

.. figure:: images/page_une_ici_grenoble_2023_05_24.png
   :align: center

   https://www.ici-grenoble.org/article/la-technopolice-a-grenoble, https://www.ici-grenoble.org/newsletter

Avez-vous déjà entendu parler de l'algorithme secret de la CAF Isère ?

Ce mercredi 24 mai à 19h à la Maison des associations de Grenoble, l'association
La Quadrature du Net anime une conférence sur la technopolice et le
contrôle social à la CAF et au Pôle emploi.

Selon une enquête de la Quadrature du Net, la CAF utilise un algorithme
secret notant les allocataires et sélectionnant celles et ceux qui seront contrôlé-e-s.

Selon quels critères fonctionne l'algorithme ? La CAF refuse pour l'instant
de divulguer les données utilisées. Pourtant, la loi prévoit que tout
individu faisant l’objet d’une décision prise sur le fondement d’un
traitement algorithmique a le droit de connaître les paramètres de cet algorithme.

Selon la Quadrature du Net, les critères dégradant la note d’un-e allocataire
et augmentant ses chances d’être contrôlé-e cibleraient les personnes
les plus précaires : le fait de disposer de faibles revenus, d’habiter
dans un quartier dit "défavorisé", d’être une mère célibataire ou
d’être né-é hors de France.



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_24.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/le-24-mai-1864-le-droit-de-faire-greve-en-france (texte à venir)
- 09h00 https://www.ici-grenoble.org/evenement/permanence-fiscale-militante-aide-a-la-declaration-de-revenus-avec-ou-sans-papiers
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 16h40 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- |important| 17h30 https://www.ici-grenoble.org/evenement/conference-de-la-quadrature-du-net-la-technopolice-et-le-controle-social-a-la-caf-et-au-pole-emploi
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- 20h00 https://www.ici-grenoble.org/evenement/concert-de-soutien-au-38-dors-chat-quitters-et-ruine
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative



Demosphere 38
=================

.. figure:: images/demo_2023_05_24.png
   :align: center

   https://38.demosphere.net/

- |important| https://38.demosphere.net/rv/1192, 17h30 Rassemblement / casserolades CMAP contre la loi retraites Devant la mairie de La Mure
- |important| https://38.demosphere.net/rv/1182 18h00 Assemblée générale de lutte en Nord Isère 362 chemin de Loudon, 38300 Ruy
- |important| https://38.demosphere.net/rv/1184 18h30 AG de lutte contre la réforme des retraites 38 rue d'Alembert Grenoble

|rebellion| 2023-05-24 18h30 à 20h **Une initiation pour être Street médic à organisée par XR Grenoble et Alternatiba Grenoble**
====================================================================================================================================

- :ref:`bocal_2023_05_14`
- https://obs-medics.org/
- https://obs-medics.org/sabonner/

Une initiation pour être Street médic à organisée par XR Grenoble et
Alternatiba Grenoble aura lieu le **mercredi 24 mai 2023 au Bocal (Base d'Organisation Collective des ALternatives)
d'alternatiba, le long de l'esplanade à Grenoble, 8 boulevard de l'Esplanade**.

Vient découvrir et discuter des bases du soin en action avec une petite
partie de bases puis un temps d’échanges et de réponses aux questions.

Attention, cette formation n’est pas une formation aux premiers secours
mais bien à des conduites à tenir et astuces en cas de pépins sur les actions.

Merci de remplir ce formulaire pour vous y inscrire :

👉 https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117 👈

Le lieu et les horaires précis de la formation vous seront reconfirmés par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr
