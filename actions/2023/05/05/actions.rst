.. index::
   pair: Solidarité ; 2023-05-05

.. _actions_2023_05_05:

==========================================================================================================================================================
**Actions du vendredi 5 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Solidarité avec nos 2 camarades** |solidarite|
==========================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie #Solidarité


.. figure:: images/une_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/article/avec-les-interpelles-du-1er-mai



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_05.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 13h00 |car38| https://www.ici-grenoble.org/evenement/observation-dune-procedure-de-comparution-immediate-au-tribunal

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- |important| 13h00 |car38| |solidarite| https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-deux-camarades-interpelles-le-1er-mai

      - https://www.ici-grenoble.org/article/avec-les-interpelles-du-1er-mai

- 14h30 https://www.ici-grenoble.org/evenement/repair-cafe-de-grenoble-centre
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 17h30 https://www.ici-grenoble.org/evenement/concert-kokonut-et-vernissage-de-lexposition-le-cameleon
- 17h50 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 18h30 |car38| https://www.ici-grenoble.org/evenement/presentation-du-livre-sur-la-sellette-sur-la-comparution-immediate
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 19h35 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 20h00 https://www.ici-grenoble.org/evenement/concert-punk-hardcore-noise-avec-balafre-shinken-et-spavedje

Demosphere 38
=================

.. figure:: images/demo_2023_05_05.png
   :align: center

   https://38.demosphere.net/

- 13h00 |car38| https://38.demosphere.net/rv/1100 (Observation de comparutions immédiates )
- 13h00 |car38| https://38.demosphere.net/rv/1159 (Rassemblement de soutien devant le tribunal judiciaire à 13h) |solidarite|
- 18h30 |car38| https://38.demosphere.net/rv/1101 (Présentation du livre "sur la sellette" )


.. _solidarite_2023_05_05:

|ici_grenoble| **Solidarité avec les interpellés du 1er mai 2023**
=======================================================================

- https://www.ici-grenoble.org/article/avec-les-interpelles-du-1er-mai
- https://cric-grenoble.info/infos-locales/article/rassemblement-de-soutiens-devant-le-tribunal-judiciaire-a-13h-vendredi-05-05-3010
- https://www.blast-info.fr/emissions/2023/gardes-a-vue-abusives-la-nouvelle-arme-anti-manifestants-Z99bO61hTC2Ww1OpyQkqNw

.. figure:: images/page_une_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/article/avec-les-interpelles-du-1er-mai


Ce vendredi 5 mai à 13h devant le Palais de justice de Grenoble, un rassemblement
de soutien s'organise pour le procès en comparution immédiate de deux
personnes interpellées lors de la manifestation du 1er mai.

Pourquoi ? Que s'est-il passé pendant la manifestation ?
En quoi est-ce un tournant dans la répression à Grenoble ?

Voici les explications du Collectif Anti-Répression Isère :

"Vendredi après-midi, deux camarades passent en comparution immédiate suite
aux évènements du premier mai.

Face à la répression, ne les laissons pas seuls, venons nombreux.ses les
soutenir au moment de leur audience à 13 heures au tribunal judiciaire.

Le 1er mai, en fin de manifestation, un cortège s’est détaché de l’anneau
de vitesse pour s’engager dans le parc Paul Mistral.
La police à très rapidement réagit en gazant abondamment les manifestant.es
qui se sont repliés avant de tenter une autre route pour sortir du parc.

Oubliant qu’ils n’étaient pas CRS, 6 ou 7 agents de la BAC ont alors
décidé de bloquer le cortège sans attendre de renforts, mais, à leur
grande surprise, ils on fait face à plus de détermination que d’habitude.

Refusant de reculer face aux grenades jetées par la BAC, les manifestant.es
ont décidé d’avancer rapidement en direction de la police qui, prise de
panique s’est enfuie en courant.

Très rapidement, des policiers de la CDI sont arrivées en renfort de leurs
collègues.
Bondissant hors de leurs véhicule, ils se ruent sur les manifestant.es,
et accompagnés de la BAC, leurs lancent des grenades et chargent les deux
banderoles pour distribuer de nombreux coups en se concentrant de manière
délibérée sur les têtes.

Résultat, 6 personnes particulièrement blessées (sutures et traumas crâniens
légers à la clé, de nombreuses traces de coups et une blessure à la jambe)
ainsi qu’un camarade arrêté, a priori au hasard.

Une deuxième personne sera arrêtée plus tard dans l’après-midi au niveau
du carrefour de l’Aigle pour un motif obscur.

Les deux camarades sont placés en garde à vue pour violence sur personne
dépositaire de l’autorité publique et rébellion.

Au bout des 24 premières heure, la garde à vue a été prolongée.

Pendant ce temps, la couverture médiatique a fait la part belle aux
témoignages de policiers et à la version de leur hiérarchie dans la plus
pure tradition du journalisme de préfecture : déplorant un blessé par
jet de projectile et un blessé par maladresse (autrement présenté, disons le),
et s’étant héroïquement défendus contre une horde d’affreux black blocs
visant à mettre à bas la république et « casser du flic », les forces
de l’ordre auraient tout de même réussit à arrêter exactement les deux
auteurs de violence dans la cohue.

Mercredi soir, plus de deux jours après leur arrestation, les deux interpellés
ont été placés en détention provisoire à Varces par le Juge des Libertés
et de la Détention en attendant leur procès en comparution immédiate vendredi.

Cette décision, surprenante au vu des excellentes garanties de représentation
des deux camarades à été ridiculement motivée par le refus d’une des deux
personnes de donner son nom au début de la garde à vue, et surtout à la
présence d’un rassemblement de soutien devant le tribunal, ce qui laisserait
penser à la possibilité d’un "risque de réitération des faits" d’ici le procès.

Au-delà du drame individuel que ces deux placements en détention représentent
pour les deux camarades et leurs proches, cette décision du JLD est une
menace pour tout le mouvement social et à la solidarité collective que
nous mettons en place.

Le message est clair : « si vous continuez à venir chanter des slogans
et manger des gâteaux devant le tribunal, on mettra vos amis en prisons ».

Les rassemblements de soutien devant le tribunal ont toujours fait parti
du répertoire des luttes.

Avec cette décision, la justice fait tomber son masque, il n’y a pas
besoin de vraie raison pour punir celles et ceux qui menacent l’ordre établi.

Nous ne nous laisserons pas intimider. Rassemblons nous encore plus nombreux.ses
ce vendredi à partir de 13h pour soutenir nos deux camarades au moment
de leur procès."


Le CAR38 |car38|

Signataires : Solidaires, CNT, le 38 Centre Social, Groupe Féministe Antifasciste.

À 13h

Devant le Palais de justice de Grenoble, https://www.openstreetmap.org/#map=18/45.19192/5.71172


.. raw:: html

   <iframe width="1000" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.7081806659698495%2C45.19037365270349%2C5.7152509689331055%2C45.19346609288795&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=18/45.19192/5.71172">Afficher une carte plus grande, https://www.openstreetmap.org/#map=18/45.19192/5.71172</a></small>

