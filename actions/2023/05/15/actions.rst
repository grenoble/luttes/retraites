
.. _actions_2023_05_15:

=========================================================================================================================================================================
**Actions du lundi 15 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #LesCommuns #Effondrement #RencontresGéopolitiqueCritique**
=========================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #LesCommuns #Effondrement #RencontresGéopolitiqueCritique


|ici_grenoble| A la une **Couper le robinet à ST ?**
============================================================================================================


.. figure:: images/page_une_ici_grenoble_2023_05_15.png
   :align: center

   https://www.ici-grenoble.org/article/couper-le-robinet-a-st


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_15.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 10h00 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h00 https://www.ici-grenoble.org/evenement/permanence-du-garage-associatif-les-soupapes-reparer-sa-voiture
- 15h40 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- |important| 18h00 https://www.ici-grenoble.org/evenement/coupons-le-robinet-a-st-rassemblement-contre-laccaparement-de-leau-par-st-microelectronics
- 18h30 https://www.ici-grenoble.org/evenement/quest-ce-que-les-communs, dans le cadre des Rencontres de Géopolitique critique
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-feministe-de-grenoble-en-mixite-choisie-sans-hommes-cisgenres
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 21h00 🎥 https://www.ici-grenoble.org/evenement/coup-deffroi-leffondrement-la-plus-realiste-des-series-post-apocalyptiques



|rebellion| Un temps collectif d’appropriation de notions juridique organisée par XR Grenoble et Alternatiba Grenoble
==========================================================================================================================


Un temps collectif d’appropriation de notions juridique organisée par
XR Grenoble et Alternatiba Grenoble aura lieu le lundi 15 mai 2023 au Bocal
d'alternatiba, le long de l'esplanade à Grenoble.

Amende forfaitaire délictuelle, filouterie, entrave à la circulation,
convention secrète de déchiffrement d’un moyen de cryptologie ...

Tous ces termes te font peur ? Viens te les approprier collectivement !
Pendant ce petit temps, nous nous pencherons ensemble sur des notions
et textes de loi pour se les approprier, mieux comprendre l'arsenal
de répression pénal à la disposition des forces de l'ordre et évaluer
rapidement les risques juridiques auxquels on s'expose.

Merci de remplir ce formulaire pour vous y inscrire :

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Le lieu et les horaires précis de la formation vous seront reconfirmés par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr


