.. index::
   pair: AGFéministe ; 2029-05-29

.. _actions_2023_05_29:

=========================================================================================================================================
**Actions du lundi 29 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AGFeministe**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AGFeministe


|ici_grenoble| A la une **Appel à dons pour "Le 17**
=====================================================================

- https://www.ici-grenoble.org/article/appel-a-dons-pour-le-17
- http://www.flagrant-deni.fr/
- https://www.flagrant-deni.fr/feed/
- https://linktr.ee/flagrantdeni
- https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes

.. figure:: ../25/images/page_une_ici_grenoble_2023_05_25.png
   :align: center

   https://www.ici-grenoble.org/article/appel-a-dons-pour-le-17, https://www.ici-grenoble.org/newsletter


En cas de violence policière, comment déposer plainte ?

Comment empêcher un policier sadique de continuer d’exercer ?

Comment obtenir une "réparation" sans attendre la fin d'un procès ?

Aujourd’hui en France, il n’existe aucun guide complet pour répondre aux
victimes de violences policières et soutenir le travail de leurs avocat·es.

Le média lyonnais `Flagrant-déni (http://www.flagrant-deni.fr) <http://www.flagrant-deni.fr/>`_
lance un `appel à dons pour les aider à finaliser "Le 17" <https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes>`_:
un guide pratique et juridique gratuit pour les victimes de violences policières.

Ce guide inédit est le résultat de 4 ans de recherches sur le droit
applicable (jurisprudences et notes internes non publiées) et les
pratiques policières et judiciaires.

Il proposera plus de 100 questions/réponses, des textes précis et sourcés
avec un langage clair, des exemples concrets et des schémas explicatifs.

Pour en savoir plus et participer à l'appel à dons, c'est `ici <https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes>`_.

PS : Les dons à Flagrant-déni sont déductibles des impôts.

**Lancé en 2022 à Lyon, Flagrant-déni.fr est un média indépendant national
ET un collectif de soutien aux victimes de violences policières**


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_29.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 10h00 https://www.ici-grenoble.org/evenement/annule-festival-ecofeministe-bien-lbourgeon (ANNULE)
- |important| 14h00 https://www.ici-grenoble.org/evenement/assemblee-generale-feministe-de-grenoble-en-mixite-choisie-sans-hommes-cisgenres

  - https://mensuel.framapad.org/p/ordre-du-jour-assemblee-femniste-29-mai-a14g?lang=fr
  - https://coordfeministe.wordpress.com/2022/07/21/communique-de-sortie-des-rencontres-nationales-feministes-de-grenoble/

- 16h40 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 17h00 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h20 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 18h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative

