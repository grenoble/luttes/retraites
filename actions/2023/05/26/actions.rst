
.. _actions_2023_05_26:

=======================================================================================================================================================================================================================
**Actions du vendredi 26 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Casserolade #FlagrantDeni #PolicePartoutJusticeNullePart #RassemblementDesGlieres #SoulèvementsDeLaterre**
=======================================================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #2023-05-26 #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Casserolade #FlagrantDeni #PolicePartoutJusticeNullePart #RassemblementDesGlieres #SoulèvementsDeLaterre


|ici_grenoble| A la une **Casserolader Olivier Véran** |casserolade|
=================================================================================

- https://www.ici-grenoble.org/article/casserolader-olivier-veran

.. figure:: images/page_une_ici_grenoble_2023_05_26.png
   :align: center

   https://www.ici-grenoble.org/article/casserolader-olivier-veran, https://www.ici-grenoble.org/newsletter


Ce vendredi 26 mai 2023, la venue d'Olivier Véran est annoncée à Grenoble.

L'intersyndicale appelle à lui faire un accueil sonore à 17h30 place Verdun,
devant la Préfecture de l'Isère.

Voici l'appel intersyndical :

"A l'appel de l'intersyndicale 38 qui lutte pour le retrait de la mauvaise
loi retraites, nous remettons le couvert pour cette deuxième visite d'Olivier
Véran à Grenoble !

Il revient remettre une médaille et arriverait pour 18h, à nous de
remettre le couvert et de le mettre face à ses responsabilités : nous
appelons à se rassembler à partir de 17h30 place de Verdun devant la
préfecture pour le non-accueillir comme il le mérite !

Alors que Macron, ses ministres et le porte-parole Olivier Véran :

- nous insultent et nous méprisent en continuant à nous imposer une réforme
  des retraites inutile et injuste qui nous fera mourir au travail et qui nous précarisera

- alors que le gouvernement Borne a multiplié les passages en force
  par 49-3 et âbimé la démocratie dans le pays

- alors qu'ils ordonnent et couvrent les violences contre les
  manifestant.e.s et les syndicalistes

- alors qu'ils menacent des associations comme la LDH mais sont particulièrement
  tolérants avec l'extrême-droite,

- alors qu'ils cassent les services publics, et souhaitent s'atteler à
  de nouvelles lois de casses sociales nous casserolerons et nous continuerons
  la lutte pour préparer la grande journée de grève et de manifestation du 6 juin 2023"

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_26.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/annule-festival-ecofeministe-bien-lbourgeon (ANNULE)
- 13h10 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 14h00 https://www.ici-grenoble.org/evenement/cours-de-francais-reserves-aux-femmes-et-aux-personnes-minorisees-de-genre
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- |important| |casserolade|  17h00 https://www.ici-grenoble.org/evenement/casserolade-pour-la-venue-dolivier-veran-a-grenoble
- 17h30 https://www.ici-grenoble.org/evenement/spectacle-petit-caillou-sur-le-coltan-et-les-smartphones
- 18h30 🎥 https://www.ici-grenoble.org/evenement/cine-rencontres-hedera-sur-les-grandeurs-et-les-deceptions-des-luttes-anarchistes
- 18h45 https://www.ici-grenoble.org/evenement/groupe-decoute-et-de-parole-pour-les-parents-les-lesbiennes-gays-bi-trans-leurs-familles-et-ami-e-s
- 19h30 https://www.ici-grenoble.org/evenement/reunion-publique-stop-au-lyon-turin
- 19h30 https://www.ici-grenoble.org/evenement/bal-fok-queer-avec-initiation-au-folk
- |important| 20h00 https://www.ici-grenoble.org/evenement/rencontre-avec-lauteur-de-on-acheve-bien-les-enfants-sur-les-ravages-des-ecrans

Demosphere 38
=================

.. figure:: images/demo_2023_05_26.png
   :align: center

   https://38.demosphere.net/

- |important| |casserolade| 17h00 https://38.demosphere.net/rv/1191 VERAN à la PREFECTURE


|crha| Vidéos sur le Rassemblement des Glières 2023
=========================================================

- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos

Le CRHA est sur Youtube depuis le 23 mai 2023 (https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos, https://www.youtube.com/@CRHA_RassemblementdesGlieres/about)
et quelques vidéos sont disponibles.

On peut déjà voir:

- l'intervention de Julien Le Guet, un des porte paroles desMouvements
  de la Terrre ici: https://www.youtube.com/watch?v=zQ3z5Ee6hm0
- L’écoféminisme, combat d’aujourd’hui ici: https://www.youtube.com/watch?v=uh8Zuj6F83A

.. figure:: images/julien_le_guet.png
   :align: center

   https://glieres.frama.io/crha/resistances-2023/05/21/prises-de-parole/julien-le-guet/julien-le-guet.html

