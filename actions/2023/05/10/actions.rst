
.. _actions_2023_05_10:

==================================================================================================================================================================
**Actions du mercredi 10 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale #RencontresGéopolitiqueCritique**
==================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale #RencontresGéopolitiqueCritique


|ici_grenoble| A la une **Les 10 infos de mai**
=====================================================================


.. figure:: images/page_une_ici_grenoble_2023_05_10.png
   :align: center

   https://www.ici-grenoble.org/article/les-10-infos-de-mai



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_10.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 06h00 https://www.ici-grenoble.org/evenement/le-10-mai-1981-la-victoire-du-larzac
- |important| |solidarite| 🇺🇦 12h30 https://www.ici-grenoble.org/evenement/table-ronde-quelle-paix-pour-lukraine (Dans le cadre des Rencontres de Géopolitique critique)
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 16h30 https://www.ici-grenoble.org/evenement/permanence-pour-les-personnes-trans-et-ou-intersexes-et-ou-en-questionnement
- |important| |rebellion| 18h30 https://www.ici-grenoble.org/evenement/atelier-sexisme-racisme-productivisme-comment-se-liberer-de-la-culture-dominante (Dans le cadre des Rencontres de Géopolitique critique, atelier avec Extinction Rebellion Grenoble)
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites

      - https://www.ici-grenoble.org/article/reflechir-et-sorganiser

- 18h30 🎥 https://www.ici-grenoble.org/evenement/cine-debat-g-reve-general-e-sur-les-luttes-etudiantes-victorieuses-contre-le-cpe-en-2006
- 19h00 https://www.ici-grenoble.org/evenement/theatre-les-guerilleres
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h40 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques

Demosphere 38
=================

.. figure:: images/demo_2023_05_10.png
   :align: center

   https://38.demosphere.net/

- 17h30 https://38.demosphere.net/rv/1169 (Rassemblement du CMAP contre la réforme des retraites et réunion de travail militant, Devant la mairie de La Mure)
- 18h30 🎥 https://38.demosphere.net/rv/1163 (Projection du film "(G)rève général(e)" de Matthieu Chatellier, Daniela De Felice, EVE, Espace vie étudiante - Campus SMH)
- |important| 18h30 https://38.demosphere.net/rv/1167 (Assemblée générale de lutte, 38 rue d'Alembert)


|solidarite| 🇺🇦 2023-05-10 12h30 Table-ronde : Quelle paix pour l’Ukraine ?
===================================================================================

- https://www.ici-grenoble.org/evenement/table-ronde-quelle-paix-pour-lukraine

.. figure:: images/recontres_geopolitiques.png
   :align: center

   https://www.ici-grenoble.org/evenement/table-ronde-quelle-paix-pour-lukraine

Cette table-ronde réunira des chercheuses et enseignantes d’origine
ukrainienne ou travaillant sur l’Ukraine.

Elles sont, pour plusieurs d’entre elles, militantes et membres actives
d’associations de solidarité avec l’Ukraine et les Ukrainien.nes.

L’objectif de cette discussion est de revenir sur les visions de la paix
en Ukraine, de recentrer le débat sur ce que les Ukrainien.nes veulent
elles et eux-mêmes.

Les intervenantes présenteront différentes formes de mobilisation et de
résistance de la société ukrainienne à l’invasion de leur pays.

Elles reviendront également sur l’importance de l’expérience de la
solidarité internationale avec l’Ukraine dans des pays aussi différents
que la France, la Pologne et la Géorgie.

Table-ronde avec :

- Olga Bronnikova, enseignante-chercheure Université Grenoble Alpes/ILCEA4 (CESC),
  membre de l’association Ukraine Grenoble Isère / Tetyana Lyubchyk,
  enseignante au Lycée Pablo Neruda, présidente de l’association Ukraine Grenoble Isère /

- Hanna Perekhoda, doctorante, Université de Lausanne, membre de
  l’association suisse Comité Ukraine / Perrine Poupin, chargée de
  recherche CNRS, AAU CRESSON

- Anastasia Riabchuk, enseignante-chercheure,Académie Mohyla-Kyïv, PAUSE,
  membre du comité de rédaction de Commons : Journal of Social Criticism

À 12h30

Maison de l’International
1 rue Hector Berlioz
38000 Grenoble


.. _geo_critique_2023_05_10:

|rebellion| 2023-05-10 18h30 Atelier : Sexisme, racisme, productivisme... Comment se libérer de la culture dominante ? atelier avec Extinction Rebellion Grenoble
======================================================================================================================================================================

- https://www.ici-grenoble.org/evenement/atelier-sexisme-racisme-productivisme-comment-se-liberer-de-la-culture-dominante

.. figure:: images/recontres_geopolitiques.png
   :align: center

   https://www.ici-grenoble.org/evenement/atelier-sexisme-racisme-productivisme-comment-se-liberer-de-la-culture-dominante

Dans le cadre des Rencontres de Géopolitique critique, atelier avec Extinction Rebellion Grenoble.

Voici la présentation
--------------------------

« La culture dominante s’est progressivement emparée du monde pendant les
dernières centaines d’années est une culture qui annihile le vivant.

Pas seulement les systèmes vivants des autres espèces, mais les systèmes
à l’intérieur même des communautés humaines (sexisme, racisme, homophobie,
validisme, classisme, agisme,...).

Si nous arrivons à résoudre le problème de, disons, les émissions de gaz
à effet de serre mais que nous fonctionnons toujours avec la même culture
dominante, alors nous aurons une autre menace existentielle après ça,
puis une autre après, et la suivante...

Nous n’allons pas vraiment changer le système, nous n’allons pas vraiment
changer la trajectoire de l’humanité sur la planète si on ne change pas la culture.

Nous avons besoin d’une culture qui n’est pas « dégénératrice » et qui
n’encourage pas l’annihilation des systèmes vivants, mais une culture
qui est régénératrice qui encourage le développement et l’évolution et
la complexité toujours plus grande des systèmes vivants, y compris des
communautés humaines »


Ce que sont les Rencontres
-------------------------------

Ce que sont les Rencontres : "Les Rencontres de Géopolitique critique
créent un espace où nous cherchons collectivement à articuler des approches,
des méthodes, des expériences et à susciter le contact et l’échange.

Nous souhaitons faire des Rencontres un moment de partage et de réflexion
à partir de nos engagements respectifs, à partir des lieux que nous
fréquentons, des risques que nous prenons, des méthodes que nous inventons,
des réseaux que nous créons.

Cela passe aussi par les postures que nous adoptons les un.es par rapport
aux autres en prenant en compte les inégalités sociales qui jalonnent
notre société et nos relations."

Thème 2023 « La paix ! »
-----------------------------

Le thème 2023 est « La paix ! » : Pour ne pas laisser aux discours majoritaires
le sens de ce mot qui en font l’opposé de la guerre et du conflit, et le
confondent avec l’ordre et la répression…pour s’en ressaisir, le comprendre
comme un processus continu qui donne sa place au conflit, lui permet de
s’exprimer, de faire entendre les besoins de ses acteur.ices, rend visibles
les violences invisibilisées et conduit à plus de justice sociale.

Modop propose une exploration de ce thème pour les Rencontres 2023 à
travers les contributions des intervenant.es.

Nous parcourrons tour à tour les manifestations de maintien de l’ordre
et du contexte répressif sous prétexte de paix, puis les propositions
de fonctionnements différents, qui donnent leur place à une expression
d’autres voix.

Pour plus d'info : info@modop.org

À 18h30

À la Base
17 rue du Dauphiné
38000 Grenoble
https://www.openstreetmap.org/#map=19/45.18467/5.71610


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.714325606822968%2C45.183872553634856%2C5.717866122722627%2C45.18547190420805&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18467/5.71610">Afficher une carte plus grande</a></small>
