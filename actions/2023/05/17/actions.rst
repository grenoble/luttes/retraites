
.. _actions_2023_05_17:

=======================================================================================================================================================
**Actions du mercredi 17 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #PasDenfantsALaRue #ActionNonViolente**
=======================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #PasDenfantsALaRue #ActionNonViolente


|ici_grenoble| A la une **Pas d'enfants à la rue !**
============================================================================================================


:download:`👉  Télécharger l'appel à rassemblement au format PDF <pdfs/Appel-rassemblement-CD_2023_05_17.pdf>` 👈

.. figure:: images/page_une_ici_grenoble_2023_05_17.png
   :align: center

   https://www.ici-grenoble.org/article/pas-denfants-a-la-rue



Ce mercredi 17 mai 2023 à 14h devant la Maison du Département (32 rue de New York),
l’inter-collectifs des écoles occupées de l’agglomération grenobloise
organise un rassemblement contre le manque de soutien social du
Département de l'Isère.

Pourquoi ? Voici l'appel à manifester :

"Depuis de nombreux mois maintenant, des parent·es et personnel·les de
l'éducation nationale ont mis à l'abri des élèves sans toit et leur
famille dans des écoles.

Aujourd'hui encore, 7 écoles sont ainsi "occupées" : nous n'acceptons
toujours pas que nos élèves, les camarades de nos enfants, dorment dehors !

Mais cela ne suffit pas : nos initiatives ne peuvent se substituer aux
pouvoirs publics.

De plus, un nombre important de personnes, dont des enfants, dorment encore
aujourd'hui à la rue en Isère, ou dans des conditions d'extrême précarité : voitures,
squats, hébergements provisoires (proposés par des institutions ou des
citoyen·nes solidaires)...

**Nous évaluons à près de 400 ce nombre d'enfants !**

Nous avons alerté à de multiples reprises l’État, le Conseil Départemental,
la Métro, les mairies de l'agglomération, et leurs réponses sont, pour
les meilleures, très en-deça de l'indispensable.

Nous rencontrerons une nouvelle fois la mairie de Grenoble mardi 16 mai,
et nous lui redemanderons de nouveau d'appliquer la réquisition des
logements vides votée par son conseil municipal il y a presque un an.

Le Conseil départemental, de son côté, se cache derrière une vision
scandaleusement restrictive de ses compétences en terme de sauvegarde
de l'enfance.

C'est pourquoi nous irons le mercredi 17 mai remettre à M.Barbier une
lettre ouverte pour exiger que l'Isère prenne sa part de responsabilité
dans la mise à l'abri de nos élèves et de leurs familles.

Nous vous invitons donc à une conférence de presse et à un rassemblement
mercredi 17 mai devant les locaux des services sociaux du Département,
avenue de New York, à 14h.

Nous en profiterons pour faire publiquement le bilan de notre rencontre
de la veille avec la mairie de Grenoble."

Parce que, encore et toujours : un toit, une école, des papiers !

À 14h

Devant la Maison du Département
32 rue de New York
Grenoble

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.7107770442962655%2C45.18347365423336%2C5.714317560195923%2C45.18502197324629&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.18425/5.71255">Afficher une carte plus grande</a></small>



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_17.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-17-mai-1990-l-oms-retire-l-homosexualite-de-la-liste-des-maladies-mentales
- https://www.ici-grenoble.org/evenement/ateliers-faire-soi-meme
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- |important| 14h00 https://www.ici-grenoble.org/evenement/rassemblement-pas-denfants-a-la-rue-a-grenoble
- |important| 14h00 https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-postier-e-s-de-grenoble-en-lutte-1
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 17h00 https://www.ici-grenoble.org/evenement/soiree-contre-les-lgbtia-phobies
- 18h10 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 19h15 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 20h45 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative

Demosphere 38
=================

.. figure:: images/demo_2023_05_17.png
   :align: center

   https://38.demosphere.net/

- 07h00 https://38.demosphere.net/rv/1181 (Piquet de grève à Coliposte - 7h (88 Av. Rhin & Danube)
- 14h00 https://38.demosphere.net/rv/1180 (Rassemblement - 14h Bureau de poste Chavant (7 Bd Lyautey))
- 17h30 https://38.demosphere.net/rv/1177 (Rassemblement du CMAP contre la loi retraites)


|rebellion| 2023-05-17 18h30-21h30 Une initiation à l'action non-violente organisée par XR Grenoble et Alternatiba Grenoble aura lieu le mercredi 17 mai  à Grenoble
=========================================================================================================================================================================

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Une initiation à l'action non-violente organisée par XR Grenoble et Alternatiba Grenoble aura lieu le mercredi 17 mai  à Grenoble.

Merci de remplir ce formulaire pour vous y inscrire :

👉 https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117 👈

Le lieu et les horaires précis de la formation vous seront communiqués par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr

.. figure:: images/planning_xr.png
   :align: center

   https://framaforms.org/inscriptions-printemps-des-formations-alternatiba-xr-1681292117


