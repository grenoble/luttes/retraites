
.. _actions_2023_05_13:

=============================================================================================================================================================
**Actions du samedi 13 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #SolidaritéInternationale #Afrique #Iran #Ukraine**
=============================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #SolidaritéInternationale #Afrique #Iran #Ukraine



|ici_grenoble| A la une **Saïd Bouamama à Grenoble**
============================================================================================================


.. figure:: images/page_une_ici_grenoble_2023_05_13.png
   :align: center

   https://www.ici-grenoble.org/article/said-bouamama-a-grenoble, https://www.ici-grenoble.org/evenement/rencontre-avec-said-bouamama-pour-un-panafricanisme-revolutionnaire



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_13.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 14h00 https://www.ici-grenoble.org/evenement/atelier-patriarchie-anger-management-face-aux-masculinites-toxiques-comment-creer-une-rage-room-feministe
- 14h00 https://www.ici-grenoble.org/evenement/discussion-sur-la-contraception-testiculaire-et-fabrication-de-sous-vetements-contraceptifs
- |important| 14h30 🇮🇷 |JinaAmini| https://www.ici-grenoble.org/evenement/rassemblement-contre-les-executions-en-iran-pour-la-liberation-de-toomaj-salehi
- |important| 14h30 |afrique| https://www.ici-grenoble.org/evenement/rencontre-avec-said-bouamama-pour-un-panafricanisme-revolutionnaire
- 15h00 https://www.ici-grenoble.org/evenement/rassemblement-pour-la-visibilite-de-lencephalomyelite-myalgique-lyon
- |important| 15h00 🇺🇦 |mriya| https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien

  - https://www.insideoutproject.net/fr/explore/group-action/on-vit-la-guerre-dukraine-parmi-vous

- 17h20 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 17h20 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 19h00 https://www.ici-grenoble.org/evenement/conference-le-secret-defense-ca-concerne-tout-le-monde
- |important| 19h00 https://www.ici-grenoble.org/evenement/resto-malap-repas-congolais-vegan-cuisine-par-des-personnes-sans-papiers

Demosphere 38
=================

.. figure:: images/demo_2023_05_13.png
   :align: center

   https://38.demosphere.net/


- 08h00 https://38.demosphere.net/rv/1057 (Manifestation pour un Nouvel Elan des Services Publics à LURE le 13 mai 2023)

  - https://38.demosphere.net/files/docs/a02213bbbfd5819.pdf

2023-05-13 |rebellion| Un arpentage sur la structure actuelle d’extinction rébellion Grenoble organisée par XR Grenoble et Alternatiba Grenoble
==================================================================================================================================================

Un arpentage sur la structure actuelle d’extinction rébellion Grenoble
organisée par XR Grenoble et Alternatiba Grenoble aura lieu le mercredi
13 mai au Bocal d'alternatiba, le long de l'esplanade à Grenoble.

Participe à un arpentage de la Structuration d'XR Grenoble pour connaitre
le fonctionnement du Groupe Local en un clin d'œil !

Merci de remplir ce formulaire pour vous y inscrire :

👉 grenoble.alternatiba.eu/printempsdesformations/ 👈

Le lieu et les horaires précis de la formation vous seront reconfirmés par email.

Pour être au courant des prochains événements  XR Grenoble : https://listes.extinctionrebellion.fr/subscription/YDieIERkgN

Être au courant des prochains événements Alternatiba Grenoble : https://grenoble.alternatiba.eu/

Pour en savoir plus sur Extinction Rebellion, nous rejoindre : https://extinctionrebellion.fr/rejoignez-nous/

🙋Des questions ? écris nous à grenoble@extinctionrebellion.fr


2023-05-13 Conférence : Le secret-défense, ça concerne tout le monde
==========================================================================

- https://www.ici-grenoble.org/evenement/conference-le-secret-defense-ca-concerne-tout-le-monde
- http://collectifsecretdefense.fr/

.. figure:: images/affiche_secret_defense.png
   :align: center

   https://www.ici-grenoble.org/evenement/conference-le-secret-defense-ca-concerne-tout-le-monde, http://collectifsecretdefense.fr/



Introduction
----------------

Rencontre avec François Graner, co-auteur avec R. Doridant de L’État français
et le génocide des Tutsis au Rwanda (Agone, 2020), et membre du collectif
Secret défense - un enjeu démocratique au titre de l'association Survie.

Voici la présentation de cette soirée
-----------------------------------------

"Le secret-défense : un enjeu démocratique qui concerne tout le monde.

En France, l'accès à certains documents, autant militaires que civils,
est limité au nom de la raison d’État, censée représenter l’intérêt
supérieur de la nation.

Dans diverses affaires judiciaires, l’État français use de manœuvres
pour entraver la recherche de la vérité par les familles, les historiens,
les chercheurs et pour empêcher que justice soit rendue aux victimes.

Certaines de ces affaires peuvent concerner n'importe qui, comme celles
concernant des accidents de bâtiments, de bateaux ou d'avions.

D'autres sont très politiques ; on présentera comme exemple des affaires
du passé, comme le rôle de la France au Burkina-Faso en 1987 ou au Rwanda
entre 1990 et 1994, et d'autres d'une actualité brûlante, comme **l'assassinat
de militants kurdes en plein Paris**.

Dans tous les cas, cela pose une question qui concerne tout le monde,
celui du rapport de forces entre gouvernants et gouvernés."

::

    Ouverture des portes : 19h
    Présentation : 20h
    Prix libre
    À Antigone
    22 rue des violettes
    Grenoble

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.706380903720857%2C45.17860912530213%2C5.709921419620515%2C45.18015757661131&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.17938/5.70815">Afficher une carte plus grande</a></small>


