
.. _actions_2023_05_23:

===================================================================================================================================================================
**Actions du mardi 23 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Greve #AgEcoloAnticapitaliste #RassemblementDesGlieres**
===================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Greve #AgEcoloAnticapitaliste #RassemblementDesGlieres


|ici_grenoble| A la une **L'AG écolo anticapitaliste**
=====================================================================

- https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste


.. figure:: images/page_une_ici_grenoble_2023_05_23.png
   :align: center

   https://www.ici-grenoble.org/article/lag-ecolo-anticapitaliste, https://www.ici-grenoble.org/newsletter


Face au désastre climatique, vous pensez qu'il ne reste que deux scénarios
possibles : l'aggravation ou le renversement du système capitaliste ?

Ce mardi 23 mai à 18h30 au 38 rue d'Alembert, c'est l'Assemblée Générale
Écolo Anticapitaliste de Grenoble.

Qui l'organise ? Dans quel but concret ?

Voici l'invitation :

"Cette assemblée générale a vu le jour à l'occasion d'une manifestation
d'opposition à la mascarade Grenoble Capitale Verte européenne 2022.

Nous sommes composé·es d'organisations, de collectifs, de partis politiques,
mais aussi d'individus, qui ensemble, souhaitons porter des revendications
écologiques anticapitalistes.

L'écologie et l'anticapitalisme sont pour nous indissociables : le capitalisme
se fonde sur l'accumulation, et donc l'exploitation, de ressources qui se
font de plus en plus rares, au profit d'entreprises ou de patrons qui
s'enrichissent de plus en plus sur le dos des travailleur·euses et des
personnes précaires.

Il est impossible de croire en un capitalisme vert pour nous sauver de
la crise écologique : les solutions techniques cherchent en réalité à
ouvrir de nouveaux marchés lucratifs sans remettre en question le
principe-même du capitalisme qui est à la racine des problèmes environnementaux.

Nous avons besoins de retrouver une autonomie et une souveraineté collective
pour pouvoir faire des choix de société cohérents avec les limites planétaires.

Or cela est inconcevable si les décisions économiques et politiques sont
prises par une classe dont les intérêts divergent de ceux du bien commun.

Combattre le capitalisme c'est aussi faire la critique d'une idéologie
propagée par les classes dominantes qui promeut la consommation et un
mode de vie débridé comme marqueurs de réussite sociale.

Nous souhaitons lutter contre cette vision du monde et ces normes qui
doivent êtres changées pour aller vers une sobriété qui ne soit pas subie
mais désirée car profitable à tout le monde.

Nous avons fait le choix de nous organiser en AG pour permettre à des
individus aussi bien qu'à des personnes mandatées par leur collectif de
rejoindre l'organisation.

Ce cadre unitaire nous permet d'avoir plus de force pour mener cette
lutte transversale, et de rattacher et mêler des causes diverses qui ne
se croisent pas facilement dans d'autres espaces.

Le but n'est pas de remplacer les personnes déjà en lutte mais d'être un
outil de soutien et de renforcement.

Nos opinions peuvent diverger mais nous nous rejoignons sur ces revendications
que nous souhaitons porter à chacune de nos apparitions publiques.

Nous revendiquons :

- un accès mutualisé et inconditionnel à tous les besoins vitaux pour tous·tes.
  Ce sont les plus riches qui polluent et consomment le plus, impactant
  les personnes les plus précaires qui se retrouvent privées des ressources
  nécessaires à leur survie (eau, nourriture, logement, santé, transport, etc.).

- des transports en commun gratuits et la suppression des ZFE qui précarisent
  encore plus

- la réquisition des logements vides et la rénovation des logements sans
  hausse de loyer, notamment les moins bien isolés

- la fin de l’artificialisation des sols

- la fin des grands projets urbains écologiquement condamnables qui ne
  répondent qu'à des logiques de croissance et de rayonnement économiques.
  D'autant plus quand ces projets s'accompagnent d'une gentrification des quartiers.

Pour cela, nos moyens d'action sont :

- le renforcement et le soutien des luttes locales

- la construction d'un cadre unitaire large d'organisations, d'individus,
  de collectifs qui permet de partager, échanger, et construire des
  revendications communes et unies

- l'apparition lors de manifestations ou l'action conjointe pour construire
  un rapport de forces sur le territoire

- le relai d'informations

Rejoins-nous et viens nous aider à construire la lutte !"

À 18h30

Au Centre Social « Tchoukar »
38 rue d'Alembert
Grenoble


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_23.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies, https://www.solidarites-grenoble.fr/
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 12h00 https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- |important| 14h00 https://www.ici-grenoble.org/evenement/manifestation-des-salari-e-s-du-social-et-medico-social
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- 18h00 https://www.ici-grenoble.org/evenement/binary-is-for-computer-permanence-informatique-a-la-b-a-f-depannages-installation-de-linux-cryptage-de-donnees
- |important| 18h30 https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste
- 18h30 https://www.ici-grenoble.org/evenement/cine-discussion-ni-dieu-ni-maitre-documentaire-passionnant-sur-lhistoire-de-lanarchisme
- |important| 18h30 https://www.ici-grenoble.org/evenement/rassemblement-contre-loperation-wuambushu-a-mayotte
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative


2023-05-23 18h30 Nouvel atelier EGE (Etats Généraux de l’Ecologie) à la Villeneuve de Grenoble !
===================================================================================================

- https://framapiaf.org/@AnneTourmen/110359984721219645
- https://metro-grenoble.eelv.fr/etats-generaux-de-lecologie-a-la-villeneuve-grenoble/

Après Saint-Egrève et Sassenage, nouvel atelier EGE (Etats Généraux de l’Ecologie)
à la Villeneuve de Grenoble !


📆mardi 23 mai
⏰18h30

📍café associatif le Barathym, 97 galerie de l’Arlequin 38100

#eelv #grenoble #isere #ecologie #etatsgenerauxdelecologie


|rebellion| Formation sécurité et hygiène numérique
===========================================================

- https://mobilizon.extinctionrebellion.fr/events/4ce5d216-45ed-4915-8c9f-00ce97f27c69

Ne clique pas sur "Participer": pas d'inscription nécessaire pour cette formation :)

Quels sont les bons réflexes à adopter lorsqu'on utilise des outils
numériques dans nos activités militantes ?

Comment se protéger les un.e.s les autres de la surveillance et de la répression ?

Cette formation te donnera des outils pour avoir un usage sécurisé du
numérique dans tes activités militantes, et mieux comprendre les risques
associés aux différents usages.

Elle aura lieu entièrement en ligne le 23 mai de 19h à 20h30.

Pour te connecter, pas besoin d'inscription, il te suffit de bien noter
l'événement dans ton agenda et de cliquer sur ce lien le jour J.

A bientôt, avec amour et rage!

Compte rendu partiel du rassemblement des Glières (à compléter)
===================================================================

Toujours beaucoup d'émotion sur le plateau des Glières.

Quelques photos et videos ici: https://glieres.frama.io/crha/resistances-2023/

.. figure:: images/plateau_des_glieres.png
   :align: center

   https://glieres.frama.io/crha/resistances-2023/

.. figure:: images/plateau_des_glieres_2.png
   :align: center
