
.. _actions_2023_05_28:

=========================================================================================================================================
**Actions du dimanche 28 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/annule-festival-ecofeministe-bien-lbourgeon (ANNULE)
- 10h00 https://www.ici-grenoble.org/evenement/this-is-the-end-brocante-de-la-baf-tout-doit-disparaitre
- 14h00 https://www.ici-grenoble.org/evenement/balade-botanique-a-prix-libre
- 14h00 https://www.ici-grenoble.org/evenement/balade-botanique-dans-le-trieves-decouvrir-les-plantes-sauvages-et-leurs-usages
- 14h00 |car38| https://www.ici-grenoble.org/evenement/formation-anti-repression-avancee-que-faire-en-cas-de-garde-a-vue
- 19h00 https://www.ici-grenoble.org/evenement/concert-de-soutien-au-festival-ecofeministe-bien-lbourgeon


Demosphere 38
=================

.. figure: images/demo_2023_05_01.png
   :align: center

   https://38.demosphere.net/

Concert de soutien au festival écoféministe "Bien l'Bourgeon"
=================================================================

- https://www.ici-grenoble.org/evenement/concert-de-soutien-au-festival-ecofeministe-bien-lbourgeon



Ce concert est organisé par l'association Mix'Arts, en soutien pour le
festival Bien l'Bourgeon. C’est avec une grande déception et amertume
que l’équipe de Mix’Arts vous annonce l’annulation du festival Bien l’Bourgeon 2023
qui devait avoir lieu les 26, 27 et 28 mai prochains à Saint-Geoire-en-Valdaine (38).

En effet, le niveau des préventes ne nous permet pas d’aborder cette 6ème
édition sereinement et laisse planer trop d'incertitudes de remplissage
qui pourraient mettre en péril le festival mais également notre association
et la dizaine de salarié·es qui la font vivre au quotidien.

Mais pour se consoler, nous vous proposons tout de même un petit bout de
ce Festival en délocalisé à Grenoble avec deux artistes sur les 19 initialement
prévues, mais non des moindres :

> MERYL • Hip-hop, rap • France Figure emblématique de la scène française,
Meryl est enfin de retour ! Retrouvez-la pour la première fois sur scène
avec des musiciens lors d'une tournée dans toute la France, sur les festivals
les plus prestigieux et pour son premier concert parisien à l'Elysée Montmartre
le 14 avril 2023.

> SARA HEBE • Hip-hop, rap, reggaeton • Argentine Considérée par les critiques
comme « la rappeuse la plus remarquable du millénaire en Argentine et
l’une des jeunes artistes clés de cette époque »,

Sara Hebe est une invitation à rebondir, à sauter, à lever les poings, à
cumbia-shaker vos hanches. Sa voix et ses performances scéniques puissantes
bouleversent tous les lieux et festivals.

À 19h

À la Belle électrique

