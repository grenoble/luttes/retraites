
.. _actions_2023_05_02:

=========================================================================================================================================
**Actions du mardi 2 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_02.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 12h: https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- 12h: https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 15h45 🎥 https://www.ici-grenoble.org/evenement/cinema-relaxe-sur-laffaire-tarnac
- 17h00: https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- 17h30 🎥 https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- 18h00 https://www.ici-grenoble.org/evenement/rencontres-les-differences-au-sein-dun-groupe-dhabitat-partage-richesse-ou-contrainte
- |important| 18h00 |car38| https://www.ici-grenoble.org/evenement/rencontres-la-repression-syndicale-dans-le-monde

      - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 18h00 🎥 https://www.ici-grenoble.org/evenement/cine-choc-a-pas-aveugles-sur-les-photos-clandestines-des-camps-de-la-mort-nazis
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- |important| 19h00 https://www.ici-grenoble.org/evenement/reunion-du-comite-grenoblois-des-soulevements-de-la-terre
- 19h00 https://www.ici-grenoble.org/evenement/soiree-enjeux-communs
- 20h30  🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 20h45  🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h45  🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines


Demosphere 38
=================

.. figure:: images/demo_2023_05_02.png
   :align: center

   https://38.demosphere.net/


- |important| 18h00 https://38.demosphere.net/rv/1046 (mardi 2 mai 2023 à 18h, LA REPRESSION SYNDICALE DANS LE MONDE)
- |important| 18h30 https://38.demosphere.net/rv/1153 (Assemblée générale de luttes au 38 )

--
