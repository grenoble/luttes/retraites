
.. _actions_2023_05_21:

=========================================================================================================================================
**Actions du dimanche 21 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RassemblementDesGlieres**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RassemblementDesGlieres


|ici_grenoble| **Agenda ici-grenoble**
============================================

.. figure:: images/ici_grenoble_2023_05_21.png
   :align: center

   https://www.ici-grenoble.org/agenda


- |crha| https://www.ici-grenoble.org/evenement/rassemblement-des-glieres-conferences-films-concerts-de-resistances
- |important| 16h00 https://www.ici-grenoble.org/evenement/assemblee-ouverte-pour-la-requisition-des-logements-vides-a-grenoble
- 16h00 https://www.ici-grenoble.org/evenement/spectacle-au-temps-des-papillons-des-meres-pour-changer-le-monde-changeons-la-facon-dont-nous-naissons
- 22h00 https://www.ici-grenoble.org/evenement/coup-de-coeur-quest-ce-qui-pourrait-sauver-lamour-nouveau-podcast-dovidie


.. _glieres_2023_05_21:

|crha| Dimanche 21 mai 2023 10h30 **Rassemblement des Glières**
===============================================================================================================

- https://citoyens-resistants.fr/spip.php?article659
- https://citoyens-resistants.fr/IMG/pdf/tract_recto_verso_impression_crha_2023.pdf
- https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf


Prises de parole au plateau des Glières
------------------------------------------

- https://fr.wikipedia.org/wiki/Plateau_des_Gli%C3%A8res
- https://www.geoportail.gouv.fr/carte?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes
- https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres


.. figure:: images/monument_des_glieres.png
   :align: center
   :width: 500

   https://fr.wikipedia.org/wiki/Monument_national_%C3%A0_la_R%C3%A9sistance_du_plateau_des_Gli%C3%A8res


.. raw:: html

   <iframe width="800" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin" src="https://www.geoportail.gouv.fr/embed/visu.html?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen>
   </iframe>


   https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres

.. figure:: images/glieres_nouvelles_nouveaux_resistantes.png
   :align: center


Angélique Huguin
++++++++++++++++++++

Opposée au projet d’enfouissement de déchets nucléaires à Bure, en Meuse,
elle fait partie des militants mis en examen pour « Association de malfaiteurs ».

Anthony Smith
+++++++++++++++++


Inspecteur du travail dont la sanction disciplinaire a été annulée par
le tribunal de Nancy en octobre dernier.

George Ibrahim Abdallah
+++++++++++++++++++++++++++

Plus vieux prisonnier politique d’Europe.
Texte de prison lu par Nicolas Berry, membre du comité de soutien et réalisateur.

Julien Le Guet
+++++++++++++++++

Porte-parole du mouvement Bassines non merci, gardé à vue, puis sous
contrôle judiciaire pour ses actions.

Lincoyan Huilcamán
++++++++++++++++++++++

Résistant et orateur chilien du peuple Mapuche

Michèle Agniel
+++++++++++++++++

Résistante dès 1940, arrêtée, déportée. (Lecture d’un texte qu’elle nous a transmis)

Paul François
++++++++++++++++

Céréalier intoxiqué par un herbicide Monsanto en 2004 qui a obtenu au
final la condamnation de Bayer. Physiquement agressé chez lui début 2023.

Pinar Selek
++++++++++++++++

- Pinar Selek  (https://pinarselek.fr/biographie/, https://nitter.cutelab.space/Pinar_Selek/rss)

Acquittée à quatre reprises dans la même affaire, elle est la victime
d’un acharnement sans précédent dans l’histoire judiciaire de la Turquie,
dans le cadre d’un procès politique qui dure depuis un quart de siècle.

Sophie Djigo
+++++++++++++++

Enseignante de philosophie menacée par l’extrême droite pour son travail
sur les migrations avec ses élèves.



|crha| Dimanche 21 mai 2023 **2 Films au cinéma Le parnal à Thorens Glières**
-----------------------------------------------------------------------------------


.. figure:: images/glieres_films_dimanche.png
   :align: center

   https://www.helloasso.com/associations/citoyens-resistants-d-hier-et-d-aujourd-hui/formulaires/1/widget


.. figure:: ../19/images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842

Dimanche 21 mai 2023 15h00 au cinéma Le parnal **Eau secours** en présence de Julia Blagny
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- http://blagny.fr/eau-secours-des-andes-a-lamazonie/
- https://vimeo.com/642722285?embedded=true&source=vimeo_logo&owner=4965346

L'eau est la source de toute forme de vie.

Elle a dessiné la Terre et l'histoire de l'Humanité.

Mais elle peut devenir dangereuse si l'on cherche à contrôler son flux.
Elle peut même devenir poison quand les hommes y déversent mercure, arsenic...

Les rivières de Bolivie, qui sont les veines du cœur de l'Amérique du
Sud, se teignent de rouge, de gris et vont même jusqu'à disparaître.
Les habitants de différents peuples de Bolivie et des spécialistes nous
avertissent : l'accès à une eau potable est chaque année plus difficile.
Des animations et des musiques originales nous entrainent d'un fleuve à
l'autre.

Si nous ne traitons pas l'eau avec respect, nous voguons à notre perte,
en Bolivie comme ailleurs.
Ce documentaire ouvre une fenêtre sur une réalité bolivienne et illustre
une urgence mondiale.
Il peut être un support pour partager nos réflexions et unir nos efforts
pour construire des alternatives, maintenant.

::

    Bolivie/France – 2021 – 52 minutes
    Documentaire de Julia Blagny
    Espagnol – Disponible avec sous-titres en français ou en anglais
    Thématiques : Eau – Environnement – Premières Nations – Droits de l'Homme – Pollution – Engagement
    Musiques originales : Antonio Perez – Marco Antonio Peña
    Dessins – Angèle Charbouillot


Dimanche 21 mai 2023 17h00 au cinéma Le parnal **Reprise en mains** en présence de Gilles Perret
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://jour2fete.com/film/reprise-en-main/
- https://nitter.net/jour2fete/rss
- https://vimeo.com/727929889?embedded=true&source=vimeo_logo&owner=6973199

Avec : Pierre Deladonchamps, Laetitia Dosch, Grégory Montel, Vincent Deniard,
Finnegan Oldfield, Samuel Churin, Marie Denarnaud

Comme son père avant lui, Cédric travaille dans une entreprise de mécanique
de précision en Haute-Savoie.

L’usine doit être de nouveau cédée à un fonds d’investissement.

Epuisés d’avoir à dépendre de spéculateurs cyniques, Cédric et ses amis
d’enfance tentent l’impossible : racheter l’usine en se faisant passer
pour des financiers !


