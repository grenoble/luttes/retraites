.. index::
   pair: Base d'Organisation Collective des ALternatives ; BOCAL (2023-05-14)
   pair: Documentaire ; Cauchemar dans les Alpes (32min) de Jamshid Golmakani (Iran)

.. _actions_2023_05_14:

=========================================================================================================================================
**Actions du dimanche 14 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Alternatiba #NonALesclavage**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Alternatiba #NonALesclavage



|ici_grenoble| A la une **Les mégapompeurs de Crolles**
============================================================================================================


.. figure:: images/page_une_ici_grenoble_2023_05_14.png
   :align: center

   https://www.ici-grenoble.org/article/les-megapompeurs-de-crolles


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_14.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 11h00 https://www.ici-grenoble.org/evenement/atelier-de-broderie-feministe
- 14h00-> 22h00 https://www.ici-grenoble.org/evenement/inauguration-du-bocal-de-grenoble-base-dorganisation-collective-des-alternatives, au 8 boulevard de l'Esplanade
- 17h35 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- https://www.ici-grenoble.org/evenement/coup-de-coeur-quest-ce-qui-pourrait-sauver-lamour-nouveau-podcast-dovidie

Demosphere 38
=================

.. figure:: images/demo_2023_05_14.png
   :align: center

   https://38.demosphere.net/

- 12h00 https://38.demosphere.net/rv/1168 (Pique-nique/débat pour bilan
  et perspectives de la lutte contre la réforme des retraites et luttes de quartier,
  **Parc Villeneuve, place rouge**)


.. figure:: images/villeneuve.png
   :align: center
   :width: 500

   https://www.openstreetmap.org/#map=18/45.16379/5.73471


.. figure:: images/place_rouge.png
   :align: center

   https://www.openstreetmap.org/#map=18/45.16379/5.73471


.. _bocal_2023_05_14:

|alternatiba| **Dimanche 14 mai 2023 de 14 à 22h au 8 boulevard de l'Esplanade, Alternatiba Grenoble ouvre un nouveau lieu de luttes : la Base d'Organisation Collective des ALternatives (BOCAL)**
========================================================================================================================================================================================================

- https://www.ici-grenoble.org/evenement/inauguration-du-bocal-de-grenoble-base-dorganisation-collective-des-alternatives
- https://grenoble.alternatiba.eu/2023/05/04/inauguration-bocal/
- https://grenoble.alternatiba.eu/le-bocal/
- https://www.openstreetmap.org/#map=18/45.19757/5.71665


Dimanche 14 mai 2023 au 8 boulevard de l'Esplanade, Alternatiba Grenoble ouvre
un nouveau lieu de luttes : la Base d'Organisation Collective des
ALternatives (BOCAL).

Il pourra accueillir des événements de plus grande envergure que la Base
d'Action Sociale et Ecologique (BASE), le bar associatif lancé par
Alternatiba en 2022, rue du Dauphiné.

Au programme de l'inauguration : concerts, ateliers, jeux...

Tous les détails sont `ici <https://grenoble.alternatiba.eu/2023/05/04/inauguration-bocal/>`_

.. figure:: images/inauguration_bocal.png
   :align: center

   https://grenoble.alternatiba.eu/2023/05/04/inauguration-bocal/


https://grenoble.alternatiba.eu/2023/05/04/inauguration-bocal/
---------------------------------------------------------------

Nous avons le plaisir, que disons-nous, l’honneur, de pouvoir (enfin)
vous présenter le tout nouveau lieu d’Alternatiba : le BOCAL (Base
d’Organisation pour les Alternatives) pour une super méga inauguration
le dimanche 14 mai 2023.

📣 De 14h à 22h on fera la part belle aux associations, collectifs et
personnes qui trouvent des solutions écologiques et de justice sociale
tout en faisant la fête pour grands et tout-petits. Expositions, ateliers
de permaculture, jeux, friperie et peinture, on vous présente les activités
dans les jours à venir

📯 Jusqu’auboutistes, nous pourrons aussi et surtout nous acoquiner de
musique et de militantisme lors de cette journée de lancement grâce à
une programmation musicale

🎈Infos pratiques
----------------------

💸 COMBIEN ? Gratuit (mais pensez à apporter du liquide ou CB pour les différentes activités !)
📒 QUAND ? De 14h à 22h
📌 OU ? 8 Bd de l’Esplanade – 38000 Grenoble🌶️🌶️ LE LIEU DE VIE (LE BOCAL) EN BREF 🌶️🌶️
🍻 Le BOCAL vous proposera de la délicieuse bière locale ainsi que des boissons soft produites par la brasserie la Furieuse et du Mont Blanc
🍰 Nourriture végé/végan, locale, pas chère


🎉 Organisations partenaires
---------------------------------------

STOP METROCABLE
+++++++++++++++++

Le collectif SMC est pluriel : ce sont des habitant.es de la métropole
soucieux.ses d’aller vers une meilleure utilisation des espaces publics
et des ressources, vers davantage de justice sociale, afin de faire face,
entre autres, au dérèglement climatique, à la crise écologique… : le collectif
lutte contre le projet MétroCâble.

EXTINCTION REBELLION
+++++++++++++++++++++++++

Extinction Rebellion (XR) est un mouvement mondial de désobéissance civile
non-violente en lutte contre l’effondrement écologique et le réchauffement climatique.

CONVERGENCE VELO
+++++++++++++++++++

La Convergence Vélo est une manifestation cyclistes, qui se veut avant
tout festive et conviviale. Un bon moment pour rouler à vélo avec d’autres
habitant.es de la région grenobloise à un rythme très tranquille et sur un
trajet sécurisé.

C’est également une manifestation revendicative : on veut plus d’aménagements
vélo ! Le territoire grenoblois dispose d’un réseau cyclable qu’il faut
continuer à développer et à sécuriser.

Il reste beaucoup à faire pour assurer la continuité des parcours et
garantir la sécurité de ses usagers.

Ouverte à toutes et tous.
Vous pouvez rejoindre la Convergence parmi les 50 étapes réparties sur
les 8 parcours proposés, il y en a forcément une qui est proche de chez vous.
Toutes les infos sur https://www.convergence-velo-grenoble.fr/

STOP MICRO
++++++++++++

StopMicro est un collectif qui lutte contre l’accaparement des ressources
et les nuisances causées par les industries locales, en particulier celles
de la microélectronique. Le collectif est né d’une volonté de recentrer
le débat public sur l’impact environnemental de l’industrie, à une époque
où on nous responsabilise individuellement de plus en plus via l’injonction
aux petits gestes : faire pipi sous la douche, baisser son chauffage etc.

Par nos actions, nous voulons rappeler que derrière le dérèglement climatique
et les injustices socio-environnementales, il y a des décisions politiques,
des entreprises et des intérêts économiques très forts.

Nous voulons également rappeler que la cuvette de la “capitale verte européenne”,
ce sont aussi des entreprises hautement énergivores et polluantes
(pas moins de 19 usines classées Seveso) ainsi que le premier pôle européen
de nano-technologies, un secteur extrêmement énergivore qui pose de
nombreux enjeux environnementaux et sociétaux.

MICROFORGE
++++++++++++

Microforge est un petit atelier de fabrication basé à Saint Martin d’Hères,
qui travaille sur divers sujets, principalement autour du recyclage plastique
et de conception de remorques à vélo.

PATRIARCHIE
+++++++++++++

Patriarchie est une association grenobloise créée en 2021 qui aborde des
thématiques engagées de manière décalée et ludique.

Notre objectif est de démocratiser et rendre accessibles les questionnements
autour des luttes sociales contemporaines au plus grand nombre grâce à
Patriarchie Magazine et les ateliers. A travers la collaboration, la
pratique culturelle et artistique, et une bonne tranche de rigolade.

Patriarchie est ouverte à touste et porte fièrement et sans compromis des
valeurs féministes, d’inclusivité et d’intersectionnalité.


📯 Programmation musicale
--------------------------------

La CAGETTE (ou “Chorale Autogérée de Grenoble et Tout Territoire Environnant)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

C’est une chorale grenobloise, militante et autogérée qui se retrouve chaque
lundi au centre social le 38 pour répéter. L’objectif est de s’amuser, mais
aussi de se retrouver pour chanter en manif ou à des évènements de soutien
pour celles et ceux qui veulent.

Notre répertoire est varié : il se compose de chants militants, mais pas que !

On chante aussi des chants du répertoire occitan, espagnol, et italien,
non militants, mais très jolis !

Toutes les répètes sont gratuites, et aucun niveau en chant n’est attendu.

MËMO
+++++++

En quelques chants, Mëmo nous fait revivre un tour du monde alternatif
avec ses rencontres, ses joies et ses peines, et nous donne le goût du
voyage local à la découverte de soi.

Les mots de Fanny Boëgeat prennent vie lorsqu’avec Caroline Vatillieux,
Nejma Frej, Rémy Boëgeat et Diandra Fabre, elle se met à chanter des
mélodies qui s’impriment dans nos têtes et nous portent pour aller de l’avant.

Au rythme des congas le piano danse, et voilà qu’une trompette et une flûte
nous emportent dans un autre paysage, un monde où tout est possible : notre monde !


TURBO BOOM BOOM
++++++++++++++++++++

Explorateur musical passionné et sans limites, réchauffeur climatique de
dancefloors et ambassadeur de l’ambiance, aussi bien à l’écoute des
dernières tendances de la musique actuelle qu’à la recherche des trésors
oubliés des discothèques mondiales, Turbo Boom-Boom envoûte les publics
avec des sets éclectiques et pointus.

Ses sélections explorent les territoires, les traditions, les rythmes et
les époques : des percussions ancestrales africaines aux rythmiques pré-hispaniques
disparues de la cordillère des Andes, de l’afrobeat nigériane à la highlife
ghanéenne en passant par l’afro-disco et l’afro-funk de la Côte d’Ivoire,
du makossa camerounais à la soukous du Congo et le benga du Kenya,
de l’afro-synth au bubblegum en passant par l’afro-house sud-africaine
et le kweito, du zouk digital au boogie, de la cadence antillaise au
funaná du Cap-Vert, du gwo ka de Guadeloupe au maloya de la Réunion,
du kuduro de l’Angola au baile funk des favelas brésiliennes, de la
cumbia colombienne à la chicha péruvienne, du bullerengue amérindien au
palenque en passant par la champeta des sound-systems afro-colombiens …

Et proposent un métissage avec les musiques électroniques actuelles :
bass music, down-tempo, dub, house, deep house, techno …

PAVILLON
++++++++++

Éveillé par la minimale et la Trance de la scène musique électronique
grenobloise, Pavillon c’est une techno mélodique planante qui transporte
et fait taper du pied. Une musique où les synthétiseurs analogiques et
les machines font écho aux premières références de la musique électronique
jusqu’à l’acid des 90’s.

Un objectif : vous emmener le temps d’un instant dans un univers où la
danse et la mélodie règnent en maître. Influencé par Boris Brejcha, Weval,
ou encore Binary Digit : Pavillon, nouveau projet émergent à suivre de
la scène techno Grenobloise.



.. figure:: images/le_bocal.png
   :align: center

   https://www.openstreetmap.org/#map=18/45.19757/5.71665

.. figure:: images/carte_bocal.png
   :align: center

   https://www.openstreetmap.org/#map=18/45.19757/5.71665


.. _jamshid_golmakani_2023_05_14:

Dimanche 14 mai 2023 de 14h00 à 15h00  🎥 Projection du film documentaire Cauchemar dans les Alpes (32min) de Jamshid Golmakani (Iran), place Victor Schoelcher, SEYSSINS
==============================================================================================================================================================================

- https://standup4humanrights.org/en/index.html
- https://www.ohchr.org/en/get-involved/campaign/fight-racism

:download:`Le programme sur la commémoration de la fin de l'esclavage à Seyssins <pdfs/programme_2023_05_14_seyssins.pdf>`

Salle Régis Prouté PLACE VICTOR SCHOELCHER, SEYSSINS, Commémoration Journée nationale des mémoires
de la traite, de l’esclavage et de leurs abolitions

Projection du film documentaire **Cauchemar dans les Alpes (32min)**
de Jamshid Golmakani (Iran), suivi d’un débat présenté et animé par le
réalisateur et l’association Refuge Solidaire de Briançon, l’organisatrice
des maraudes au Montgenèvre.

Dans le couloir de l’Espace Victor Schoelcher seront disposées une ou
deux expositions photographiques réalisées par l’association Horizons et
son journaliste Jamshid GOLMAKANI (Iran) et l’association Refuge Solidaire
de Briançon, l’organisatrice des maraudes au Montgenèvre.

L’exposition se dénomme Refuge Solidaire de Briançon : une hospitalité rare !

.. figure:: images/jamshid_golmakani.png
   :align: center

   Jamshid GOLMAKANI



.. figure:: images/place_victor_schoelcher.png
   :align: center

   PLACE VICTOR SCHOELCHER, SEYSSINS https://www.openstreetmap.org/#map=19/45.16591/5.68894

Compte rendu
-------------

- :ref:`iran_luttes:utopiran_2023_05_14`
