.. index::
   ! Flagrant Deni

.. _actions_2023_05_25:

==========================================================================================================================================================================================
**Actions du jeudi 25 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #ViolencesPolicières #LuttesLGBTQI #FlagrantDeni #PolicePartoutJusticeNullePart**
==========================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #ViolencesPolicières #LuttesLGBTQI #FlagrantDeni #PolicePartoutJusticeNullePart

|ici_grenoble| A la une **Appel à dons pour "Le 17**
=====================================================================

- https://www.ici-grenoble.org/article/appel-a-dons-pour-le-17
- http://www.flagrant-deni.fr/
- https://www.flagrant-deni.fr/feed/
- https://linktr.ee/flagrantdeni
- https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes

.. figure:: images/page_une_ici_grenoble_2023_05_25.png
   :align: center

   https://www.ici-grenoble.org/article/appel-a-dons-pour-le-17, https://www.ici-grenoble.org/newsletter


En cas de violence policière, comment déposer plainte ?

Comment empêcher un policier sadique de continuer d’exercer ?

Comment obtenir une "réparation" sans attendre la fin d'un procès ?

Aujourd’hui en France, il n’existe aucun guide complet pour répondre aux
victimes de violences policières et soutenir le travail de leurs avocat·es.

Le média lyonnais `Flagrant-déni (http://www.flagrant-deni.fr) <http://www.flagrant-deni.fr/>`_
lance un `appel à dons pour les aider à finaliser "Le 17" <https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes>`_:
un guide pratique et juridique gratuit pour les victimes de violences policières.

Ce guide inédit est le résultat de 4 ans de recherches sur le droit
applicable (jurisprudences et notes internes non publiées) et les
pratiques policières et judiciaires.

Il proposera plus de 100 questions/réponses, des textes précis et sourcés
avec un langage clair, des exemples concrets et des schémas explicatifs.

Pour en savoir plus et participer à l'appel à dons, c'est `ici <https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes>`_.

PS : Les dons à Flagrant-déni sont déductibles des impôts.

**Lancé en 2022 à Lyon, Flagrant-déni.fr est un média indépendant national
ET un collectif de soutien aux victimes de violences policières**

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_25.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 14h00 https://www.ici-grenoble.org/evenement/atelier-couture-aux-petites-cantines, https://grenoble.lespetitescantines.org/
- 15h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 16h30 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- 18h00 https://www.ici-grenoble.org/evenement/atelier-de-francais-a-prix-libre-pour-debutant-es-et-intermediaires
- 18h30 🎥 https://www.ici-grenoble.org/evenement/cine-rencontres-hedera-sur-les-grandeurs-et-les-deceptions-des-luttes-anarchistes, https://www.yiny.org/w/3FhSDVTj49MsmpK5Fx8XjT
- 18h30 🌈 https://www.ici-grenoble.org/evenement/appel-a-benevoles-pour-la-marche-des-fiertes-de-grenoble-prevue-le-1er-juillet
- 19h00 https://www.ici-grenoble.org/evenement/foire-aux-questions-internet-et-le-numerique
- 20h00 🎥 🌈 https://www.ici-grenoble.org/evenement/cine-debat-pride-sur-la-convergence-des-luttes-entre-des-mineurs-et-des-activistes-lgbti
- 20h50 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 21h00 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative


Demosphere 38
=================

.. figure:: images/demo_2023_05_25.png
   :align: center

   https://38.demosphere.net/

- |important| https://38.demosphere.net/rv/1193 18h15 Réunion CMAP , Organisation d'action(s) populaires et revendicatives en vue de fêter la fin des "100 jours" de Macron...  UL-CGT LA MURE
