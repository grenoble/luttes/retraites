.. index::
   pair: Réquisitions; Logements vides

.. _actions_2023_05_07:

=========================================================================================================================================
**Actions du dimanche 7 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Réquisitions**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- https://www.ici-grenoble.org/uploads/medias/2023/02/0223/brochure-requisition-63f78d4c7a300952406772.pdf
- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://100joursdezbeul.fr/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Réquisitions


|ici_grenoble| En une de ici-grenoble Assemblée pour les réquisitions
==========================================================================

- https://www.ici-grenoble.org/uploads/medias/2023/02/0223/brochure-requisition-63f78d4c7a300952406772.pdf

.. figure:: images/page_une_ici_grenoble_2023_05_07.png
   :align: center

   https://www.ici-grenoble.org/article/assemblee-pour-les-requisitions



Il y a bientôt un an, le conseil municipal de Grenoble votait l'expérimentation
par le maire de la réquisition de logements vacants.
Depuis, rien ne s'est passé, malgré une douzaine d'écoles occupées pour
mettre des familles à l'abri en urgence, et de plus en plus d'appels au 115.

Ce dimanche 7 mai à 16h à l'École Jean-Macé occupée (18 rue Henri Tarze),
l'association Droit Au Logement Isère organise une assemblée ouverte,
rassemblant les familles mal-logéEs, leurs soutiens et les organisations
souhaitant faire appliquer la loi de réquisition des logements vides.

Précisons que sur les 197 000 logements privés de l'agglomération grenobloise,
près de 17 000 sont des logements vides : soit des logements dégradés
dont les propriétaires ne peuvent assurer les travaux de rénovation, soit
des logements spéculatifs acquis par des groupes privés et des banques
à l'affût d'opérations immobilières juteuses.

Sur le même territoire, des milliers de personnes  sont à la rue, des
dizaines de milliers dans des logements indignes.

Cette indécence vous donnerait envie de réquisitionner les logements vides ?
Oui, mais comment ?

Que dit la loi ? Pour tout comprendre, nous vous recommandons la récente
brochure du DAL Isère : `Les lois de réquisition des logements vides pour les nul-e-s <https://www.ici-grenoble.org/uploads/medias/2023/02/0223/brochure-requisition-63f78d4c7a300952406772.pdf>`_

À 16h

École Jean-Macé occupée, https://www.openstreetmap.org/#map=19/45.19993/5.71030
18 rue Henri Tarze
Grenoble


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
    src="https://www.openstreetmap.org/export/embed.html?bbox=5.7085320353508%2C45.19913450014413%2C5.712072551250459%2C45.20073342189054&amp;layer=mapnik"
    style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.19993/5.71030">Afficher une carte plus grande</a></small>



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_07.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 11h00 |car38| https://www.ici-grenoble.org/evenement/theatre-discussion-bleu-sur-la-formation-des-crs

  - https://www.ici-grenoble.org/article/15aine-anti-repression-le-programme

- 14h00 https://www.ici-grenoble.org/evenement/apres-midi-chill-avec-deviations-entre-personnes-deviantes-de-genre
- 14h00 |car38| https://www.ici-grenoble.org/evenement/jeu-de-role-qui-sont-les-acteurs-de-la-repression-sur-les-crs-la-bac-la-prefecture
- 15h45 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- |important| 16h00 https://www.ici-grenoble.org/evenement/assemblee-ouverte-pour-la-requisition-des-logements-vides-a-grenoble

  - https://www.ici-grenoble.org/article/assemblee-pour-les-requisitions

- |important| 18h00 |car38| https://www.ici-grenoble.org/evenement/atelier-peut-on-allier-la-telephonie-mobile-et-lactivisme
- 18h40 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-la-psychiatrie-alternative
- 20h10 🎥https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 22h00 https://www.ici-grenoble.org/evenement/coup-de-coeur-quest-ce-qui-pourrait-sauver-lamour-nouveau-podcast-dovidie


Demosphere 38
=================

.. figure:: images/demo_2023_05_07.png
   :align: center

   https://38.demosphere.net/

- |important| 11h00 |car38| https://38.demosphere.net/rv/1104 (Pièce de théatre - "Bleu")
- 14h00 |car38| https://38.demosphere.net/rv/1105 (Jeu : Les acteurs de la répression)
- |important| |important| 18h00 |car38| https://38.demosphere.net/rv/1106 (TELEPHONIE MOBILE ET ACTIVISME peut-on mélanger les deux ? )

Événements de XR Grenoble pour le mois de mai 2023
=======================================================

- https://mobilizon.chapril.org/@xr_grenoble@mobilizon.extinctionrebellion.fr/events?showPassedEvents=false

.. figure:: ../images/extinction_rebellion_2023_05.png
   :align: center

   https://mobilizon.chapril.org/@xr_grenoble@mobilizon.extinctionrebellion.fr/events?showPassedEvents=false


|casserolade| zbeul
===========================

- https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations
- https://100joursdezbeul.fr/
- https://video.blast-info.fr/w/433d333c-64d1-41cc-ba26-ea4ed2027050 (INTERVILLES DU « ZBEUL » : LA FRONDE S’ORGANISE FACE À MACRON)
- https://piaille.fr/@cedricr
- https://piaille.fr/@cedricr.rss
- :ref:`zbeul_2023_05_06`
- :ref:`100jours_zbeul`


.. figure:: images/zbeul_cedric_rossi.png
   :align: center

   https://piaille.fr/@cedricr/110323015118265864


Depuis quelques jours, un site s’efforce de mettre un peu d’ordre dans
le zbeul français: 100joursdezbeul.fr

Quels sont les meilleurs départements? Quels sont leurs secrets?

Y-a-t-il de la triche dans le classement?

Quels sont les créatrices et créateurs de cette opération de haute subversion?
Sont-ils financés par Poutine, la CIA, Attac ou la branche informatique
de Solidaires?

L’humour, l’arme ultime du militantisme? A l’heure de la messe, ce dimanche:
on a fait la lumière. A vos casseroles!


.. _compte_ag_rendu_requisition_2023_05_07:

2023-05-07 Compte rendu de la 1ere assemblée ouverte pour la réquisition
=============================================================================

- https://ul38.cnt-f.org/2023/05/10/requisition-des-logements-vides-17-21-mai-et-5-juin/

::

    Objet:   [Logement-infos] CR de la 1ere assemblée ouverte pour la réquisition
    De:      Dal38@droitaulogement.org
    Date:    Lun 8 mai 2023 8:27
    À:       dal38@listes.gresille.org
    Copie à: logement-diffusion@rezo.net
    --------------------------------------------------------------------------

A l'appel du DAL 38, une cinquantaine de personnes, mal-logéEs ou
non, représentants plusieurs organisations (associations, collectifs
d'écoles occupées et syndicats) ont participé à la première assemblée
pour la réquisition des logements vides par le maire de Grenoble.

L'objectif est d'obtenir l'application de la délibération municipale
du 25 juin 2022, permettant au maire de Grenoble de réquisitionner des
logements vacants pour reloger des familles mal logées :  vivant à la
rue ou dans des taudis, ou qui ne peuvent plus payer leurs loyers ou
leurs charges et sont donc menacées d'expulsion. Et Grenoble compte
environ 9.000 logements vides selon l'INSEE, dont 1/3 depuis plus de
deux ans !

Notez les prochaines dates de mobilisation :

- Mercredi 17 mai à 14h devant le Conseil Départemental au 32 rue de
  New York pour une conférence de presse organisée par la coordination
  des écoles occupées de Grenoble

- Dimanche 21 mai à 16h à l'école Jean Macé occupée, prochaine
  assemblée ouverte suivi d'un grand collage dans tout Grenoble

- Lundi 5 juin à 18h sur l'esplanade de la Caserne de Bonne, pour
  célébrer les 1 an du campement qui a duré 6 semaines, et lancer une
  nouvelle série d'actions jusqu'à l'application de la loi de
  réquisition et le relogement des familles sans abris occupant les
  écoles, pour commencer !
