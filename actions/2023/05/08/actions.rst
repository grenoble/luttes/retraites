
.. _actions_2023_05_08:

=========================================================================================================================================
**Actions du lundi 8 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #8mai1945**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_01.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 15h00 https://www.ici-grenoble.org/evenement/balade-botanique-dans-le-trieves-decouvrir-les-plantes-sauvages-et-leurs-usages
- |important| 15h00 https://www.ici-grenoble.org/evenement/rassemblement-pour-la-reconnaissance-par-l-etat-francais-des-massacres-de-setif-guelma-et-kherrata-le-8-mai-1945
- 18h00 https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-fanfare-chorale-de-lutte-de-grenoble
- 22h00 https://www.ici-grenoble.org/evenement/coup-de-coeur-les-coulisses-de-la-revue-la-hulotte


Demosphere 38
=================

.. figure: images/demo_2023_05_08.png
   :align: center

   https://38.demosphere.net/


- |important| https://38.demosphere.net/rv/1164 (Commémoration de l'héritage social de la Résistance à Lyon / E. Macron présent pour le 8 mai 1945 )
- https://38.demosphere.net/rv/1165  (Rassemblement en mémoire Sétif 1945)
- |important| https://38.demosphere.net/rv/1166 (Formation antirepression : droits et stratégies en manif et garde à vue)
