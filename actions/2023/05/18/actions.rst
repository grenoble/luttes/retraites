
.. _actions_2023_05_18:

=========================================================================================================================================
**Actions du jeudi 18 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AssembleeGenerale


|ici_grenoble| A la une **Les 10 infos de mai**
=====================================================================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mai
- https://www.ici-grenoble.org/newsletter


.. figure:: ../10/images/page_une_ici_grenoble_2023_05_10.png
   :align: center

   https://www.ici-grenoble.org/article/les-10-infos-de-mai, https://www.ici-grenoble.org/newsletter

Vous ne lisez pas le Dauphiné Libéré, mais les infos marquantes vous
intéressent ?

Chaque mois, le média ici Grenoble vous propose la ''Grevue de presse'',
une sélection d'infos courtes et percutantes glanées dans la presse
locale ou dans nos réseaux.

Au menu de mai : une mégabassine dans le Trièves, des mégapompeurs dans le
Grésivaudan, un drone russe "made in Isère", la clinique mutualiste dans
une pyramide de Ponzi, la fin de la BAF, le début du BOCAL et plein
d'autres infos.

Bonne lecture !


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_18.png
   :align: center

   https://www.ici-grenoble.org/agenda


- 14h30 https://www.ici-grenoble.org/evenement/permanence-dautodefense-pour-lacces-aux-droits-caf-secu-titres-de-sejour-dossiers-a-remplir
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- 18h00 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- |important| 18h30 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 20h45 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative



Demosphere 38
=================

.. figure:: images/demo_2023_05_18.png
   :align: center

   https://38.demosphere.net/

Assemblée générale de lutte en Nord Isère
---------------------------------------------

- 18h00 https://38.demosphere.net/rv/1182 (Assemblée générale de lutte en Nord Isère , 362 chemin de Loudon, 38300 Ruy)

  .. figure:: images/ag_nord_isere.png
     :align: center

  .. figure:: images/affiche_nord_isere.png
     :align: center

Assemblée Générale de lutte - 18h30 au centre social Tchoukar (38 rue d'Alembert - Grenoble)
------------------------------------------------------------------------------------------------

- https://38.demosphere.net/rv/1184 (AG de lutte contre la réforme des retraites)

  Assemblée Générale de lutte - 18h30 au centre social Tchoukar (38 rue d'Alembert - Grenoble)
  Assemblée ouverte à tout.e.s celles et ceux qui ne se résignent pas à
  cesser le combat contre la réforme des retraites (et son im-monde).

  Au programme: discussions, organisation, actions!
