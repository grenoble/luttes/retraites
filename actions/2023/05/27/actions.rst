
.. _actions_2023_05_27:

=========================================================================================================================================
**Actions du samedi 27 mai 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_05_27.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/annule-festival-ecofeministe-bien-lbourgeon (ANNULE)
- 09h30 https://www.ici-grenoble.org/evenement/rassemblement-contre-la-caravane-du-snu-service-national-universel
- 15h00 https://www.ici-grenoble.org/evenement/conference-les-resistances-musicales-face-a-lesclavage-et-la-traite-negriere
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cine-rencontres-la-part-des-autres-sur-la-securite-sociale-de-lalimentation
- 19h00 https://www.ici-grenoble.org/evenement/resto-malap-repas-congolais-vegan-cuisine-par-des-personnes-sans-papiers

Demosphere 38
=================

.. figure: images/demo_2023_05_27.png
   :align: center

   https://38.demosphere.net/

- https://38.demosphere.net/rv/1190, Rassemblement contre la caravane du SNU, La Bobine

SNU
======

- https://www.ici-grenoble.org/evenement/rassemblement-contre-la-caravane-du-snu-service-national-universel

Voici l'appel à rassemblement contre le Service National Universel, que
le gouvernement projette de rendre obligatoire et en temps scolaire :

"La caravane du Service Nationale Universel (SNU) débarque à Grenoble
le samedi 27 mai avec ses chemises blanches et casquettes bleu pour une
belle propagande de ce service militaire revisité.

Est-ce que nos jeunes ont vraiment besoin d'un bourrage de crâne de 2
semaines encadré par l'armée à base de levé de drapeau, marseillaise,
discipline et tout le toin toin ?

Pour une jeunesse engagée et militante, pas une jeunesse engagée dans
l'armée ! Contre la mise au pas de la jeunes ! Contre la militarisation
de la société ! Contre le nationalisme !"

RDV le samedi 27 mai au Village du SNU pour un accueil très festif !

Pour plus d'infos sur le SNU : https://www.politis.fr/articles/2023/02/info-politis-snu-service-national-universel-comment-macron-veut-mettre-la-jeunesse-au-pas/

À 9h30

Devant la Bobine
Parc Mistral


Slogans
----------

Après brainstorming, quelques propositions de slogans contre le SNU:

- Sévice National Universel: Merci ReNaissance
- Être jeune en Macronie: planète bousillée, école sabotée, droits
  sociaux amputés, libertés rognées. Merci ReNaissance
- + 2 ans de turbin pour les daron.e.s / Sévice national pour les fiston.e.s
- Jeunesse militarisée, révolte réprimée
- Balance ton gradé / Balance ton officier / Balance ton militaire
- Macron ferme des écoles pour ouvrir des casernes
- Du pognon pour la santé et l'éducation, pas pour les troufions!
- Témoignages d'ex volontaires SNU: _« Traumatisant », « terrifiant »,
  « agressif », « très méprisant » (Source Politis)
- "harcèlement sexuel, agression, racisme" = valeurs républicaines?

Compte rendu de la manifestation antifasciste à Annecy
=========================================================

- https://librinfo74.fr/une-manifestation-antifasciste-a-annecy-perturbee-par-lextreme-droite/
