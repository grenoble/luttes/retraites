.. index::
   pair: Manifestation; mardi 6 juin 2023

.. _actions_2023_06_06:

================================================================================================================================================================================
**Actions du mardi 6 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #ManifestationContreLaRetraiteA64ans #SolidariteSoulevementsDeLaTerre**
================================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-06 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #ManifestationContreLaRetraiteA64ans #SolidariteSoulevementsDeLaTerre

|ici_grenoble| A la une **Souffler sur les braises**
==================================================================================

- https://www.ici-grenoble.org/article/souffler-sur-les-braises

.. figure:: images/page_une_ici_grenoble_2023_06_06.png
   :align: center

   https://www.ici-grenoble.org/article/souffler-sur-les-braises, https://www.ici-grenoble.org/newsletter

Malgré la répression et les multiples contre-feux médiatiques, la lutte
contre la réforme des retraites continue : ce mardi 6 juin 2023 à 10h au
départ de la gare de Grenoble, l'intersyndicale appelle à une déferlante
populaire pour marquer notre détermination et notre ténacité.

|communisme_libertaire| **Un cortège révolutionnaire s'organise au sein de la manifestation**.

**Dans ce contexte politique si sombre, face à un capitalisme de plus en
plus autoritaire, mensonger et répressif, face à l'extrême-droite en embuscade,
merci à celles et ceux qui résistent, qui manifestent, qui s'organisent,
qui bloquent, qui prennent des risques**.

Rappelons qu'au plus fort de la lutte, les manifestations ont rassemblé
plus de 50 000 personnes à Grenoble selon les syndicats (plus de 30 000
selon la police).

Les luttes se sont également durcies après le 49.3 : énormes manifs sauvages,
jet de cocktails molotov sur la Préfecture de l'Isère, barrages filtrants
aux portes de Grenoble, opérations péage gratuit à Voreppe, sabotages...

Nous étions manifestement dans une situation pré-révolutionnaire.

Face à l'ampleur de la colère contre une réforme des retraites injuste,
les luttes vont-elles repartir en juin, et la répression policière aussi ?

Combien de temps les CRS vont-ils soutenir ce gouvernement impopulaire
au sein de leurs propres familles ?



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_06.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 05h00 https://www.ici-grenoble.org/evenement/le-6-juin-2013-edward-snowden-revele-la-surveillance-de-masse-des-communications-privees-telephone-et-internet
- |important| 08h00 https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste
- |communisme_libertaire| 10h00 https://www.ici-grenoble.org/evenement/cortege-revolutionnaire-dans-la-manif
- |important| 10h00 https://www.ici-grenoble.org/evenement/manifestation-unitaire-pour-le-retrait-de-la-reforme-des-retraites
- 12h00 https://www.ici-grenoble.org/evenement/permanence-du-rusf-isere-aide-a-la-reprise-detude-soutien-administratif
- 12h00 https://www.ici-grenoble.org/evenement/repas-sur-place-ou-a-emporter-proposes-par-cuisine-sans-frontieres
- 16h10 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- |important| 18h30 https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste
- 18h30 ♀️✊🏽 https://www.ici-grenoble.org/evenement/cercle-de-lectures-feministes-de-grenoble
- 20h15 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h30 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques


Demosphere 38
=================

.. figure:: images/demo_2023_06_06.png
   :align: center

   https://38.demosphere.net/

- 07h30 https://38.demosphere.net/rv/1208, Piquet de grève et petit déjeuner revendicatif, Ocellia (IFTS), 3 ave Victor Hugo
- 08h30 https://38.demosphere.net/rv/1203, La réforme des retraites c'est NON !  Trièves
- 09h30 https://38.demosphere.net/rv/1197, RENDEZ-VOUS ARRÊT TRAM MALHERBE POUR REJOINDRE LA MANIF, tram A Malherbe
- 10h00 https://38.demosphere.net/rv/1198, GRÈVE ET MANIFESTATION nationale contre la réforme des retraites, Alsace lorraine - Jaures
- 10h00 https://38.demosphere.net/rv/1204, Face à un pouvoir brutal, radicalisons la lutte !  Alsace-Lorraine / jean-Jaurès
- 14h00 https://38.demosphere.net/rv/1202; champ de Mars (Gare routière) Vienne


|communisme_libertaire| Tracts CNT38 + intersyndicale
==========================================================

- https://ul38.cnt-f.org/2023/06/05/la-lutte-continue/
- https://ul38.cnt-f.org/2023/06/05/mardi-6-juin-manifestation-contre-la-reforme-des-retraites-et-leur-monde/
- https://ul38.cnt-f.org/wp-content/uploads/tract-intersyndical-Isere-6-juin-2023.pdf
- https://ul38.cnt-f.org/wp-content/uploads/20230406-Retraites-coul-2-1.pdf

La CNT lutte pour préserver nos droits, chacun durement acquis au cours
des luttes sociales antérieures, car rien n’a jamais été ”offert” par
la classe dirigeante, ni patronat, ni gouvernement.

La CNT lutte aussi pour conquérir de nouveaux droits, pour améliorer nos
quotidiens, particulièrement en ces temps de régression sociale généralisée
et d’ultra-libéralisme.
Et la CNT lutte également contre le capitalisme et pour construire un
monde meilleur, un autre futur.

C’est dans cette logique que les syndicats de la CNT grenobloise ont
décidé de participer au “cortège révolutionnaire” durant cette manifestation
du 6 juin contre la réforme des retraites.

Ce cortège, rassemblement de différents cortèges d’organisations où la
CNT aura toute sa place, s’est retrouvé autour du texte que nous
retranscrivons ci-dessous.

Vous pouvez retrouver `ici <https://ul38.cnt-f.org/wp-content/uploads/20230406-Retraites-coul-2-1.pdf>`_
le tract CNT que nous distribuerons demain à  propos de la réforme des retraites,
ainsi que le texte de `l’Intersyndicale Isère pour le mardi 6 juin 2023 <https://ul38.cnt-f.org/wp-content/uploads/tract-intersyndical-Isere-6-juin-2023.pdf>`_


Face à un pouvoir brutal, radicalisons la lutte !
------------------------------------------------------

Après des mois de mobilisation contre la réforme des retraites et son monde,
un rejet massif dans la population et les manifestations les plus denses
depuis des décennies, force est de constater que le pouvoir ne plie toujours pas.

C’est ainsi que l’impuissance tant du dialogue social que du parlementarisme
pour défendre nos droits les plus élémentaires devient patent.

**Le capitalisme nous dirige droit vers le désastre écologique, et dans
cette fuite en avant, l’antagonisme de classe est impossible à dissimuler**.

Alors, parce que nous n’obtiendrons jamais rien sans assumer un rapport
de force direct, et parce qu’il ne peut y avoir de victoire durable pour
notre camp social sans révolution politique et sociale,

Le mardi 6 Juin 2023, rendez-vous à 10h, avenue Alsace-Lorraine à Grenoble,
rejoignez-nous derrière la banderole rouge et noire !

Un cortège révolutionnaire ?

Nous ne croyons ni aux voies de la politique institutionnelle ni au dialogue
social qui ont démontré leur impuissance au cours de ce mouvement social.

Nous croyons bien plus à la lutte des classes et au rapport de force direct
avec le pouvoir, par la grève générale et l’organisation collective.

L’offensive antisociale et sécuritaire en cours n’est pas le fait d’une
poignée de gouvernant·es mais la conséquence du modèle de société capitaliste,
c’est pourquoi seule une révolution politique et sociale peut constituer
une véritable victoire pour notre camp.

**C’est pour faire exister cet horizon révolutionnaire que le cortège existe**.

Et en pratique ?

Ce cortège prône le respect de la diversité des tactiques et la solidarité
face à toute répression. Notre objectif est de construire un cortège ouvert,
où toutes et tous peuvent revendiquer à leur manière des idées et des
modes d’action révolutionnaires sans crainte de se retrouver isolé·es.

Ainsi, la stigmatisation d’actions camarades en notre sein n’est pas la
bienvenue, et le cortège ne se laissera diviser ni politiquement ni
matériellement.

Pour la sécurité des participant·es, une équipe de coordination est
organisée afin de faciliter les mouvements du cortège et lui permettre
de réagir collectivement si cela s’avère nécessaire.

Au plaisir de se retrouver ensemble dans la lutte !

14h00 Manifestation à Annecy
===============================

- https://librinfo74.fr/ce-mardi-6-juin-mobilisation-contre-la-reforme-des-retraites/


|sdterre| **Appel à soutien suite à une vague d'arrestations en france en lien avec le désarmement d'une usine lafarge**
============================================================================================================================

- https://lessoulevementsdelaterre.org/blog/appel-a-soutien-suite-a-une-vague-d-arrestations-en-france-en-lien-avec-le-desarmement-d-une-usine-lafarge
- https://lessoulevementsdelaterre.org/blog/nous-sommes-les-soulevements-de-la-terre
- https://www.instagram.com/soulevements.de.la.terre/
- Les soulèvements de la terre <contact@lessoulevementsdelaterre.org>
- https://www.mediapart.fr/journal/ecologie/050623/vague-d-interpellations-coup-de-poing-dans-la-mouvance-ecologiste
- https://reporterre.net/En-France-une-vague-d-arrestations-contre-le-mouvement-ecologiste-radical


.. figure::  images/sdt.png
   :align: center


Nous vous contactons suite à une vague d'une quinzaine d'arrestations et
de perquisitions survenue ce matin (lundi 05 juin 2023) dans plus de 10 communes,
partout en France.

D'après nos informations, il s'agirait d'une opération policière en lien
avec l'action de désarmement de l'usine Lafarge-Holcim de Bouc Bel Air
(13) survenue le 10 décembre 2022.

Nous venons de publier un communiqué en réaction à cette opération policière
où nous dénonçons la criminalisation des militant-es écologistes et affirmons
notre solidarité avec toutes celles et ceux qui dénoncent, occupent, bloquent,
désarment l'industrie du béton en général et Lafarge-Holcim en particulier.

S'opposer à cette industrie mortifère est un geste courageux et d'utilité
publique face à la catastrophe écologique qui fait rage.

Pour toutes ces raisons, nous sommes pleinement solidaires de cette action
et des personnes arrêtées ce matin.

Nous leur apporterons notre soutien dans les semaines et mois à venir - que
cela soit sur les plans juridique, médiatique ou politique.

En tant que signataire de la tribune de janvier contre la criminalisation
du mouvement et /où de l'appel "Nous sommes les Soulèvements de la Terre"
qui compte aujourd'hui plus de 100 000 membres déclarés, des dizaines
d'organisations nationales et collectifs locaux, nous voulions vous tenir
informé-e de ce nouvel épisode répressif contre le mouvement écologiste,
qui survient après Sainte-Soline et les menaces de dissolution des Soulèvements.

Comment agir ?
-------------------

**Nous appelons à des rassemblements devant les préfectures et les sites
Lafarge-Holcim à proximité de chez eux mercredi 7 juin 2023 (plus d'infos
à venir sur notre site (https://lessoulevementsdelaterre.org)**, pour
exiger la libération des personnes gardées à vue.

**En tant que signataire de l'appel, nous vous encourageons fortement à
vous joindre à ces rassemblements et faire fleurir des messages de soutien
lors de la manif retraites du 6 juin 2023**, afin de témoigner de notre
solidarité collective, entretenir le rapport de force avec ces industries
mises à mal par la catastrophe écologique dont elles sont responsables
et l'État qui les protègent et d'affirmer que nous ne plierons jamais
face à la répression.

Enfin, nous vous invitons à vous rendre massivement aux deux prochaines
mobilisations des Soulèvements de la Terre.

Notamment, les 10 et 11 juin 2023 à la mobilisation "Fin de carrières 44"
en Loire-Atlantique contre l'extension des carrières de sable de Lafarge-Holcim
et GSM qui menacent les terres agricoles et la biodiversité localement.

Mais aussi en Maurienne, les 17 et 18 juin 2023 à la mobilisation contre
le Lyon-Turin : un projet inutile et pharaonique, qui ne pourrait voir
le jour sans l'industrie du béton.

Cet été d'autres actions contre les mégabassines sont d'ores et déjà
prévues, nous vous invitons à vous rendre sur notre site (100 jours pour
les sécher et le convoi de l'eau) ou sur le canal Télégram pour en savoir plus.

**Les actions de désarmement de l'industrie du béton se répandent et ce n’est
probablement qu’un début. Nous appelons à les amplifier**.

**Nous exigeons la libération immédiate de toutes les personnes arrêtées ce jour**.

**Nous appelons à des gestes de solidarité avec elles dans toutes les manifs
retraite de demain**.

**Nous appelons à des rassemblements de soutien partout en France mercredi
soir (07 juin), face à des préfectures, ou des sites Lafarge-Holcim.**

**Nous appelons à converger massivement ce dimanche en Loire-Atlantique
pour les convois Fin de carrières, et notamment contre l’extension
d’un site Lafarge à Saint Colomban**

**Ne nous laissons pas intimider par la répression, désarmons le béton !**



Article mediapart
-------------------

- :download:`Article mediapart <pdfs/Mediapart.pdf>`


