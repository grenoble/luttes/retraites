.. index::
   pair: DAL ; 2023-06-04
   pair: Effondrement ; 2023-06-04


.. _actions_2023_06_04:

====================================================================================================================================================================
**Actions du dimanche 4 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AutoDefense #Effondrement #RequisitionDesLogementsVides**
====================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-04 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #AutoDefense #Effondrement ##RequisitionDesLogementsVides


.. _dal_2023_06_04:

|ici_grenoble| A la une **17 000 logements vides !**
==================================================================================

- https://www.ici-grenoble.org/article/17-000-logements-vides
- http://www.ici-grenoble.org/infospratiques/fiche.php?id=4010
- https://www.ici-grenoble.org/uploads/medias/2023/02/0223/brochure-requisition-63f78d4c7a300952406772.pdf

.. figure:: images/page_une_ici_grenoble_2023_06_04.png
   :align: center

   https://www.ici-grenoble.org/article/17-000-logements-vides, https://www.ici-grenoble.org/newsletter


Il y a un an, le conseil municipal de Grenoble votait l'expérimentation
par le maire de la réquisition de logements vacants.

Depuis, rien ne s'est passé, malgré une douzaine d'écoles occupées pour
mettre des familles à l'abri, et de plus en plus d'appels au 115.

Ce lundi 5 juin 2023 à 16h sur l'esplanade Alain Le Rey (Caserne de Bonne),
l'association `Droit Au Logement Isère <http://www.ici-grenoble.org/infospratiques/fiche.php?id=4010>`_
organise un rassemblement avec les familles mal-logéEs, leurs soutiens
et les organisations souhaitant faire appliquer la loi de réquisition des logements vides.

Précisons que sur les 197 000 logements privés de l'agglomération grenobloise,
près de 17 000 sont des logements vides : soit des logements dégradés dont
les propriétaires ne peuvent assurer les travaux de rénovation, soit des
logements spéculatifs acquis par des groupes privés et des banques à
l'affût d'opérations immobilières juteuses.

Sur le même territoire, des `milliers de personnes sont à la rue <https://www.ici-grenoble.org/question-answer/combien-de-personnes-sont-sans-domicile-fixe-en-isere>`_,
des dizaines de milliers dans des logements indignes.

Cette indécence vous donnerait envie de réquisitionner les logements vides ?

Oui, mais comment ? Que dit la loi ? Pour tout comprendre, nous vous
recommandons la récente brochure du DAL Isère : Les lois de réquisition
des logements vides pour les nul-e-s.

À 18h

Sur l'esplanade Alain Le Rey
Caserne de Bonne
Grenoble


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_04.png
   :align: center

   https://www.ici-grenoble.org/agenda


- |important| 09h30 https://www.ici-grenoble.org/evenement/stage-dautodefense-pour-femmes-cis-et-trans-personnes-non-binaires-et-intersexes
- 14h00 https://www.ici-grenoble.org/evenement/balade-naturaliste-a-prix-libre-pres-de-grenoble
- 17h00 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 18h25 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- |important| 21h00 🎥 https://www.ici-grenoble.org/evenement/serie-leffondrement-la-plus-realiste-des-series-post-apocalyptiques


Demosphere 38
=================

.. figure:: images/demo_2023_06_04.png
   :align: center

   https://38.demosphere.net/


- 14h00 https://38.demosphere.net/rv/1207 (DEVANT LA BIBLIOTHÈQUE MUNICIPALE À CHAVANT)

.. _effondrement_2023_06_04:

Série (novembre 2019) : L'effondrement (la plus réaliste des séries post-apocalyptiques ?)
================================================================================================

- https://www.youtube.com/watch?v=8EowLLRkDoY (Projection de L'Effondrement !, Les Parasites)
- https://www.ici-grenoble.org/evenement/serie-leffondrement-la-plus-realiste-des-series-post-apocalyptiques
- https://www.youtube.com/@LesParasitesFilms/videos
- http://www.ici-grenoble.org/infos/la-une.php?id=11384
- https://www.instagram.com/lesparasites/
- https://www.instagram.com/p/CHjFcuBJ99v/


.. figure:: images/leffondrement.png
   :align: center

   https://www.ici-grenoble.org/evenement/serie-leffondrement-la-plus-realiste-des-series-post-apocalyptiques


Le média ici Grenoble vous propose ses "coups de coeur" : des podcasts,
des émissions ou des films qui nous ont particulièrement marqués.

Aujourd'hui nous vous proposons une série marquante sur le `désastre
climatique en cours <http://www.ici-grenoble.org/infos/la-une.php?id=11384>`_ :  la série L'effondrement (2019).

Pourquoi ce choix ? Parce qu'il s'agit à notre connaissance de la plus
réaliste des séries post-apocalyptiques.
Une série qui nous plonge dans les conséquences matérielles mais aussi
psychologiques d'un monde qui s'effondre.

Chaque épisode est un plan séquence de 15 à 25 minutes, ce qui permet de
ressentir la détresse et l'urgence des situations vécues.

Attention, ces courts-métrages sont passionnants mais atroces de par
leur réalisme. Risque de cauchemars...

Par ailleurs, cette série peut choquer en terme de sexisme, d'idéologie,
de rapport à la politique, de vision de l'humanité...

Bref, une série à débattre.

**Et vous, en cas de grande crise systémique, comment imaginez-vous l'évolution de la société ?**

Les 8 épisodes sont disponibles sur Youtube:

1. `Le Supermarché <https://www.youtube.com/watch?v=vLa-HhBpgc4>`_ (https://www.youtube.com/watch?v=vLa-HhBpgc4)
2. `La Station Service <https://www.youtube.com/watch?v=o8JrVlD_Nzo>`_ (https://www.youtube.com/watch?v=o8JrVlD_Nzo)
3. `L'aérodrome <https://www.youtube.com/watch?v=riT6DFedz0Y>`_ (https://www.youtube.com/watch?v=riT6DFedz0Y)
4. `Le Hameau <https://www.youtube.com/watch?v=7fFKKy4n_1A>`_ (https://www.youtube.com/watch?v=7fFKKy4n_1A)
5. `La Centrale <https://www.youtube.com/watch?v=HiZW1uAi2fo>`_ (https://www.youtube.com/watch?v=HiZW1uAi2fo)
6. `La Maison de retraite <https://www.youtube.com/watch?v=lZB_r4v50CM>`_ (https://www.youtube.com/watch?v=lZB_r4v50CM)
7. `L'île <https://www.youtube.com/watch?v=q1cIf4LBQHg>`_ (https://www.youtube.com/watch?v=q1cIf4LBQHg)
8. `L'émission <https://www.youtube.com/watch?v=g9KfNKS9FNQ>`_ (https://www.youtube.com/watch?v=g9KfNKS9FNQ)
