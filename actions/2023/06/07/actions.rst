
.. _actions_2023_06_07:

=========================================================================================================================================
**Actions du mercredi 7 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-07  #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie


|ici_grenoble| A la une **Et maintenant, on fait quoi ?**
==================================================================================

- https://www.ici-grenoble.org/article/et-maintenant-on-fait-quoi
- https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner

.. figure:: images/page_une_ici_grenoble_2023_06_07.png
   :align: center

   https://www.ici-grenoble.org/article/et-maintenant-on-fait-quoi, https://www.ici-grenoble.org/newsletter

Malgré la répression et les contre-feux médiatiques, la lutte contre la
réforme des retraites continue : 5 000 à 10 000 personnes ont manifesté
ce mardi 6 juin 2023 à Grenoble.

Et maintenant, que faire ?

Ce mercredi 7 juin à 18h30 au 38 rue d'Alembert, c'est l'Assemblée Générale
de lutte contre la réforme des retraites. Venez réfléchir et organiser la
suite du mouvement.

Pour alimenter le débat, nous vous recommandons une réflexion critique
sur les stratégies de l'intersyndicale par Frédéric Lordon : `Vouloir perdre, vouloir gagner <https://blog.mondediplo.net/vouloir-perdre-vouloir-gagner>`_.



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_07.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h58 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h00 https://www.ici-grenoble.org/evenement/cafe-chomeur-euse-s-echange-dinfos-et-dexperiences-hors-du-monde-du-travail
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 16h20 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- |important| 18h30 https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cinema-marinette-sur-la-footballeuse-lesbienne-marinette-pichon-une-heroine-meconnue
- 19h15 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques

Demosphere 38
=================

.. figure:: images/demo_2023_06_07.png
   :align: center

   https://38.demosphere.net/

- https://38.demosphere.net/rv/1205
- 18h30 https://38.demosphere.net/rv/1201, AG de luttes
