.. index::
   pair: Logiciel Libre ; 2023-06-01

.. _actions_2023_06_01:

================================================================================================================================================================
**Actions du jeudi 1er juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Velorution #LogicielLibre #internet #surveillance**
================================================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-01 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Velorution #LogicielLibre #internet #surveillance


|ici_grenoble| A la une **Résignation ? Vélorution !**
=====================================================================


.. figure:: images/page_une_ici_grenoble_2023_06_01.png
   :align: center

   https://www.ici-grenoble.org/article/resignation-velorution, https://www.ici-grenoble.org/newsletter


Ce jeudi 1er juin 2023 à 17h au départ du Parvis de la Belle Electrique,
rejoignons la vélorution de lutte contre la réforme des retraites.

Voici l'invitation :

"Ensemble rendons visible la mobilisation contre la réforme des retraites
à travers les quartiers de Grenoble, et appelons à converger en manifestation
le mardi 6 juin !

Venez déambuler dans Grenoble et ses quartiers pour informer, afficher,
differ et appeler à la manif intersyndicale.

Viens avec ton vélo (ou autre truc qui roule sans essence), tes dispositifs
sonores portatifs, ta colle et ta brosse... et ta banane."

À 17h

Au départ du Parvis de la Belle Electrique
Grenoble


**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_01.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 10h00 https://www.ici-grenoble.org/evenement/permanence-de-l-accorderie-de-grenoble
- 14h00 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h00 https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement
- |important| 17h00 https://www.ici-grenoble.org/evenement/velorution-de-lutte-contre-la-reforme-des-retraites
- 18h00 https://www.ici-grenoble.org/evenement/atelier-de-francais-a-prix-libre-pour-debutant-es-et-intermediaires
- 18h30 |alternatiba| https://www.ici-grenoble.org/evenement/aperotiba-soiree-de-presentation-dalternatiba-grenoble
- 18h45 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h35 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques

Demosphere 38
=================

.. figure:: images/demo_2023_06_01.png
   :align: center

   https://38.demosphere.net/


- https://38.demosphere.net/rv/1194 17h00 Vélorution de lutte contre la réforme des retraites (et son im-monde), Parvis de la Belle Électrique - Grenoble

2023-06-01 19h00 🎥 LOL Logiciel Libre – une affaire sérieuse #logiciel #informatique #numérique #grenoble #internet #surveillance #cinéma #documentaire
==============================================================================================================================================================

- https://metro-grenoble.eelv.fr/lol-logiciel-libre-une-affaire-serieuse/
- http://www.gigowattfilm.com/coffre/fiction/doc-lol
- https://www.imagotv.fr/documentaires/lol-logiciel-libre-une-affaire-serieuse
- https://www.ldh-france.org/wp-content/uploads/2019/09/Dossier-de-presse-LoL_LDH.pdf
- https://www.librealire.org/lol-logiciel-libre-une-affaire-serieuse
- https://imaginair.es/@thierrybayoud/110310603337202496
- https://imaginair.es/@thierrybayoud/110360240937216424

Le 1er juin 2023 à Grenoble sera projeté notre documentaire intitulé
**LoL - Logiciel Libre, une affaire sérieuse**.

.. figure:: images/lol.jpg
   :align: center

La séance est prévue à 19h à la `Maison des écologistes 8 rue Marceline Desbordes-Valmore, Grenoble (https://www.openstreetmap.org/#map=18/45.17178/5.729663) <https://www.openstreetmap.org/#map=18/45.17178/5.729663>`_
et sera suivie d'un débat avec un représentant de l' @aprilorg , Jean-Christophe Becquet,
et un autre de la ville d'Échirolles, @nicolasvivant

L'entrée est libre et gratuite.

Plus de détails en suivant ce lien internet : https://metro-grenoble.eelv.fr/lol-logiciel-libre-une-affaire-serieuse/

Un grand merci @Gallorum pour l'organisation de cet événement, lequel
permet à notre travail de continuer à vivre.

#logiciel #informatique #numérique #grenoble #internet #surveillance #cinéma #documentaire

À travers les entretiens de quatorze intervenants, « LoL » est un voyage en 8
étapes dans le cœur de nos habitudes numériques.
Formellement, le film entremêle leurs points de vue, sans ajout de voix off,
agrémentés d’illustrations parfois allégoriques nourries de films anciens.

Le logiciel est la base de l’informatique. Un téléphone portable, un
réfrigérateur ou une voiture sont devenus des objets largement informatisés.

Ces logiciels sont composés de code appelé code source.

Or ce code a fini par être caché par les fabricants, car source de profits.

À l’heure où les cinq plus grosses capitalisations boursières mondiales
sont Google, Apple, Facebook, Amazon et Microsoft, les populations sont-elles
au fait de l’impact que leur usage du numérique a sur leur vie ?

Un film de François Zaïdi, Thierry Bayoud et Léa Deneuville sorti en 2019.

Pour en savoir plus :

- le `film (visionnable en ligne) <https://www.imagotv.fr/documentaires/lol-logiciel-libre-une-affaire-serieuse>`_
- le `dossier de presse <https://www.ldh-france.org/wp-content/uploads/2019/09/Dossier-de-presse-LoL_LDH.pdf>`_,
- la `transcription par l’April <https://www.librealire.org/lol-logiciel-libre-une-affaire-serieuse>`_.

Débat
------

20 H Discussion – débat avec Jean-Christophe Becquet et Nicolas Vivant

Jean-Christophe Becquet est vice-président de `l’April association nationale
de promotion et de défense du logiciel libre <https://april.org/>`_.

Nicolas Vivant est directeur de la stratégie et de la culture numériques
de la ville d’Échirolles.
Il a documenté la libération numérique en cours d’Échirolles dans `une
série d’articles <https://framablog.org/2023/03/10/echirolles-liberee-la-degooglisation-1/>`_.

Vous pouvez arriver à 20 h, pour participer seulement au débat, si vous
préférez regarder le film chez vous.

Soirée ouverte à toutes et tous, organisée par Europe Écologie Les Verts
dans le cadre des `États généraux de l’écologie <https://lesecologistes.fr/replay-lancement-des-etats-generaux-de-lecologie/>`_.

