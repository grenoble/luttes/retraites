
.. _actions_2023_06_05:

=========================================================================================================================================
**Actions du lundi 5 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RequisitionDesLogementsVides**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-05 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #RequisitionDesLogementsVides



|ici_grenoble| A la une **17 000 logements vides !**
==================================================================================

- https://www.ici-grenoble.org/article/17-000-logements-vides
- http://www.ici-grenoble.org/infospratiques/fiche.php?id=4010
- https://www.ici-grenoble.org/uploads/medias/2023/02/0223/brochure-requisition-63f78d4c7a300952406772.pdf


.. figure:: ../04/images/page_une_ici_grenoble_2023_06_04.png
   :align: center

   https://www.ici-grenoble.org/article/17-000-logements-vides, https://www.ici-grenoble.org/newsletter


Il y a un an, le conseil municipal de Grenoble votait l'expérimentation
par le maire de la réquisition de logements vacants.

Depuis, rien ne s'est passé, malgré une douzaine d'écoles occupées pour
mettre des familles à l'abri, et de plus en plus d'appels au 115.

Ce lundi 5 juin 2023 à 16h sur l'esplanade Alain Le Rey (Caserne de Bonne),
l'association `Droit Au Logement Isère <http://www.ici-grenoble.org/infospratiques/fiche.php?id=4010>`_
organise un rassemblement avec les familles mal-logéEs, leurs soutiens
et les organisations souhaitant faire appliquer la loi de réquisition des logements vides.

Précisons que sur les 197 000 logements privés de l'agglomération grenobloise,
près de 17 000 sont des logements vides : soit des logements dégradés dont
les propriétaires ne peuvent assurer les travaux de rénovation, soit des
logements spéculatifs acquis par des groupes privés et des banques à
l'affût d'opérations immobilières juteuses.

Sur le même territoire, des `milliers de personnes sont à la rue <https://www.ici-grenoble.org/question-answer/combien-de-personnes-sont-sans-domicile-fixe-en-isere>`_,
des dizaines de milliers dans des logements indignes.

Cette indécence vous donnerait envie de réquisitionner les logements vides ?

Oui, mais comment ? Que dit la loi ? Pour tout comprendre, nous vous
recommandons la récente brochure du DAL Isère : Les lois de réquisition
des logements vides pour les nul-e-s.

À 18h

Sur l'esplanade Alain Le Rey
Caserne de Bonne
Grenoble

**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_05.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h00 https://www.ici-grenoble.org/evenement/permanence-du-garage-associatif-les-soupapes-reparer-sa-voiture
- |important| 18h00 https://www.ici-grenoble.org/evenement/rassemblement-pour-la-requisition-des-logements-vides
- 18h45 https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-autogeree-la-cagette
- 19h00 🎥 https://www.ici-grenoble.org/evenement/cine-rencontres-home-sweet-home-sur-une-revolte-dans-une-maison-de-retraite, https://www.bibliothequeantigone.org/?p=5549
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques
- 20h40 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative



2023-06-05 18h00 Rassemblement pour la réquisition 5 juin, date anniversaire de l'occupation de la Caserne de Bonne
=======================================================================================================================

Ce lundi 5 juin 2023, à 18h, aura lieu sur l'esplanade de la Caserne
de Bonne a Grenoble, le rassemblement pour la réquisition des
logements vacants à Grenoble, et pour le relogement des familles
occupant les écoles actuellement.

De nombreuses organisations appellent à ce rendez-vous important dans
la lutte sans relâche et historique menée par les familles sans-logis,
qui n'ont cessé de se mobiliser cette année, contre les expulsions,
le logement cher, les lois antisociales Kasbarian et Darmanin mais
aussi durant le mouvement des retraites.

Cet événement est symbolique à plusieurs titres :

- il s'agit de la datte anniversaire du début de l'occupation de
  l'esplanade de la Caserne de Bonne l'été dernier, qui a duré 6
  semaines et a abouti, faute de réponses de la préfecture de l'Isère, à
  l'occupation de l'école Jean Macé, puis dans la foulée de très
  nombreuses autres, soutenues par des enseignants et des parents
  d'élèves, mais aussi le réseau RESF, la FCPE et l'intersyndicale
  enfants migrants.
- Il s'agit aussi de la fin de l'ultimatum posé à la Ville de Grenoble
  et à la Métropole, à qui il a été demandé de trouver des solutions
  d'hébergement en attendant des logements pérennes. Il est difficile
  d'entendre que rien n'a été anticipé en amont de l'été, où les écoles
  vont fermer et promettant la mise à la rue de ces dernières familles.

Rendez-vous donc pour exiger des réquisitions, le respect des droits
fondamentaux pour toutes et tous, justice et dignité pour celles et
ceux qui se battent contre ce monde mortifère.

**Un toit, c'est un droit, partagez autour de vous !**
