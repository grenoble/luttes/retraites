.. index::
   pair: Transports; Convergence vélo (2023-06-03)
   pair: Logiciel Libre  ; 2023-06-03
   pair: OpenStreetMap ; 2023-06-03


.. _actions_2023_06_03:

=========================================================================================================================================
**Actions du samedi 3 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Velorution**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#Grenoble #64AnsCestToujoursNon #ReformedesRetraites  #SeuleLaLuttePaie


.. _soutien_yovan_et_mathieu_2023_06_03:

|ici_grenoble| |car38| A la une **Soutenir Yovan et Mathieu** |solidarite|
==================================================================================


.. figure:: ../02/images/page_une_ici_grenoble_2023_06_02.png
   :align: center

   https://www.ici-grenoble.org/article/soutenir-yovan-et-mathieu, https://www.ici-grenoble.org/newsletter


Lors de la manifestation du 1er mai 2023 à Grenoble, Yovan et Mathieu ont été
arrêtés et placés en détention provisoire à Varces.

Pourquoi ?

Que s'est-il passé pendant cette manifestation ?

**En quoi est-ce un tournant dans la répression à Grenoble ?**

Voici le :ref:`récit et les explications du Collectif Anti-Répression Isère <soutien_yovan_et_mathieu_2023_06_02>`



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_03.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 06h00 https://www.ici-grenoble.org/evenement/le-3-juin-1970-les-incroyables-emeutes-du-campus-de-saint-martin-d-heres
- |important| |alternatiba| 08h00 https://www.ici-grenoble.org/evenement/grande-convergence-velo-de-lagglomeration-grenobloise-velorution-geante
- 09h30 https://www.ici-grenoble.org/evenement/atelier-clown-e-s-en-mixite-choisie-1
- https://www.ici-grenoble.org/evenement/pop-up-store-lorella-bijoux (erreur 500)
- 10h00 https://www.ici-grenoble.org/evenement/tournoi-de-foot-en-mixite-choisie-sans-mec-cis (Au stade Espagnac 4 rue du repos Grenoble)
- 15h25 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 17h30 🎥 https://www.ici-grenoble.org/evenement/cine-discussion-lhistoire-des-gilets-jaunes-par-nous
- 17h00 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative



2023-06-03 Convergences vélo région grenobloise
=====================================================

- https://www.convergence-velo-grenoble.fr/
- https://www.convergence-velo-grenoble.fr/#convergence
- https://umap.openstreetmap.fr/fr/map/convergence-velo-grenoble-2023-samedi-3-juin_888835#11/45.1276/5.7355

.. figure:: images/convergence_velo.png
   :align: center

   https://www.ici-grenoble.org/evenement/grande-convergence-velo-de-lagglomeration-grenobloise-velorution-geante

L'Association pour le Développement des Transports en Commun (ADTC) et
Alternatiba Grenoble organisent une "Grande Convergence Vélo" : une vélorution
géante au départ de plusieurs points de l'agglomération grenobloise
(Fontanil, Noyarey, Montbonnot, Seyssins, Claix, Domène, Vizille, Le Gua)
et en direction de la Place Verdun.

Le but ? Exiger de meilleurs aménagements cyclables et promouvoir les
déplacements à vélo, pour se libérer de toute cette pollution atmosphérique
insupportable.

Des départs sont organisés à partir de 8h depuis de nombreuses communes
de la Métropole. Tous les horaires de passages sont disponibles sur le
lien suivant : https://www.convergence-velo-grenoble.fr/



