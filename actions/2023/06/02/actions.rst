
.. _actions_2023_06_02:

=========================================================================================================================================
**Actions du vendredi 2 juin 2023 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Solidarite #Yovan #Mathieu**
=========================================================================================================================================

- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#2023-06-02 #Grenoble #64AnsCestToujoursNon #ReformedesRetraites #SeuleLaLuttePaie #Solidarite #Yovan #Mathieu


.. _soutien_yovan_et_mathieu_2023_06_02:

|ici_grenoble| |car38| A la une **Soutenir Yovan et Mathieu** |solidarite|
==================================================================================


.. figure:: images/page_une_ici_grenoble_2023_06_02.png
   :align: center

   https://www.ici-grenoble.org/article/soutenir-yovan-et-mathieu, https://www.ici-grenoble.org/newsletter


Lors de la manifestation du 1er mai 2023 à Grenoble, Yovan et Mathieu ont été
arrêtés et placés en détention provisoire à Varces.

Pourquoi ?

Que s'est-il passé pendant cette manifestation ?

En quoi est-ce un tournant dans la répression à Grenoble ?

Voici le récit et les explications du Collectif Anti-Répression Isère :

"Tribunal politique, où va la République ???
--------------------------------------------------

Suite au procès en comparution immédiate pour les événements du premier mai,
nous tenons, en tant que victimes de la répression judiciaire et leurs soutiens,
à exprimer notre point de vue sur les faits.

En effet, pendant toute la durée de l'enquête et du procès, la communication
de la préfecture, du procureur et de la police a saturé l'espace médiatique.

Nous souhaitons donc vous faire part de l'histoire telle qu'elle a été vécue
de l'intérieur par les principaux concernés.

Les deux personnes qui comparaissaient, Yovan et Mathieu, étaient accusées
de violences aggravées sur Personnes Dépositaires de l'Autorité Publique
entraînant des ITT de plus de 8 jours (10 et 21 jours respectivement),
ainsi que d'autres délits mineurs (rébellion, refus de signalétique...).

Elles ont été condamnées à 4 et 6 mois de prison ferme aménagés sous forme
de bracelet électronique, ainsi que du sursis, 3 ans d'interdiction de
manifester, 1 an de privation des droits civiques et 5 ans d'interdiction
de port d'arme.

Pour nous, cette condamnation est avant tout politique, car elle s'est
basée sur un dossier pourtant vide de preuves et avec une gestion de
l'affaire par la police et la justice assez exceptionnelle pour une
comparution immédiate.

Le 1er mai 2023, vers 13h, en fin de manifestation, un cortège s'est
détaché de l'anneau de vitesse pour s'engager dans le parc Paul Mistral.

Six ou sept agents de la BAC ont alors décidé de bloquer le cortège sans
attendre de renforts. Ils ont fait face à une détermination plus forte
que d'habitude, les manifestants refusant de reculer face aux grenades
jetées par la BAC, et ont décidé d'avancer rapidement.

Dans la foulée, des renforts de la CDI sont arrivés et les policiers ont
chargé le cortège. Au cours de l'affrontement, six manifestants et deux
policiers ont été blessés.

Une personne, Mathieu, a également été arrêtée. Il a été frappé à la tête
et traîné au sol sur 50 mètres.

Une autre personne, Yovan, a été arrêtée 2h30 plus tard au carrefour de
l'Aigle, lors d'un rassemblement devant le McDonald's, sans qu'un motif
lui soit donné, au seul prétexte qu'il avait de la boue sur le pantalon.
L'arrestation et le transport ont été (assez) brutaux pour lui aussi.

Suite à cette arrestation, Yovan a été placé en garde à vue sans explications
précises des faits qui lui étaient reprochés.
Malgré sa demande, Yovan n'a vu son avocat qu'à 11h le lendemain. C'est
lors de cet entretien qu'il a découvert le motif exact de sa garde à
vue : il était suspecté d'avoir jeté une pierre sur un policier, entraînant
une ITT de 10 jours.
Pourtant, la première description du lanceur de cailloux n'apparaît au
dossier qu'à partir de 18h30, soit 2h30 après l'arrestation.

Quant à Mathieu, il aurait été formellement reconnu sur une séquence
photographique... par les policiers qui l'ont arrêté ! Pourtant, il
apparaît masqué sur les images prises pendant la manifestation...

Malgré une attitude conciliante de Yovan et des tentatives de se disculper
(prise d'ADN, empreintes digitales et témoignage précis), tout a été
retenu à charge contre lui, tout comme Mathieu qui a préféré garder le
silence.

Comme d'habitude, la police, n'ayant pas pour but d'enquêter à décharge,
n'a pas cherché à écouter les gardés à vue.

Lors de la prolongation de la garde à vue, malgré sa demande d'avocat,
Yovan n'en a pas eu.
Il a donc été menotté, intimidé, provoqué et même menacé lors de sa
deuxième audition. Pire encore, lors de son procès, l'avocat a avancé
des preuves montrant que l'avocat commis d'office n'avait pas été prévenu
de la prolongation de cette garde à vue.

Le lendemain matin, Yovan a été emmené à son domicile pour une perquisition.

Des policiers en civil, cagoulés, ont procédé à cette perquisition, et
seule sa carte d'identité a été récupérée. Il en a été de même pour
les parents de Mathieu, bien que cette perquisition ne soit pas mentionnée
dans le dossier.

Mathieu et Yovan ont ensuite été transférés devant la Juge des libertés
et des détentions, qui, malgré de très bonnes garanties de représentation,
a choisi la détention provisoire en prenant comme excuse les manifestations
de soutien devant le tribunal.

Ils ont ensuite été écroués à la prison de Varces le mercredi soir, isolés
pendant 2 jours supplémentaires, dans l'incapacité de préparer correctement
leur défense.

Toujours dans l'optique de prolonger leur calvaire, ils ont été déférés
au palais de justice le vendredi à 12h30, sans avoir mangé depuis 17h30
la veille.
Ils ont vu leurs avocats à 13h30 pour une audience à 14h, leur laissant
un temps très réduit pour préparer leur défense.

Pendant près de 4h30 de procès, les soutiens ont été empêchés d'entrer
dans le tribunal pour assister au procès, alors que quelques places
restaient libres malgré la monopolisation de la salle par des policiers.

Seuls quelques membres de la famille ont été autorisés à entrer grâce à
la pression des soutiens extérieurs.

Cette interdiction d'entrer dans le tribunal a également empêché plusieurs
personnes de se rendre au tribunal pour d'autres raisons, comme assister
à leur propre procès.

Les deux prévenus ont été jugés sur le fond le jour même car ils ont
choisi de ne pas demander de délais par peur de la détention provisoire.

À plusieurs reprises lors du procès, les éléments des dossiers ont été
mélangés par l'accusation, confondant Yovan et Mathieu.
Les avocats des deux prévenus ont mis en avant des dossiers vides dont
les rares éléments posaient de réels problèmes chronologiques.

Après 1h30 de délibération, Mathieu et Yovan ont été reconnus coupables
sans preuves tangibles, et plusieurs policiers ont obtenu des dommages
et intérêts malgré l'absence d'ITT.
Les deux prévenus ont fait appel, ce qui, en attendant un nouveau procès,
les rend de nouveau "présumés innocents" et suspend donc la condamnation.

**On ne peut que s'indigner de ce procès.**

Étant donné que les deux camarades sont de nouveau libres et "présumés innocents",
la preuve est faite que rien ne justifiait leur placement en détention
provisoire et l'utilisation de la procédure de comparution immédiate.

Pour rappel, cette procédure indigne est généralement utilisée pour des
affaires suffisamment simples pour permettre un jugement immédiat sans
investigation supplémentaire et pour des personnes au statut social
généralement moins privilégié que nos deux camarades (chômeurs,
sans-domicile fixe, sans-papiers, travailleurs pauvres, personnes racisées,
multirécidivistes, etc.), avec pour résultat des audiences d'une demi-heure
et des condamnations à des peines de prison ferme élevées.

Si l'audience a duré si longtemps, si la police a dû garder le tribunal
et si tout s'est fait avec autant de précipitation, c'est que la procédure
a été instrumentalisée à des fins politiques et médiatiques.

Nos deux camarades n'étaient pas les seuls visés à travers cette procédure,
mais bien toute la mobilisation avec eux.

Le but était manifestement de terroriser le mouvement social et
d'empêcher que d'autres actions déterminées aient lieu, tout en
masquant les responsabilités du travail répréhensible de la police
dans cette affaire.

Appel à vidéos
------------------

Dans la perspective de l'appel qui aura lieu dans quelques mois, nous
lançons un appel à vidéos pour pouvoir contribuer à disculper les
deux accusés.

Vous pouvez nous les transmettre par e-mail (en utilisant un hébergeur
tiers comme www.grosfichiers.com, s'il vous plaît) ou en nous rencontrant
directement.

En attendant, on ne lâche rien et on continue à se mobiliser pour un
monde meilleur (et après aussi)."

Le Collectif Anti-Répression Isère |car38|



**Agenda ici-grenoble**
==========================

.. figure:: images/ici_grenoble_2023_06_02.png
   :align: center

   https://www.ici-grenoble.org/agenda

- 09h59 https://www.ici-grenoble.org/evenement/distribution-de-repas-gratuits-pour-les-personnes-les-plus-demunies
- 14h30 https://www.ici-grenoble.org/evenement/repair-cafe-de-grenoble-centre
- 15h00 https://www.ici-grenoble.org/evenement/permanence-du-lokal-autogere
- 16h00 https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- 16h10 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- |important| 19h00 🎥 |alternatiba| |solidarite| https://www.ici-grenoble.org/evenement/cine-caisse-de-greve-la-cigale-le-corbeau-et-les-poulets-sur-des-papys-revolutionnaires-face-a-la-police-antiterroriste
- https://www.ici-grenoble.org/evenement/soiree-a-la-ferme-du-loutas
- 19h35 🎥 https://www.ici-grenoble.org/evenement/cinema-je-verrai-toujours-vos-visages-sur-les-experiences-de-justice-reparative
- 20h30 🎥 https://www.ici-grenoble.org/evenement/cinema-lamour-et-les-forets-sur-les-pervers-narcissiques-et-les-relations-amoureuses-toxiques

