
.. _actions_2023_03_21:

======================================================
**Actions du mardi 21 mars 2023**
======================================================

#grenoble #retraites #luttes #RéformeDesRetraites #49_3

Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_21.png
   :align: center

   https://38.demosphere.net/


Agenda ici-grenoble
=====================

.. figure:: images/ici_grenoble_2023_03_21.png
   :align: center

   https://www.ici-grenoble.org/agenda



Une immense vague humaine déferle ce soir sur les quais de #Grenoble : le message adressé à #Macron, à #Borne, #Dussopt, #Veran, on ne lachera rien !
=========================================================================================================================================================

- https://pouet.chapril.org/@lcomparat/110062940787814871
- https://storage.gra.cloud.ovh.net/v1/AUTH_011f6e315d3744d498d93f6fa0d9b5ee/qotoorg/cache/media_attachments/files/110/062/940/960/119/392/original/98b936e9a4f02e75.mp4


Une immense vague humaine déferle ce soir sur les quais de #Grenoble :
le message adressé à #Macron, à #Borne, #Dussopt, #Veran, on ne lachera rien !

Les Grenoblois-es mobilisé-es jusqu'au retrait de la loi scélérate.

#manif21mars  #64AnsCestToujoursNon  #ReformedesRetraite
