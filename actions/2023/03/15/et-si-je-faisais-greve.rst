.. index::
   pair: Tract; 2023-03-15
   pair: Faire grève; 2023-03-15

.. _faire_greve_2023_03_15:

=======================================
2023-03-15 **Et si je faisais grève**
=======================================

TOUS LES SALARIES PEUVENT SE METTRE EN GREVE
===================================================

Les syndicats ont déposé des préavis de grève au niveau national et dans
toutes les branches professionnelles pour TOUS LES JOURS jusqu'à la fin
du mois de mars.


Donc TOUS les salariés ont le droit de faire grève, même moi !
===================================================================

- oui, même si je suis le seul à faire grève dans mon entreprise
- oui, même si je ne suis pas syndiqué ou qu'il n'y a pas de syndicat dans mon entreprise
- oui, même si mon patron me dit « tu n'as pas le droit de faire grève »
- oui, même les jours où il n'y a pas de manifestation
- oui, même si je travaille dans le secteur privé ou dans une toute petite entreprise


Est-ce que je dois prévenir à l'avance ou demander l'autorisation
======================================================================

NON, dans la plupart des secteurs et professions, je peux décider à la
dernière minute de me mettre en grève, et je ne suis pas tenu d'informer
que je suis en grève.

C'est à mon employeur de constater que je ne suis pas venu au travail
et de me déclarer gréviste.

(Seules certaines professions sensibles sont soumise à une nécessité de
prévenir 48h à l'avance : écoles maternelles et élémentaires, transports
en commun, etc... je me renseigne auprès d'un syndicat local de mon secteur
si je ne suis pas sûr...)

NON, le droit de grève est un droit fondamental, j'ai le DROIT de faire
grève tant qu'au moins un syndicat a déposé un préavis dans mon secteur.


Je n'ai pas les moyens de faire grève...
==============================================

Il existe des caisses de solidarité pour les grévistes (dans chaque syndicat,
et aussi des caisses intersyndicales ouvertes aux non syndiqués)

Dans le secteur privé, je peux faire grève (=débrayer) seulement une
partie de la journée, dans ce cas seule une partie de mon salaire de la
journée m'est retiré.

Je peux décider de faire grève au moment où c'est le plus handicapant
ou voyant pour mon entreprise, les clients ou l'Etat

Peut-être que je suis épuisé ou malade et que je me forçais à venir au
travail : j'en profite pour aller voir mon médecin et demander un arrêt
maladie, tout à fait justifié.


Je ne suis pas salarié, je ne peux pas faire grève
=======================================================

OUI ! MAIS je peux donner aux CAISSES DE GREVES des syndicats ou
intersyndicales, pour permettre aux salarié-es de tenir sur la durée
et de bloquer l'économie.


Mon métier n'est pas « essentiel », ma grève ne bloque rien
=============================================================

SI : tous les métiers participent à l'économie d'une manière ou d'une autre.

D'ailleurs, pendant le confinement, l'état était pressé que TOUT LE MONDE
reprenne le travail, justement pour « sauver l'économie ».

Chaque jour de grève, je fais perdre de l'argent (provisoirement) à mon
employeur, et donc à l'Etat, et je participe à la désorganisation du pays,
qui incitera les patrons et les puissants à exiger de Macron de capituler
pour remettre de l'ordre dans le pays.

C'est mon entreprise que je pénalise, pas Macron !
=====================================================

OUI, MAIS : mon entreprise s'en remettra, et plus les entreprises seront
impactées, plus elles se plaindront, et Macron écoute le patronat (ce sont
même les seules personnes qu'il écoute).

Il sera obligé de céder sous leur pression.

Ce sont les usagers que je pénalise, pas Macron ni les patrons !
====================================================================

OUI, MAIS : la majorité des français soutiennent le mouvement et même le
blocage du pays. Les usagers comprendrons.

Et un usager qui ne peut prendre les transports ou mettre son enfant à
l'école, c'est un usager qui ne pourra lui-même pas travailler et
contribuera à ralentir l'économie, et à réveiller les patrons comme Macron.

Je vais plutôt donner aux caisses de grèves des cheminots, aux électriciens et aux raffineurs, c'est eux qui peuvent bloquer le pays
=========================================================================================================================================

OUI, MAIS : tout argent est bon à prendre, mais eux-mêmes (cheminots etc.)
demandent à ce que les autres secteurs les rejoignent dans la grève,
ils refusent de tenir la grève tous seuls car cela ne suffira pas à faire
plier le patronat et le gouvernement.


De toute façon la loi va passer cette semaine
================================================

ET ALORS ? En 2006, le gouvernement a passé la loi sur le CPE (contrat
première embauche) en mars, la bataille a continué pendant 2 mois et le
président a finalement RENONCÉ à promulguer la loi.




