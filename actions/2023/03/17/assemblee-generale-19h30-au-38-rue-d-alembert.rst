.. index::
   pair: Assemblée Générale; 2023-03-17

.. _ag_2023_03_17:

=========================================================================================================
2023-03-17 **L'AG DE lutte a été déplacée à ce soir (vendredi) 19h30 au 38 rue d'Alembert à Grenoble !**
=========================================================================================================

- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://38.demosphere.net/rv/728

Suite au 49.3, l'AG DE LUTTE a été déplacée à ce soir (vendredi) 19h30
au 38 rue d'Alembert à Grenoble !

Assemble générale ouverte à tou-te-s, organisant des ACTIONS en soutien
à la lutte contre la réforme des retraite.

Si vous aussi, vous pensez qu'il ne faut pas attendre jeudi prochain et
son ènième manif pour agir ! Soyons le plus nombreux possible !

