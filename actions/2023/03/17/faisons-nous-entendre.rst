.. index::
   ! Bloquons l'économie

.. _faisons_nous_entendre_2023_03_17:

===============================================================
2023-03-17 **Faisons nous entendre, bloquons l'économie**
===============================================================

:download:`Télécharger le tract au format PDF <pdfs/tractage_faisons_nous_entendre.pdf>`


.. figure:: images/rien_nest_joue.jpg
   :align: center


.. figure:: images/bloquons_leconomie.jpg
   :align: center
