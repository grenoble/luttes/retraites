
.. _agendas_2023_03_20:

======================================================
**Actions du 20 mars 2023**
======================================================

Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_20.png
   :align: center

   https://38.demosphere.net/


Agenda ici-grenoble
=====================

.. figure:: images/ici_grenoble_2023_03_20.png
   :align: center

   https://www.ici-grenoble.org/agenda

