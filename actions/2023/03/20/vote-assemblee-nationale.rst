.. index::
   ! Motion de censure

.. _vote_1240_2023_03_20:

================================================================
2023-03-20 **Motion de censure legislature-16/vote_1240**
================================================================

- http://x91ms.mjt.lu/nl3/3m-SzflY3n6ZzqGz0fBDoA?m=AVIAABAuzmkAAAAPrjQAABszKqIAAAAAiDYAAGTqABhdbwBkGW1mQQOriTs3TZyWH3zbZMA2rAAXzIs&b=d2a719e7&e=c1fa21a6&x=c0-x5Hvz_86BNZFZU2a8MQ
- https://datan.fr/votes/legislature-16/vote_1240


Les motions de censure rejetées à l'Assemblée nationale

Les deux motions de censure ont été rejetées, lundi 20 mars, à
l'Assemblée nationale.

La motion de censure du Rassemblement nationale a recueilli 94 voix
tandis que celle déposée par le groupe LIOT et soutenue par la NUPES
a été votée par 278 voix.

Cette dernière a donc été rejetée à 9 voix près.

Ces motions ont été déposées à la suite de l'utilisation de l'article 49-3
par le gouvernement d'Elisabeth Borne pour faire adopter la réforme des
retraites.

Pour rappel, si une motion de censure est adoptée, le gouvernement doit
démissionner.

Avec ces rejets, la réforme des retraites a donc été adoptée.
