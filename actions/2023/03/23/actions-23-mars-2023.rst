
.. _actions_2023_03_23:

==========================================================================
**Actions du jeudi 23 mars 2023 Le jeudi de la colère**  #Greve23Mars
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_23.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/article/le-jeudi-de-la-colere
- https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste
- https://www.ici-grenoble.org/evenement/manifestation-intersyndicale-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cortege-feministe-contre-la-reforme-des-retraites-et-son-monde
- https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/rencontres-de-luniversite-autogeree-le-non-travail
- https://www.ici-grenoble.org/evenement/cinema-we-are-coming-chronique-dune-revolution-feministe


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_23.png
   :align: center

   https://38.demosphere.net/


Librinfo 74
==============

- https://librinfo74.fr/les-pancartes-citoyennes-du-23-mars-retrospectives/
