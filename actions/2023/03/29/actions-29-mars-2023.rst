
.. _actions_2023_03_29:

==========================================================================
**Actions du mercredi 29 mars 2023**
==========================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_29.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

Rassemblement de soutien aux grévistes d'Athanor (blocage de l'incinérateur) |solidarite|
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-aux-grevistes-dathanor-blocage-de-lincinerateur

Rassemblement à l'incinérateur de déchets, en soutien aux agent.e.s de
la collecte en grève contre la réforme des retraites !

Soyons solidaires |solidarite| avec les "premiers de corvée".

Besoin d'être très nombreux.ses !!

À 6h du matin

Rendez-vous devant l'incinérateur
51 Rue de la Carronnerie
38240 Meylan

- https://www.ici-grenoble.org/evenement/formation-les-masculinite-s-en-jeux-deconstruire-les-mythes-de-virilite-et-de-domination

AG des étudiant-e-s en lutte contre la réforme des retraites
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://www.ici-grenoble.org/evenement/ag-des-etudiant-e-s-en-lutte-contre-la-reforme-des-retraites

Les étudiant-e-s en lutte contre la réforme des retraites organisent une
Assemblée Générale ouverte à toutes et à tous.

Venez débattre et décider des actions à venir.

À midi

Sur le campus
Devant Stendhal


- https://www.ici-grenoble.org/evenement/permanence-du-magasin-gratuit-du-38-rue-dalembert-don-de-vetements-aliments-objets
- https://www.ici-grenoble.org/evenement/rencontres-balance-ta-quittance-comment-sorganiser-face-a-lexplosion-des-loyers-et-des-charges
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran
- https://www.ici-grenoble.org/evenement/nouvelle-reunion-gnoup-pour-la-creation-dun-centre-social-autogere-a-grenoble
- https://www.ici-grenoble.org/evenement/assemblee-generale-de-lutte-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/repetition-ouverte-de-la-chorale-de-chants-revolutionnaires-les-barricades
- https://www.ici-grenoble.org/evenement/reunion-de-quartier-saint-bruno-organisons-la-lutte-contre-la-reforme-des-retraites


CRIC
=======

.. figure:: images/cric_2023_03_29.png
   :align: center

   https://cric-grenoble.info/spip.php?page=agenda


Liens
---------


C’est quoi le colonialisme aujourd’hui ?
--------------------------------------------

- https://cric-grenoble.info/infos-locales/article/c-est-quoi-le-colonialisme-aujourd-hui-2871


Dans le cadre du Mois Décolonial, Antigone accueille l’exposition de la
Fédération des Associations de Solidarité avec Tou·te·s les Immigré·e·s
sur le thème "C’est quoi le colonialisme aujourd’hui ?"

L’exposition est visible pendant les permanences d’Antigone (le mercredi
de 18h30 à 21h30 et le dimanche de 17h à 20h) durant tout le mois de mars.


AG de luttes et rdv de la semaine
++++++++++++++++++++++++++++++++++++++

- https://cric-grenoble.info/infos-locales/info-locales/article/ag-de-luttes-et-rdv-de-la-semaine-2911


Notre assemblée a pour objectif d’appuyer les luttes en cours par la promotion,
l’organisation et le soutien à des actions directes.

En effet bien que nous appelions et participions aux manifestations massives
qui ont lieu depuis plusieurs semaines, nous avons la conviction qu’elle
ne suffiront pas à faire battre le gouvernement en retraite.

Depuis le début du mouvement social nous avons donc organisé et/ou soutenu
les blocages universitaires, des blocages de transport ou d’espaces commerciaux
ou encore des opérations de péages gratuits. Une partie de ces actions
a servi à soutenir la grève générale reconductible en reversant les
bénéfices à différentes caisses de grève.

Contre la réforme des retraites, la loi "Plein emploi" ou encore les lois
Darmanin et Kasbarian, continuons de nous réunir, en tant qu’organisation
ou individus afin de mettre en échec leurs projets bourgeois, racistes
et sexistes !

Vous pourrez également discuter avec certain·es d’entre nous et prendre
une boisson chaude en manif’ sous le poulpe rouge et noir de la Roulutte.

CONTACT
agdeluttesgre@canaglie.net


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_29.png
   :align: center

   https://38.demosphere.net/


.. figure:: ../28/images/affiche_contre_macron_et_son_monde.jpg
   :align: center
