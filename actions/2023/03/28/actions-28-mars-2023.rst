
.. _actions_2023_03_28:

=========================================================================================================================================
**Actions du mardi 28 mars 2023, 10e manifestation appel intersyndical à bloquer le pays contre une réforme des retraites injuste**
=========================================================================================================================================

- https://www.mediamanif.org/
- https://www.ici-grenoble.org/agenda
- https://38.demosphere.net/
- :ref:`argumentaires`


#64AnsCestToujoursNon #ReformedesRetraite #Grenoble #grenoble #MediaManif


**Agenda ici-grenoble**
==========================

- https://www.ici-grenoble.org/article/grevue-de-presse-les-10-infos-de-mars

.. figure:: images/ici_grenoble_2023_03_28.png
   :align: center

   https://www.ici-grenoble.org/agenda


Liens
-------

- https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste
- https://www.ici-grenoble.org/evenement/manifestation-intersyndicale-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cinema-we-are-coming-chronique-dune-revolution-feministe
- https://www.ici-grenoble.org/evenement/permanence-du-dal-isere-pour-tout-probleme-de-logement-en-tant-que-locataires
- https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites
- https://www.ici-grenoble.org/evenement/cine-caisse-de-greve-merci-patron
- https://www.ici-grenoble.org/evenement/ag-ecolo-anticapitaliste
- https://www.ici-grenoble.org/evenement/cercle-decoute-feministe-pour-hommes
- https://www.ici-grenoble.org/evenement/cine-caisse-de-greve-la-sociale-sur-les-origines-de-la-securite-sociale
- https://www.ici-grenoble.org/evenement/special-retraites-un-pays-qui-se-souleve-frederic-lordon


Demosphere 38
=================

.. figure:: images/demosphere_38_2023_03_28.png
   :align: center

   https://38.demosphere.net/

.. figure:: images/affiche_contre_macron_et_son_monde.jpg
   :align: center


Quelques pancartes
===================

Au lieu de nous bassiner avec les retraites, retraitez les bassines !
---------------------------------------------------------------------------

- https://pouet.chapril.org/@lcomparat/110100964998306630

#Grenoble #greve28mars retrait de la #reformedesretraites et des #megabassine même combat !

.. figure:: images/retraitez_les_bassines.png
   :align: center

   Au lieu de nous bassiner avec les retraites, retraitez les bassines !


L'eau est aussi précieuse que nos retraites
---------------------------------------------------------------------------------------------------

- https://pouet.chapril.org/@lcomparat/110100938636260752

Message universel à résonance grenobloise ! #grenoble non a la #reformeDesRetraites #greve28mars

.. figure:: images/eau_preciseuse.png
   :align: center

   L'eau est aussi précieuse que nos retraites


Compte rendus
==============

Encore une très grosse manifestation à Grenoble, 40 000 annoncé, certes
un peu moins que la dernière mais toujours déterminé.es.

Les éboueurs de la Metro enfin en grève.
80% de grévistes au Dauphiné Libéré (qui n’est pas sorti dans les kiosques).

Présence forte des jeunes, avec la fac, les lycées, l’Ecole d’Archi et
l’Ecole d’Art.

Gros cortège culture (plus que le 23). Plus de 300.

Une AG Culture en lutte s’était tenue à midi à la Bobine avec une quinzaine
de personnes motivées pour continuer les actions notamment vers les
festivals, les musées (Le Louvre est en grève !) après avoir tracté le
dimanche devant les cinémas.

Une exposition de la fédération Spectacle CGT sur les occupations de 2021
est en préparation avec l’Ecole d’Art toujours occupée.

Prochaine AG Culture en lutte vendredi 31 mars à 12H à la Bobine.


cric
-----

- https://cric-grenoble.info/infos-locales/info-locales/article/journee-et-soiree-du-28-mars-2921


Librinfo74
==============

- https://librinfo74.fr/les-pancartes-citoyennes-du-28-mars-retrospectives/
