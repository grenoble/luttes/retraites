.. index::
   pair: Communiqué; Intersyndical (2023-01-10)

.. _com_intersyndical_2023_01_10:

===============================================================================================================
2023-01-10 **communiqué intersyndical national (CFDT, CGT, FO, CFE-CGC, CFTC, Unsa, Solidaires, FSU)**
===============================================================================================================


"Retraites : première journée de mobilisation le 19 janvier 2023
================================================================

La Première Ministre a annoncé ce 10 janvier 2023 le report de l’âge
légal de départ à la retraite à 64 ans avec une accélération de
l’augmentation de la durée de cotisation.

Cette réforme va frapper de plein fouet l’ensemble des travailleurs et
travailleuses, et plus particulièrement celles et ceux qui ont commencé
à travailler tôt, les plus précaires, dont l'espérance de vie est inférieure
au reste de la population, et celles et ceux dont la pénibilité des métiers
n'est pas reconnue.

Elle va aggraver la précarité de celles et ceux n’étant déjà plus en
emploi avant leur retraite, et renforcer les inégalités femmes-hommes.

Le système de retraites par répartition n’est pas en danger, rien ne
justifie une réforme aussi brutale.

Attachées à un meilleur partage des richesses, les organisations syndicales
n’ont eu de cesse pendant la concertation avec le Gouvernement de proposer
d’autres solutions de financement, à commencer par l’amélioration de
l’emploi des seniors.

Jamais le gouvernement, arc-bouté sur son projet, ne les a étudiées sérieusement.

Suite à l’annonce de la réforme gouvernementale, toutes les organisations
syndicales se sont immédiatement réunies pour construire une réponse
commune de mobilisation interprofessionnelle.

Celle-ci prendra la forme d’une première journée de grèves et de
manifestations le 19 janvier 2023.

Parce qu’elles représentent l’ensemble des travailleurs et des travailleuses,
demandeurs et demandeuses d’emploi, et retraité.es, c’est cette date syndicale
qui donne le départ d’une puissante mobilisation sur les retraites dans la durée.

Elles décident d’ores et déjà de se réunir le 19 janvier au soir avec
les organisations de jeunesse pour prolonger le mouvement de mobilisation
et convenir d’autres initiatives.

Les organisations syndicales appellent les salarié.es à se mobiliser
fortement dès cette première journée dans l’ensemble du territoire et à
participer aux différentes initiatives organisées en intersyndicale. "

