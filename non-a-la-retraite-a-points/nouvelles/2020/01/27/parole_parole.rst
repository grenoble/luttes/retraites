.. index::
   pair: Retraites; Parole, Parole,  Parole 🎶✊


.. _parole_2020_01_27:

===========================================
**Parole parole parole** 🎶✊
===========================================

.. seealso::

   - https://twitter.com/tropical_boy/status/1221943872395382786?s=20



Les grévistes ont interrompu les voeux de la ministre des sports Roxana Maracineanu
avec la chanson de Dalida "parole, parole, parole". 🎶✊


.. figure:: parole_parole/parole_parole.png
   :align: center

   https://twitter.com/tropical_boy/status/1221943872395382786?s=20
