.. index::
   pair: Il faut savoir...retirer une « réforme ! ; Tract
   pair: CNT ; Tract

.. _tract_cnt_2020_01_29:

==================================================================================================
**Il faut savoir...retirer une réforme !** (mercredi 29 janvier 2020)
==================================================================================================

.. seealso::

   - https://paris.demosphere.net/rv/77746


Tract de la CNT pour la **manifestation du mercredi 29 janvier 2020**

.. contents::
   :depth: 3

Tract au format PDF
=====================

:download:`Tract au format PDF <tract_cnt/tract_retraites_2020_01_29.pdf>`

Le texte
=========

C’est comme une rengaine : **Il faut savoir terminer une grève**, reprise depuis
quelques jours par le gouvernement et ses valets.

Mais que demandent les travailleurs et les travailleuses qui font vivre le
mouvement social contre le projet de **réforme** des retraites entamé
le 5 décembre 2019 ? Le retrait du projet !

Les fossoyeurs de notre système de protection sociale solidaire,
dont Edouard Philippe et Emmanuel Macron, sont passés maîtres dans
cette pratique de l’enfumage au sujet du système "universel" de
retraite. En novembre 2019, pour justifier la casse du régime actuel, on nous
annonçait entre 8 et 17 milliards d’euros de déficit des caisses de
retraite en 2025.
Cette approximation de 9 milliards prouvait déjà le manque de sérieux d’un tel
argument. Surtout, alors que la Constitution prévoit que toute réforme fasse
l’objet d’une étude d’impact, celle du projet Delevoye s’est fait attendre.
C’est à quelques jours seulement de la présentation du projet de loi en conseil
des ministres (le 24 janvier 2020) que le gouvernement s’est mis à communiquer
sur les chiffres.  Curieux silence. serait-ce parce que la population continue
de soutenir le mouvement social ?!

Le gouvernement a voulu faire diversion en annonçant le retrait provisoire de
l’âge pivot, auquel seule adhère la direction de la CFDT.
L’âge pivot aura un rôle capital, pouvait-on lire dans « LeMonde » le 21 janvier 2020
(qui annonçait le même jour qu’Emmanuel Macron envisageait une nouvelle baisse
de la fiscalité des entreprises). Et pour cause : l’instauration d’un âge pivot
également appelé âge d’équilibre – fixé, pour le moment, à 64 ans a bien pour
objectif de nous faire bosser plus longtemps !

En dessous de ce seuil, la pension serait minorée ; au-delà, elle serait majorée.
Ce système de bonus-malus sur la valeur du point de retraite est synonyme de
double peine pour les travailleurs et travailleuses : un départ «anticipé»
signifiant moins de points de retraite accumulés, et une valeur du
point moins importante.

Pas de panique, nous dit-on : toute personne qui travaillera jusqu’à 67 ans serait
gagnante avec le nouveau système ! Mais si nous sommes censés vivre plus longtemps,
l’espérance de vie en bonne santé est de 62,5 ans pour les hommes et de 64,7 ans
pour les femmes (chiffres Insee, septembre 2019).

Et alors que les jeunes accèdent à l’emploi de plus en plus tard, lorsqu’ils y
accèdent, un tiers des seniors sont déjà privés d’emploi au moment où ils
liquident leur retraite. Enfin, l’abaissement des pensions programmé par
cette «réforme» forcera un glissement vers des retraites par capitalisation,
vers une logique d’épargne individuelle soumise aux chantages de la finance.

Au-delà de cette nouvelle attaque, c’est contre l’idéologie clairement affichée
par ce projet que nous nous battons. La société que nous voulons construire, en
rupture avec le capitalisme, est celle de l’action collective, de l’égalité et
de la solidarité.

Notre mobilisation continue aujourd’hui.

L’arme des travailleuses et travailleurs, c’est la grève et le blocage de
l’économie. C’est le seul langage que comprennent le gouvernement et le patronat.

La CNT appelle à poursuivre et amplifier la mobilisation, partout où il le
faudra, comme il le faudra, tant qu’il le faudra !


Dans l’immédiat, la CNT revendique le retrait de la réforme
Delevoye. Et, pour la suite, nous exigeons : - une retraite à taux
plein pour toutes et tous à 55 ans - l’égalité des pensions et leur
augmentation - la réappropriation et l’autogestion des caisses de
sécurité sociale par les salariées et salariés eux-mêmes
