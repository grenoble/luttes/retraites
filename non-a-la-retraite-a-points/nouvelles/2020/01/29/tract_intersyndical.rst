.. index::
   pair: Pour la défense de nos retraites Toujours déterminé.es à gagner ! ; Tract

.. _tract_retraites_2020_01_29:

==================================================================================================
**Pour la défense de nos retraites Toujours déterminé.es à gagner !** (mercredi 29 janvier 2020)
==================================================================================================

Tract unitaire de l'intersyndicale pour la **manifestation du mercredi 29 janvier 2020**

.. contents::
   :depth: 3

Tract au format PDF
=====================

:download:`Tract au format PDF <tract_intersyndical/Tract_unitaire_2020_01_29.pdf>`

Tract au format doc (désolé)
==================================

:download:`Tract au format doc <tract_intersyndical/Tract_unitaire_2020_01_29.doc>`

Le texte
=========

Le Président de la République a donné son feu vert à l’examen en Conseil des
ministres du projet de loi sur les retraites le 24 janvier 2020.
Ce projet renvoie à de nombreuses ordonnances et décrets qui définiront
ultérieurement et sans débat les dispositions structurantes du régime
prétendu universel.

**Travailler plus longtemps et de baisser les pensions**.

La retraite par point, c’est forcément une baisse des pensions car toute la
carrière sera prise en compte dans le calcul, contre les 25 meilleures années
dans le privé et les 6 derniers mois dans le public actuellement.

Le gouvernement ment en promettant une retraite minimum à 1000 € puisqu’il
faudra avoir cotisé 43 ans et qu’il n’y a aucune garantie sur la valeur du
point (art. 40 du projet de loi).

L’âge de départ réel à la retraite à taux plein est repoussé à 65 ans (art. 10).

Le gouvernement assume vouloir développer les **fonds de pensions et assurances
privées** pour compenser les faibles pensions futures (art. 64), tout en excluant
les salaires de plus de 10.000 €/mois de cotisation, soit une perte
de 3,7 milliards par an.

Ce projet amènera mécaniquement, comme en Allemagne ou en Suède, à une explosion
de la précarité chez les personnes âgées et pour les carrières non continues,
en particulier les femmes.

**La population n’est pas dupe et continue à s’opposer à cette réforme.**
Les actions et les grèves se multiplient sur l’ensemble du territoire.

Nos organisations se félicitent de la forte participation des salarié.es, des
retraité.es et de la jeunesse sur les temps de grève et de manifestations.

Le Parlement devrait commencer à discuter du projet de Loi et le 30 janvier 2020,
le gouvernement ouvrira la première réunion de la conférence sur le financement
des retraites.

Nos organisations appellent à une journée massive de grève et de manifestations
interprofessionnelles et intergénérationnelles la veille, ainsi qu’à amplifier
les actions les jeudi et vendredi (arrêts de travail, piquets de grèves, assemblées
générales, diffusions de tracts, interpellations des parlementaires, actions de
dépôt des outils de travail dans des lieux symboliques...)

**La détermination à faire retirer ce projet de loi est entière, la nécessité de
réussir est de la responsabilité de tou.te.s.**

::

    Mercredi 29 janvier
    Grève et manifestations
    Grenoble, 14h départ Gare SNCF
    Une manif Retraites aux flambeaux aura lieu le 30 janvier à 18h à la Cote St André
