.. index::
   pair: Article ; Les ahurissantes explications de responsables LREM sur les retraites

.. _jean_gadrey_2020_01_26:

========================================================================================================================
**Les ahurissantes explications de responsables LREM sur les retraites** par Jean Gadrey le dimanche 26 janvier 2020
========================================================================================================================

.. seealso::

   - https://blogs.alternatives-economiques.fr/gadrey/2020/01/26/les-ahurissantes-explications-de-responsables-lrem-sur-les-retraites


Introduction
=============

Ou bien ils n’ont rien compris, ou bien ils savent qu’ils mentent, ou bien les
deux.
Dans de nombreux cas, je penche pour cette dernière hypothèse. Florilège de
stupidités mensongères ou ignorantes.

Trouvé dans le métro lillois hier, le gratuit 20 minutes, incluant une interview
avec Olivier Véran, député LREM et, excusez du peu, « l’un des rapporteurs de la réforme ».

Un expert donc.


Olivier Véran
================

Question : combien d’euros de salaire brut permettront d’acquérir un point ?

Réponse : «dix euros permettront d’acquérir un point».

Si cette réponse ne vous dit rien, ne paniquez pas, presque personne ne comprend
ce machin, qui est fait justement pour nous embrouiller, avant de nous dépouiller.

Mais le député-rapporteur est malgré tout supposé savoir que la bonne réponse
au sens du regretté Delevoye (paix à ses multiples fonctions non déclarées)
est : **dix euros cotisés** valent un point.

Erreur vénielle qui n’est rien à côté de celles des grandes vedettes de l’humour
marcheur que sont Sibeth Ndiaye et Muriel Pénicaud.

Sibeth Ndiaye
==============

La première est porte-parole du gouvernement. Face à Léa Salamé et à Nicolas Demorand,
le 12 décembre 2019 sur France Inter, elle a crânement assuré que ceux qui
partiraient à l'âge légal payeraient « un malus pendant deux ans et à partir de 64 ans
[reviendraient] à taux plein ».
Et pour ceux qui partent à 62 ans : « On paie un malus pendant deux ans et à
partir de 64 ans, on revient à taux plein. »

Quelques heures plus tard, bien conseillée, elle réparait cette grossière erreur
un tantinet populiste par un tweet : «partir avant l’âge d’équilibre, ce
sera partir avec un malus qui **s’appliquera tout au long de la retraite**.

Nous voilà rassurés.

La même porte-parole a fait mieux avant-hier 24 janvier 2020 sur BFMTV, en réponse à
la question : comment expliquer que 61% des Français se prononcent pour le
retrait de la réforme ?

Réponse : "Je pense que ça, ça traduit une forme d'exaspération. D'exaspération,
d'abord qu'on en parle énormément, qu'il y ait beaucoup de manifestations,
qu'il y ait parfois de la violence assez radicale dans certaines manifestations.

Je pense que ça traduit de la lassitude, et que les Français ont envie de passer
à autre chose"

Comment ne pas plaindre une porte-parole sommée de répondre à des questions
aussi insidieuses, voire malveillantes, et se trouvant dès lors dans l’obligation
de fournir la seule réponse logique qui lui vient à l’esprit : les gens sont
contre cette réforme parce qu’ils en ont marre des manifs contre la réforme.
Pas si bête… Ndiaye !

Muriel Pénicaud
==================

Mais la grande pédagogue de la réforme, c’est Muriel.

Elle a tout bien compris, et ça se sent, en particulier dans cette interview
remarquée que quelques vilains moqueurs ont raillée. J’ai retranscrit ses
propos, et, vous allez le constater, c’est limpide :

Question (fort impertinente) du journaliste de BFMTV : comment ça pourra
être mieux qu’avant si on ne prend plus en compte les 25 meilleures années,
franchement je ne comprends pas ?

Muriel : si, parce qu’on a le système de points qui va permettre de valoriser
toutes les années [déjà, là, elle marque… des points et d’ailleurs le journaliste
ne moufte pas].
Aujourd’hui, on compte les salaires mais pas la valeur en matière de salaires
[et toc ! Sidéré le journaliste, il n’avait pas pensé à « la valeur en matière
de salaire », une adaptation fine et libre de la théorie marxiste de la valeur].

Je m’excuse mais c’est un peu technique [vous êtes excusée Madame, c’est déjà
beau d’essayer d’expliquer des choses très techniques à des gens simples].

Par le système de points et de valeur du point on peut tout à fait faire en
sorte que l’ensemble de la carrière soit égal ou supérieur au système actuel
où on prend certes des années mais pas tout [pas bête ça, si vous pouvez ajouter
vos points acquis sur les mauvaises années, c’est tout bénéfice.

Au lieu de seulement 25 années, vous avez toute votre carrière qui est
récompensée, c’est trop cool].

Il est assez rare que des responsables politiques, sans doute soucieux de ne
pas paraître arrogants, se mettent à ce point au niveau de compréhension du
peuple, et dans le cas présent nettement en dessous. Chapeau !

Je quitte pour terminer le ton du mauvais humour. Il est clair pour (presque)
tout le monde que ces propos d’une extrême confusion, bourrés d’inexactitudes
ou de mensonges délibérés, parfois drôles à force d’être brumeux, reflètent
simplement l’embarras dans lequel se trouvent les porteurs de la parole
présidentielle lorsqu’ils font face à des questions simples et de bon sens :
comment cela pourrait-il être mieux en supprimant la référence aux meilleures
années, pourquoi une majorité de gens restent-ils durablement hostiles à une
réforme présentée comme juste, etc.

D’où ces réponses alambiquées et finalement marquées par le **mépris des
auditeurs et auditrices**.
Quand on n’a pas le droit de dire la vérité parce qu’on est un ou une porte-parole
de l’injustice et de l’injustifiable, et quand en plus on n’a pas tout compris
à un machin fait pour être opaque, on a recours au brouillard d’un discours
creux, illogique, incompréhensible. Le brouillard discursif pour enfumer,
et, dans les rues, les gaz lacrymogènes, c’est **toujours de la violence à l’œuvre**.
