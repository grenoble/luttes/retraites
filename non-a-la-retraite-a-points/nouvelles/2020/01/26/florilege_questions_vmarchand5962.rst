.. index::
   pair: Article ; Un florilège questions pertinentes et non complaisantes
   pair: Journaliste ; vmarchand5962

.. _vmarchand5962_2020_01_26:

========================================================================================================================
**Un florilège questions pertinentes et non complaisantes** de @vmarchand5962 le dimanche 26 janvier 2020
========================================================================================================================

.. seealso::

   - https://twitter.com/nico_lambert/status/1221781022167224320?s=20
   - https://twitter.com/vmarchand5962


.. figure:: vmarchand5962/video_vmarchand5962.png
   :align: center

   https://twitter.com/nico_lambert/status/1221781022167224320?s=20
