.. index::
   pair: Manifestation interprofessionnelle ; mardi 14 janvier 2020

.. _manif_2020_01_14:

============================================================================
**Manifestation interprofessionnelle** à Grenoble le mardi 14 janvier 2020
============================================================================

.. seealso::

   - https://38.demosphere.net/rv/129
