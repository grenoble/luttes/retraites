.. index::
   pair: Justice sociale; Justice climatique

.. _changer_systeme_2020_01_18:

==========================================================================================================================================
**Changer de système** par Philippe Martinez, Cécile Duflot, Jean-François Julliard, leur appel pour la **justice sociale et le climat**
==========================================================================================================================================

.. seealso::

   - https://www.lejdd.fr/Politique/philippe-martinez-cecile-duflot-jean-francois-julliard-leur-appel-pour-la-justice-sociale-et-le-climat-3943853
   - :ref:`info_retraites_2019_11_25`


.. contents::
   :depth: 3

Introduction
==============


Dans une tribune, huit responsables d'associations environnementales et de
syndicats comme la CGT, Oxfam France, Attac France ou encore Greenpeace France
appellent à **répondre à la double urgence climatique et sociale**.

"Une autre vision du monde que celles des élites de Davos existe", ajoutent-ils,
quelques jours avant l'ouverture du Forum économique mondial.

Face à la montée des températures et des inégalités sociales, huit responsables
d'associations environnementales et de syndicats s'allient pour appeler à
"répondre à la double urgence climatique et sociale". Avant la tenue du Forum
économique mondial de Davos (Suisse), du 21 au 24 janvier, ils disent rejeter
la vision du monde portée par cet événement. Voici leur tribune : "Alors que
s’ouvre la 50e édition du Forum économique de Davos, la planète brûle.

Et jamais le système économique et financier mondial, inégalitaire et climaticide,
symbolisé par ce rassemblement des élites économiques et politiques, n’a été
autant remis en question.

Scientifiques et économistes sont chaque jour plus nombreux à tirer la sonnette
d’alarme. Marches et grèves des jeunes pour le climat, mouvement des Gilets jaunes,
manifestations pour défendre nos retraites en France : ces derniers mois, ici
comme dans de nombreux pays dans le monde, les mouvements sociaux et environnementaux
portent des exigences communes et tentent de construire des projets de société
plus égalitaires, démocratiques et fondés sur la justice sociale et la préservation
des ressources de notre planète.

En France, les crises sociale et écologique puisent leurs racines dans des
décennies de politiques d’affaiblissement des acquis sociaux et des services
publics, de soumission à des doctrines économiques productivistes inefficaces
et destructrices, de toute-puissance du secteur financier et de cadeaux fiscaux
aux multinationales les plus polluantes.

Comment peut-on accepter que le lobbying d’entreprises comme Total permette de
contourner la loi avec le soutien du gouvernement et de conserver une niche
fiscale pour l’huile de palme alimentant la déforestation à l’autre bout du monde ?
Comment peut-on accepter que des entreprises comme Amazon échappent à leurs
obligations fiscales, imposent des conditions de travail indignes à leurs salariés
et détruisent davantage d’emplois qu’elles n’en créent, là encore avec le plein
soutien du gouvernement ?
Comment peut-on accepter que les très riches émettent 40 fois plus de carbone
que les plus pauvres, et qu’en plus ces derniers paient, en proportion de leurs
revenus, une taxe carbone quatre fois plus chère?

Malgré les forts mouvements de résistance, Emmanuel Macron poursuit la mise en
place de réformes néolibérales, sources d’accroissement des inégalités et de
destruction des conquêtes sociales qui structurent notre pays, et ne prend aucune
mesure ambitieuse pour faire face au changement climatique et la destruction
de la biodiversité. C'est pourtant bien la perspective d’un changement de
société qui est en jeu. En tant qu’acteurs des mouvements sociaux, écologistes
et syndicaux, nous ne pouvons nous résigner à vivre encore des années de reculs
sociaux, alimentés par les politiques des gouvernements successifs, et d’inaction
face à l'urgence écologique.
Face au sentiment grandissant d’exaspération et de crainte pour l’avenir, nous
ne pouvons accepter que la seule alternative dans les prochaines années soit
des politiques plus autoritaires, discriminantes et xénophobes.

Il n'y aura pas de résolution à la crise d'aujourd'hui avec les solutions
d'hier ou le repli sur soi. Nous, acteurs de la société civile, syndicats,
associations, ne détenons pas la recette miracle pour inverser cette tendance.
Mais nous affirmons ici une volonté de reconstruire des perspectives de
transformation radicale de la société, écologique, solidaire et démocratique,
fondées sur l’accès aux droits pour toutes et tous et le rejet de toute
discrimination de genre, d’origine, de classe et d’orientation sexuelle.

Nous appelons à une véritable transition énergétique, agricole et industrielle,
c’est-à-dire une transformation profonde des façons de produire et de consommer
qu'imposent les crises écologique et climatique. Nous aspirons à un meilleur
partage des richesses passant par une vraie justice fiscale, l'élargissement
des droits sociaux, la fin de la toute-puissance des marchés financiers et de
l'influence des multinationales sur le politique. En somme, l'opposé de ce
qui attire à Davos une élite économique et politique qui cherche avant tout
à préserver ses privilèges et ses intérêts au détriment de conditions dignes
de vie pour l’immense majorité.

**C’est dans cette perspective que nous appelons dans les prochains mois à
débattre et bâtir un projet émancipateur du 21ème siècle**.

Ce projet ne saurait irriguer la société s'il ne parle pas aussi aux personnes
les plus concernées, les plus précaires, celles qui subissent le plus durement
la violence du système néolibéral actuel et de toutes ses retombées.
Ce projet doit être relié à notre quotidien, à ce qui se vit au travail, dans
les difficultés d’accès aux services publics comme la santé, l’éducation, le
logement, les transports… Il doit s'ancrer dans des expériences et des luttes
collectives déjà menées ou en cours, être débattu dans tous les cercles,
en premier lieu les entreprises qui doivent évoluer pour faire face aux enjeux
de demain. Il doit montrer concrètement comment, par des politiques nationales
et locales alternatives, soutenant d'autres régulations internationales,
d’autres formes d’entreprises, d'autres formes de gestion des communs, nous
pouvons cesser de subir l'injustice sociale et la catastrophe écologique.
Parce qu'il n'y aura pas d'emplois sur une planète morte, il doit montrer
comment l’industrie, les services, l’agriculture doivent, elles aussi, se
soucier de protéger les ressources offertes par la nature.

C'est à ce chantier immense que nous voulons nous attaquer dans les mois à venir,
en même temps que nous voulons barrer la route, collectivement, aux projets du
gouvernement qui s'y opposent : de la casse du système de retraites et de
l'assurance chômage à la fermeture des lignes de fret ferroviaire, des
privatisations en cascade aux signatures d'accords de libre-échange climaticides…

Notre initiative n'est pas une fin mais bien le début d'un processus ouvert à
tous les acteurs qui comprennent que notre système est à bout de souffle.
Il doit nous amener à réfléchir ensemble et à produire, à partir de situations
réelles, ancrées dans le quotidien du plus grand nombre, des propositions
concrètes pour répondre à la double urgence climatique et sociale.
Et ainsi montrer à nos concitoyens qu'une autre vision du monde que celle des
élites de Davos existe."

Liste des signataires : Jean-François Julliard, directeur général de Greenpeace France ;
Philippe Martinez, secrétaire général de la CGT ; Aurélie Trouvé, porte-parole
d'Attac France ; Cécile Duflot, directrice générale d'Oxfam France ; Khaled Gaiji,
président des Amis de la Terre ; Nicolas Girod, porte-parole de la Confédération paysanne ;
Cécile Gondard-Lalanne, porte-parole de l'Union syndicale Solidaires ;
Benoit Teste, secrétaire général de la FSU.
