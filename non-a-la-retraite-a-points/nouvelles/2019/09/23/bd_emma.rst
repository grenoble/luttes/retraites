.. index::
   pair: Emma ; Bande dessinée

.. _bd_retraites_2019_09_23:

===========================================
**C'est quand qu’on arrête ?** par Emma
===========================================


.. seealso::

   - https://emmaclit.com/2019/09/23/cest-quand-quon-arrete/
   - https://emmaclitdotcom.files.wordpress.com/2019/09/cest-quand-quon-arrc3aate-1.pdf


.. contents::
   :depth: 3

Format PDF de la bande dessinée
==================================

.. seealso::

   - https://emmaclitdotcom.files.wordpress.com/2019/09/cest-quand-quon-arrc3aate-1.pdf
