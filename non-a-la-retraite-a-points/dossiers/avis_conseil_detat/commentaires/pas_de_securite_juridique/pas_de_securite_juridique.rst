.. index::
   pair: Figaro ; Avis très sévère du Conseil d'État: Le conseil d'Etat ne garantit pas la sécurité juridique de la réforme

.. _dossier_figaro_avis_tres_severe_2020_01_24:

==================================================================================================================================================
**Avis très sévère du Conseil d'État: Le conseil d'Etat ne garantit pas la sécurité juridique de la réforme** (Figaro, vendredi 24 janvier 2020)
==================================================================================================================================================

.. seealso::

   - https://www.lefigaro.fr/flash-eco/retraites-le-conseil-d-etat-ne-garantit-pas-la-securite-juridique-de-la-reforme-20200124


.. contents::
   :depth: 3


Chapeau
==========

**L'avis très sévère du Conseil d'État sur la réforme des retraites**

L'institution estime ne pas avoir le temps de «garantir au mieux la sécurité juridique»
de la réforme et déplore des **projections financières lacunaires**.

Le Conseil d'État estime ne pas avoir eu le temps de «garantir au mieux la
sécurité juridique» de la réforme des retraites, selon un avis publié vendredi 24 janvier 2020,
l'institution déplorant aussi les **projections financières lacunaires** du
gouvernement et un **recours aux ordonnances qui «fait perdre la visibilité d'ensemble»**.


Saisi le 3 janvier 2020, **le Conseil d'État n'a disposé que de trois semaines** pour
rendre son avis sur les deux projets de loi (organique et ordinaire),
que **le gouvernement a en outre modifiés à six reprises** durant cette période,
ce qui «ne l'a pas mis à même de mener sa mission avec la sérénité et les délais
de réflexion nécessaires pour garantir au mieux la sécurité juridique de l'examen
auquel il a procédé», estime-t-il.

Une «situation d'autant plus regrettable» qu'il s'agit d'une réforme «inédite
depuis 1945 et destinée à transformer pour les décennies à venir (...) l'une
des composantes majeures du contrat social», ajoute la plus haute juridiction
administrative française, dans ce document publié sur le site Légifrance.

Un avis sévère, qui n'épargne pas l'étude d'impact accompagnant les deux textes:
la première mouture était «insuffisante» et même une fois complétée, **les projections
financières restent lacunaires**, en particulier sur la hausse de l'âge de
départ à la retraite, **le taux d'emploi des seniors**, les dépenses d'assurance-chômage
et celles liées aux minima sociaux.


Le problème des ordonnances (29 ordonnances)
==============================================

Le Conseil d'État pointe également le choix de recourir à **29 ordonnances**, y
compris «pour la définition d'éléments structurants du nouveau système de retraite»,
ce qui «fait perdre la visibilité d'ensemble qui est nécessaire à l'appréciation
des conséquences de la réforme et, partant, de sa constitutionnalité et de sa
conventionnalité».

Celle portant sur la **conservation à 100% des droits constitués** au moment de la
bascule entre le système actuel et le futur «système universel» est jugée
«particulièrement cruciale», à tel point «qu'en l'absence d'une telle ordonnance»
la réforme «ne s'appliquera pas» aux personnes nées à partir de 1975.

Enfin, l'engagement de revaloriser les enseignants et des chercheurs via des loi
de programmation est condamné à disparaître du texte car «ces dispositions
constituent une injonction au gouvernement de déposer un projet de loi et sont
ainsi **contraires à la Constitution**.
