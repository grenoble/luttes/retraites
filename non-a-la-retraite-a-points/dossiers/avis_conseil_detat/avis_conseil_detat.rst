.. index::
   pair: Conseil d'Etat ; vendredi 24 janvier 2020

.. _avis_conseil_detat_2020_01_24:

==========================================================================================================================================================
**Avis sur un projet de loi organique et un projet de loi instituant un système universel de retraite** par le Conseil d'Etat le vendredi 24 janvier 2020
==========================================================================================================================================================

.. seealso::

   - https://www.conseil-etat.fr/ressources/avis-aux-pouvoirs-publics/derniers-avis-publies/avis-sur-un-projet-de-loi-organique-et-un-projet-de-loi-instituant-un-systeme-universel-de-retraite
   - https://www.conseil-etat.fr/Media/actualites/documents/2020/01-janvier/avis_retraite/avis_retraite


Format PDF
=============

.. seealso::

   - https://www.conseil-etat.fr/Media/actualites/documents/2020/01-janvier/avis_retraite/avis_retraite


Commentaires
=============

.. toctree::
   :maxdepth: 3

   commentaires/commentaires
