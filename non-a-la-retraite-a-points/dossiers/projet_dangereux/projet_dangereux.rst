.. index::
   pair: Henri ; Sterdyniak
   pair: Un projet de loi dangereux ; Henri Sterdyniak

.. _dossier_projet_dangereux_2020_01_13:

=======================================================================================================
La retraite par points: **Un projet de loi dangereux** par Henri Sterdyniak le lundi 13 janvier 2020
=======================================================================================================

.. seealso::

   - https://www.alternatives-economiques.fr/retraites-un-projet-de-loi-dangereux/00091520
   - https://www.alternatives-economiques.fr/users/henri-sterdyniak
   - - https://www.youtube.com/watch?v=rlM1zLW8SUY


Henri Sterdyniak est conseiller scientifique à l'OFCE.

.. contents::
   :depth: 3


Vidéo : MACRON ENTERRE LES RETRAITÉS par Henri Sterdyniak
============================================================

.. seealso::

   - https://www.youtube.com/watch?v=rlM1zLW8SUY
   - https://twitter.com/LeMediaTV

"Malgré l’opposition de la majorité des citoyens, malgré le refus de nombreuses
professions, des éboueurs aux avocats, malgré les grèves les plus importantes
depuis 1968, le gouvernement persiste avec ce qui serait un profond recul de la
démocratie sociale : un passage en force à coup d'ordonnances."


Introduction
===============

Article par article, l’économiste Henri Sterdyniak décrypte ce que contiennent
les projets de loi de la réforme des retraites.

Le projet de loi sur la réforme des retraites a été communiqué aux organismes
de Sécurité sociale, le jeudi 9 juillet.

Samedi, le Premier ministre annonçait son intention de suspendre l’article 56 bis,
l’âge pivot à 64 ans, tout en maintenant le reste du projet.

Même si cette première lecture est nécessairement incomplète et qu’elle est en
outre susceptible d’être précisée sur certains points juridiques, deux points
sont d’ores et déjà frappants.
L’objectif de l’équilibre financier est prégnant alors que la promesse initiale
d’un niveau de retraite satisfaisant, bien que proclamé au départ, ne donne lieu
à aucun engagement précis. Le système des retraites serait étatisé et centralisé.

L’autonomie des régimes complémentaires disparaît complètement.

Le projet marque un recul de la démocratie sociale.

Il existe une forte contradiction entre la manière étatique et centralisée dont
le projet a été conçu, et la proclamation que le système serait ensuite géré par
les partenaires sociaux. Pourquoi ne pas leur avoir donné la main dès le début ?


Les chômeurs et les femmes y perdent
======================================

.. figure:: article_42.png
   :align: center


L’article 42 garantit l’attribution de points pour les périodes de congés
maladie et congés maternité, de chômage.

**Pour les périodes de chômage, les points seront basés sur l’allocation de retour
à l’emploi (ARE) ou l’allocation spécifique de solidarité (ASS) et non plus sur
le salaire antérieur**.

.. warning::
   Il n’y aura plus de points attribués pour les périodes de chômage non indemnisé

C’est un double recul par rapport à la situation actuelle.

L’article 45 prévoit des points gratuits pour les périodes de congés parentaux,
mais celles-ci ne seront validées qu’à 60 % du Smic, ce qui n’incite guère à en prendre.
