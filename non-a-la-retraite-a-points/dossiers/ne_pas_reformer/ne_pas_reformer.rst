.. index::
   pair: 2019-12-03 ; Retraites
   pair: Raveaud ; Gilles
   pair: twitter ; https://twitter.com/RaveaudGilles

.. _dossier_ne_pas_reformer_2019_12_03:

========================================================================================================================
Dossier **Il ne faut pas réformer les retraites** par Gilles Raveaud (alias Jacques Littauer) le mardi 3 décembre 2019
========================================================================================================================

.. seealso::

   - https://charliehebdo.fr/2019/12/economie/dossier-reformes-retraites-macron-intox/
   - https://charliehebdo.fr/2019/11/economie/retraite-deficit-intox-gouvernement-travail/
   - https://twitter.com/RaveaudGilles


.. contents::
   :depth: 3

Introduction
==============


En France, dès qu’il s’agit du système des retraites, on entend un peu tout et
n’importe quoi, surtout quand il est question de réformer ledit système.

Du coup, le camarade Jacques Littauer a eu envie de tirer les choses au clair,
alors qu’un mouvement de contestation de grande ampleur se prépare dans le pays,
avec comme (premier) point d’orgue une grève générale des transports le 5 décembre 2019.

Le système coûte-t-il vraiment trop cher ?
Faut-il absolument repousser l’âge du départ à la retraite ?
Les régimes spéciaux doivent-ils être supprimés au nom de l’égalité républicaine ?

Autant de questions auxquelles vous trouverez les réponses ici, à raison d’un
nouveau billet chaque jour du 25 novembre au 5 décembre.

Bref, aucune excuse pour ne pas être incollable sur la question.

- ÉPISODE 1/10 : `Y a-t-il vraiment un déficit des retraites ?`_
- ÉPISODE 2/10 : `Comment financer les cotisations ?`_
- ÉPISODE 3/10 : `Qui a le meilleure système de retraite au monde ?`_
- ÉPISODE 4/10 : `Le système à points est-il plus juste ?`_
- ÉPISODE 5/10 : `Les retraites à la suédoise, c’est vraiment si bien que ça ?`_
- ÉPISODE 6/10 : `Pourquoi y a-t-il un âge de départ légal ?`_
- ÉPISODE 7/10 : `D’où viennent les régimes spéciaux ?`_
- ÉPISODE 8/10 : `Pourquoi les fonds de pension sont-ils une mauvaise idée ?`_
- ÉPISODE 9/10 : `Les classes moyennes paient-elles pour tout le monde ?`_
- Épisode 10/10 : `Travailler plus pour gagner plus ?`_

.. _`Y a-t-il vraiment un déficit des retraites ?`: https://charliehebdo.fr/2019/11/economie/retraite-deficit-intox-gouvernement-travail/
.. _`Comment financer les cotisations ?`: https://charliehebdo.fr/2019/11/economie/reforme-des-retraites-la-solution-cest-lemploi/
.. _`Qui a le meilleure système de retraite au monde ?`:  https://charliehebdo.fr/2019/11/economie/reformes-des-retraites-est-les-champions-monde/
.. _`Le système à points est-il plus juste ?`:  https://charliehebdo.fr/2019/11/politique/reforme-des-retraites-un-systeme-mauvais-points/
.. _`Les retraites à la suédoise, c’est vraiment si bien que ça ?`:  https://charliehebdo.fr/2019/11/economie/reforme-retraites-suede-macron-pauvrete-taux-remplacement/
.. _`Pourquoi y a-t-il un âge de départ légal ?`: https://charliehebdo.fr/2019/11/economie/retraite-a-60-ans-qui-croit-encore/
.. _`D’où viennent les régimes spéciaux ?`:  https://charliehebdo.fr/2019/12/economie/reforme-retraite-sncf-regimes-speciaux-nest-pas-que-vous-croyez/
.. _`Pourquoi les fonds de pension sont-ils une mauvaise idée ?`: https://charliehebdo.fr/2019/12/economie/reforme-des-retraites-fonds-de-pension-piege-a-retraite/
.. _`Les classes moyennes paient-elles pour tout le monde ?`:  https://charliehebdo.fr/2019/12/economie/reforme-des-retraites-passion-francaise-pour-les-inegalites/
.. _`Travailler plus pour gagner plus ?`: https://charliehebdo.fr/2019/12/economie/reforme-des-retraites-avenir-croissance-emploi-productivite/


Episodes
===========

.. toctree::
   :maxdepth: 3

   9/9
