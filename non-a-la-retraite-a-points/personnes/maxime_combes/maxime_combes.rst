.. index::
   pair: Maxime ; Combes
   ! Maxime Combes

.. _maxim_combes:

=========================================================
Maxime Combes (Economiste, Porte-parole d'@attac_fr)
=========================================================

.. seealso::

   - https://twitter.com/MaximCombes
   - https://twitter.com/attac_fr
