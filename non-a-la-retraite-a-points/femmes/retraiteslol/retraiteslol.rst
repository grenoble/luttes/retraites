.. index::
   pair: Femmes ; grandes gagnantes ? 😂 Lol !

.. _femmes_lol:

===================================================
**Les femmes, grandes gagnantes ? 😂 Lol !**
===================================================

.. seealso::

   - https://laretraite.lol/grandesgagnantes/
   - https://lemouvement.ong/appeldesgrandesgagnantes/


Edouard Philippe l’a dit : les femmes seront les « grandes gagnantes » de la
réforme des retraites. Aucun chiffre, aucune simulation pour le démontrer.

Ayez confiance.

Sauf que nous nous sommes plongées dans sa réforme et qu’il y a un paquet de
problèmes pour les femmes.
La retraite serait désormais calculée en prenant en compte l’ensemble de la
carrière et non plus les meilleures années. Plus possible de neutraliser les
périodes de temps partiel, le chômage ou les interruptions pour prendre en
charge les enfants et les personnes dépendantes.

Les majorations pour enfants seraient une nouvelle fois rognées.

Les pensions de réversions excluraient désormais les couples divorcés et
seraient accessibles seulement une fois à la retraite.

Les professions dans lesquelles les femmes sont concentrées paient un lourd
tribut : plus de reconnaissance de la pénibilité et de départ avant 62 ans
pour les 400 000 aides soignantes, infirmières et sage-femmes, effondrement
de la retraite des 600 000 enseignantes…

Le premier ministre l’a martelé : « il faudra travailler plus longtemps ».

Pourquoi ? Parce que le principe de la réforme est de bloquer le financement
de notre système de retraite à son niveau actuel, 14% du PIB, alors que la
proportion de retraité·es dans la population augmente. Ce plafonnement se
traduira notamment par le retour du risque de pauvreté chez les retraité.e.s
âgé.e.s, en grande majorité des femmes.


.. figure:: regression.png
   :align: center

   https://laretraite.lol/grandesgagnantes/
