.. index::
   pair: Argumentaires; blastinfo

.. _argumentaires_blastinfo:

=================================================================================
**Argumentaires blastinfo**
=================================================================================

- https://www.blast-info.fr/tags/retraites-0FGH9CNsSmqudhXDndwIxg


2023-03-28  retraites : décryptage des arguments d’emmanuel macron
========================================================================

- https://piped.video/UwaIxsBNhrg


C’était l’une des interviews les plus attendues de ces derniers mois, celle
du président de la république, venu défendre coûte que coûte sa réforme
des retraites, en dépit de la colère de la rue.

Dans cet entretien de plus de 30 minutes, il a utilisé de nombreux arguments
économiques pour justifier une réforme dont une grande partie des français
ne veut pas.

La réforme serait nécessaire, indispensable, juste, et de manière générale,
le président de la république se battrait pour plus de justice sociale.

Pour comprendre si ces arguments sont fondés,Salomé Saqué les décrypte
avec Michael Zemmour, l’économiste de référence en ce qui concerne cette réforme.

Article pour comprendre l’usage du mot « usure » et non « pénibilité » :

- https://www.nouvelobs.com/editos-et-chroniques/20230113.OBS68262/retraite-pourquoi-le-gouvernement-prefere-parler-d-usure-plutot-que-de-penibilite.html

- Journaliste: Salome Saqué
- Montage: Alexandre Cassier
- Images: Arthur Frainet
- Son: Baptiste Veilhan
- Graphisme: Adrien Colrat
- Production: Sophie Romillat
- Directeur du développement: Mathias Enthoven
- Rédaction en chef: Soumaya Benaissa
- Directeur de la rédaction: Denis Robert


2023-02-06 **Retraites : Une reforme anti-écologique, un contre sens historique**
====================================================================================

- https://www.blast-info.fr/emissions/2023/retraites-une-reforme-anti-ecologique-un-contre-sens-historique-a-oKXIauQsG-V-KGCmWYPQ

::

    “Pas de retraite, sans planète”

    “le futur sera vert ou ne sera pas”,

    “travailler moins pour polluer moins”.

Voilà le genre de messages que l’on a pu voir dans les manifestations
massives contre la réforme des retraites ces dernières semaines.

La construction d’une réforme des retraites revient en réalité à proposer
une projection dans l’avenir, une vision de la société.

Et une chose est claire, la réforme des retraites promise par le
gouvernement en propose une vision libérale et productiviste.


