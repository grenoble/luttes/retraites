
.. _argumentaire_2023_01_20:

=================================================================================
2023-01-20 **Votre retraite, vous la voulez bien cuite ?**
=================================================================================

- https://bonpote.com/votre-retraite-vous-la-voulez-bien-cuite/


Introduction
==============

La réforme des retraites est en France un sujet de crispation depuis
plusieurs décennies.

Des grèves de 1995 jusqu’à la dernière élection présidentielle 2022, le
sujet alimente les débats chaque année, et est l’une des priorités du
gouvernement Macron.

Depuis plusieurs semaines, les débats font rage sur tous les plateaux TV,
et tout le monde y va de son analyse, en citant notamment “le rapport du COR“.

De gauche comme de droite, ce rapport est brandi comme la Bible et sert
systématiquement à justifier l’idéologie souhaitée.

La meilleure façon de passer outre le cherry-picking étant d’aller à la
source soi-même, on se rend vite compte que ce rapport et ses projections
semblent oublier de légers détails qui vont rythmer le 21e siècle : le changement
climatique et l’effondrement de la biodiversité.


La réforme des retraites est idéologique et non indispensable
===================================================================

Rappelons un point fondamental : le COR ne dit à aucun moment qu’il faut
engager ou non une réforme des retraites.

Tout politique ou éditorialiste qui dit le contraire ment éhontément,
puisque le contraire est marqué dans le rapport :

    Au regard de ces résultats, il ne revient pas au COR de se positionner
    sur le choix du dimensionnement du système de retraite.

    Selon les préférences politiques, il est parfaitement légitime de
    défendre que ces niveaux sont trop ou pas assez élevés, et qu’il
    faut ou non mettre en œuvre une réforme du système de retraite.

Cela diffère très largement des discours politiques que les Français
peuvent entendre depuis des mois (des années !).

Il arrive également que des politiques changent d’avis, à l’instar
d’Olivier Dussopt, actuel ministre du Travail, qui déclarait en 2010 que
“reporter l’âge de départ à la retraite est injuste car cela ne met pas
à contribution les revenus issus du capital“.

Des convictions qui changent selon le sens du vent ?

Ce n’est pas le cas d’Emmanuel Macron, sauf lorsqu’il déclarait en 2019
“qu’il serait hypocrite de décaler l’âge de départ à la retraite“.

Qui aurait pu prédire qu’il change d’avis ?


Bien cuite, votre retraite ?
===================================

Problème systémique, éthique, solidarité intra et intergénérationnelle…

le sujet des retraites rejoint en plusieurs points celui du changement
climatique.

Mais pendant que des politiques s’évertuent à vous expliquer qu’il faut
absolument faire passer des réformes, le changement climatique rappelle
qu’il se contrefiche des contraintes économiques et que la contrainte
physique n’est pas négociable.

L’économie est un jeu dont nous pouvons changer les règles. Pas la physique.

En quittant mon travail il y a deux ans, je précisais que je préférais
passer les 30 prochaines années de ma vie à préparer ma retraite.

Ma crainte est toujours la même deux ans après : devoir vivre une retraite
où le changement climatique aura fondamentalement changé le monde et où
les politiques n’auront toujours pas pris la mesure de la gravité de la
situation.

Qui souhaite une retraite où l’été 2022 devient la norme, où les sécheresses
vont aggraver une situation où nous nous retrouverons avec la moitié des
maisons en France avec des fissures, des chaleurs potentielles à 50°C ?

Se battre contre le changement climatique, c’est aussi se battre pour sa retraite.

Un système économique où la gestion du vieillissement et la réponse à
l’urgence climatique vont de pair reste à construire.

**Mais ce n’est certainement pas grâce au système économique actuel que
nous allons y faire face.**





