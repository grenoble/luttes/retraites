
.. _retraite_deplacer_le_debat:

=================================================================================
2023-01-31 **Retraites : déplacer le débat** par Bernard Friot
=================================================================================

- https://www.ici-grenoble.org/article/des-blocages-aux-flambeaux
- https://lmsi.net/Retraites-deplacer-le-debat


Introduction d’un livre important : L’enjeu des retraites
===============================================================


L’affaire semble entendue et ne pas souffrir de débat : il y a un problème
des retraites.

Dans l’introduction de son indispensable livre, L’enjeu des retraites,
Bernard Friot revient sur la construction même de ce « problème », et
déconstruit la manière très particulière dont il est posé par l’actuel
gouvernement, comme par le précédent, pour imposer des « réformes »
qui s’apparentent à un démantèlement.

Pour y résister, nous relayons également l’appel à manifester ce mardi 31 janvier 2023.


Pourquoi ne sauve-t-on pas les retraites de la même manière qu’on a sauvé les banques ?
==========================================================================================

On vient de sauver les banques en leur donnant de l’argent, beaucoup
d’argent d’ailleurs, tandis que, pour « sauver » les retraites, depuis
vingt ans on ne fait que leur ôter de l’argent, principalement par le
gel du taux des cotisations patronales. N’est-ce pas étrange ?

L’hôpital public aussi, on le sauve à coups de fermetures d’établissements
et de suppressions de postes, de même qu’on « sauve » les emplois de
tant d’entreprises en en supprimant une partie et en réduisant le salaire
des restants.

Sauver par la saignée : Molière nous a appris à nous méfier de ces
dangereux médecins et de leurs prétendus remèdes.

D’autant plus qu’il y a trente ans que cette thérapeutique dure et que
nous voyons bien que ces sauvetages ne sauvent que les actionnaires.

