
.. index::
   pair: Manifestation; mediamanif

.. _manifestation:

=================================================================================
Manifestation
=================================================================================


Mediamanif
===========

- https://www.mediamanif.org/

MediaManif est un média citoyen, une information cartographique des
manifestations et grève en temps-réel.

Chacun peut y signaler les événements dans sa ville :

- manifestants (peu ou très nombreux),
- dangers divers à éviter (incendies, violence...),
- présence de lacrymogène ou autres gaz,
- grève et manifestations

Cette carte permettra ainsi à chacun de manifester en toute sécurité.

Pour utiliser le service, localisez-vous à l'aide de l'icone fleche
regardez ce qu'il se passe autour de vous. cliquez pour en savoir plus
ou confirmer l'information : c'est tout simple !

Pour participer, cliquez sur un emplacement sur la carte pour signaler
un événement.
