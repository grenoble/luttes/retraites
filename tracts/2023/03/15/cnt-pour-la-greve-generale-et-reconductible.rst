.. index::
   pair: CNT; Tract

.. _cnt_tract_greve_generale_2023_03_15:

========================================================================================================================
2023-03-15 Tract CNT **Pour la grève générale et reconductible, notre travail nous appartient notre retraite aussi !**
========================================================================================================================

- https://ul38.cnt-f.org/2023/03/18/jeudi-23-mars-notre-travail-nous-appartient-notre-retraite-aussi/
- https://ul38.cnt-f.org/
- https://www.caisse-solidarite-educ38.org/
- :ref:`faire_greve_2023_03_15`

:download:`Télécharger le tract de la CNT au format PDF <pdfs/tract_cnt_retraites_2023_03_15.pdf>`


NOTRE TRAVAIL NOUS APPARTIENT NOTRE RETRAITE AUSSI !

Nous sommes des millions et nous sommes déterminé·e·s à nous défendre
face à cette nouvelle attaque contre nos droits.
**Les buts visés par la réforme des retraites sont de nous faire travailler
plus longtemps, de réduire et privatiser les pensions de retraite et
d’augmenter la précarité**.

À la CNT, nous pensons que les travailleurs·euses ne peuvent compter que
sur leurs propres forces pour se défendre.
Notre stratégie, c’est l’action directe : agir par nous-mêmes et pour
nous-mêmes, sans intermédiaire ou représentant·e.
Ne déléguons pas notre révolte !
**Dans la situation actuelle, la grève est une stratégie incontournable :
une grève générale et reconductible qui bloque la production**, seule
capable d’instaurer un rapport de force en faveur des travailleurs·euses !

Se mettre en grève reconductible,

- c’est s’inscrire dans une dynamique collective ,étendre la lutte au-delà
  des journées de grève nationale.
- C’est avoir du temps pour s’organiser ensemble et faire converger nos forces.
- C’est revendiquer que notre travail nous appartient.
- C’est faire perdre beaucoup à ceux qui s’enrichissent sur notre dos.
- C’est s’opposer aux actionnaires et aux proﬁ ts qui n’enrichissent que quelques uns.

Se mettre en grève reconductible, c’est rejoindre aujourd’hui un mouvement
qui s’est amorcé il y a déjà plusieurs semaines dans de nombreux secteurs,
l’énergie, l’éducation, les transports, les raffineries...

Pour organiser la solidarité, les caisses de grève permettent de tenir
dans la durée en compensant les pertes de salaire des grévistes.

Certaines sont ouvertes, comme dans l’éducation à Grenoble par exemple
(https://www.caisse-solidarite-educ38.org/), aux non-syndiqué·e·s.

N’hésitez pas à faire une demande d’aide aux caisses de grève, ou à contribuer
si vous le pouvez.

Pour soutenir ou solliciter la caisse de grève éducation 38 : https://www.caisse-solidarite-educ38.org/


Organisons-nous, syndiquons-nous !
===================================

**Il est possible de s’organiser autrement et de gagner**

La CNT est un syndicat indépendant de l’État et des partis politiques.

Un syndicat autogéré sans chefs ni permanent·e·s. Ce sont les syndiqué·e·s
qui décident lors d’assemblées générales souveraines.
La CNT est un syndicat porteur d’un projet de transformation sociale.
Contre la double oppression de l’État et des patrons, elle prône la gestion
directe de la société, par ses membres eux et elles-mêmes !

Les femmes, encore et toujours grandes perdantes de la réforme
=================================================================

L’allongement de la durée de cotisation pénalisera prioritairement les femmes
et les minorisé·e·s de genre.

Pourquoi ?
-------------

Principalement parce que les femmes ont des carrières plus hachées que celles
des hommes : temps partiels imposés, congés parentaux… du fait de l’assignation
sexiste des femmes à l’éducation des enfants et au travail domestique.

Cette réforme est un miroir grossissant des inégalités au cours de la vie
et au travail. Elle reﬂète la conception patriarcale et capitaliste de
notre société.




Se mettre en grève, c’est se donner la possibilité de gagner, de conserver
nos acquis et de construire les bases de futures victoires.
confédération nationale du travail un syndicat de combat,
autogéré et anticapitaliste


::

    www.cnt-f.org/ul38 04 58 00 31 46 ul38@cnt-f.org
    PERMANENCES : mercredis 18h - 19h, 102 rue d’Alembert, Grenoble
    Inscrivez-vous, sur le site, à la NEWSLETTER de la CNT Grenoble
    pour être informé·e des luttes ! Si vous êtes sur les RÉSEAUX SOCIAUX,
    abonnez-vous @cnt_38 à nos comptes !

