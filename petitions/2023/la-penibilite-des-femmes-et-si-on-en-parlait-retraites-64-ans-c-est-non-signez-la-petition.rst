.. index::
   pair: Pétition; Pénibilité dans les métiers féminisés

.. _petitions_femmes_2023:

================================================================================================================================
2023-04-02 **Pénibilité dans les métiers féminisés, si on en parlait ? Retraites à 64 ans c'est non !** #Pas1JourDePlus
================================================================================================================================

- https://www.change.org/p/la-p%C3%A9nibilit%C3%A9-des-femmes-et-si-on-en-parlait-retraites-64-ans-c-est-non-signez-la-p%C3%A9tition


64 ans, c’est non !

#Pas1JourDePlus

Nous, caissières, aides à domicile, assistantes maternelles, salariées
du commerce et de la distribution, nous sommes au côté de vos ainés pour
les aider à mieux vieillir.

Au côté de vos enfants pour les aider à mieux grandir.

Nous vous accompagnons dans votre quotidien. Nos métiers sont essentiels
à notre société…
Dans nos secteurs du commerce et des services, les femmes étaient en première
ligne durant la crise sanitaire.

Rappelez-vous : « {Le pays} tient tout entier sur des femmes et des hommes
que nos économies reconnaissent et rémunèrent si mal »

Nous devrions nous contenter d’un simple merci et travailler 2 ans de plus ?

A chaque réforme des retraites, les femmes son sensées être les grandes
gagnantes alors que les écarts de salaire à travail équivalant sont de
27 % et celui des pensions de base de 40 %.

La pénibilité, et si on en parlait ?

… Salaires de misère, temps partiel subi, carrières hachées, discriminations,
 précarité de l’emploi… les accidents du travail explosent, les maladies
 professionnelles aussi.

Le travail est physiquement et psychologiquement pénible.

Les femmes développent 2 fois plus de TMS que les hommes...

Postures pénibles, mouvements douloureux, port de charges lourdes...

Des horaires atypiques (travail le weekend, la nuit, horaires décalés)..
Et ce cumul des pénibilités n’est même pas reconnu !

On se mobilise pour l’égalité professionnelle et la reconnaissance de la
pénibilité de nos métiers

On se réveille, on fait entendre nos droits en faisant porter nos voix !

Signons la pétition !

