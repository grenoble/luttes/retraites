
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>


.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇺🇦
.. 🇬🇧
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦


.. un·e

.. _luttes_grenoble_retraites:
.. _grenoble_retraites:

=========================================
**Luttes sur les retraites à Grenoble**
=========================================

#Pas1JourDePlus #Retraites #64ansCestToujoursNon #NoBassaran #JeMeSoulève #Grenoble

:ref:`grenoble_infos:agendas_grenoble`


.. toctree::
   :maxdepth: 6

   actions/actions
   evenements/evenements
   tracts/tracts
   argumentaires/argumentaires
   droit/droit
   greves/greves
   petitions/petitions
   occupations/occupations
   manifestation/manifestation
   100joursdezbeul/100joursdezbeul
   non-a-la-retraite-a-points/non-a-la-retraite-a-points

.. toctree::
   :maxdepth: 3

   glossaire/glossaire

