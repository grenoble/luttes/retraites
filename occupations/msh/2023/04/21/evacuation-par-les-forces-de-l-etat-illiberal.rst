

.. _msh_evacuation_2023_04_21:

=================================================================================
2023-04-21 **Evacuation par les forces de l'Etat illibéral**
=================================================================================

- :ref:`communique_msh_2023_04_20`
- https://www.placegrenet.fr/2023/04/21/reforme-des-retraites-la-police-evacue-le-batiment-occupe-sur-le-campus-de-saint-martin-dheres-depuis-deux-jours-par-des-etudiants/600813

La police est intervenue sur le campus de Saint-Martin-d’Hères, ce vendredi 21 avril 2023,
pour évacuer le bâtiment de la Maison des sciences de l’Homme (MSH) occupé
depuis deux jours par des étudiants opposés à la réforme des retraites.

Mercredi 19 avril, une petite centaine de manifestants avaient investi
ces locaux, après l’assemblée générale étudiante organisée devant la
bibliothèque universitaire.

Un important dispositif policier a été mis en place, ce vendredi 21 avril 2023,
sur le campus de l’Université Grenoble Alpes (UGA), à Saint-Martin-d’Hères.

Les forces de l’ordre sont en effet intervenues, peu après midi, pour évacuer
le bâtiment de la Maison des sciences de l’Homme (MSH), occupé depuis
quarante-huit heures par des étudiants mobilisés contre la réforme des retraites.

Les policiers ont ainsi pris position tout autour des locaux pour déloger
rapidement les occupants présents sur les lieux.

En début d’après-midi, l’opération d’évacuation était ainsi terminée,
**malgré l’arrivée de nombreux fourgons de police sur le domaine universitaire,
au grand étonnement des étudiants**.

**D’autres occupations envisagées prochainement sur le campus**

L’occupation du bâtiment de la Maison des sciences de l’Homme a débuté
mercredi 19 avril, à l’issue de l’assemblée générale organisée devant
la bibliothèque universitaire droit et lettres, à quelque pas de là.

Une AG à laquelle ont pris part près de 250 personnes : des étudiants
bien sûr, mais aussi des représentants syndicaux de salariés, des facteurs,
en grève reconductible, ou encore des électriciens d’EDF.

Les participants ont alors décidé de bloquer ou occuper plusieurs bâtiments
universitaires à partir du lundi 24 avril, comme ils le font depuis le
début du mouvement contre la réforme des retraites.

Mais une action, envisagée de longue date, était toutefois prévue ce mercredi.

Dès la fin de l’AG, une petite centaine d’étudiants sont ainsi partis en
cortège pour aller occuper les locaux de la MSH.

**Une occupation qui aura  donc tenu deux jours…**

Avant la prochaine.

