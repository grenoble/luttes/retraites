

.. _communique_msh_2023_04_20:

=================================================================================
2023-04-20 **Occupation de la Maison des Sciences de l'Homme**
=================================================================================

- https://38.demosphere.net/rv/1090


Objectifs
---------------

- Créer un espace de lutte pour organiser la mobilisation étudiante grenobloise,
- informer les étudiant.e.s de la situation en cours,
- organiser des échanges avec des intervenant.e.s,
- proposer un espace d'échange à participation collective.

Les revendications portés par l'occupation:

- Politisation et formation anti-répression accessible à tou.te.s les
  étudiant.e.s (à préciser en AG pendant l'occupation pour les personnes non étudiantes)
- Plus de lieux autogérés sur le campus par les étudiant.e.s
- Lieu ouvert à tou.te.s (à préciser en AG pendant l'occupation pour les
  personnes non étudiantes) pour discuter, participer à la vie du lieu et aux activités
- Éducation sociale, écologique, politique et autogestionnaire
- Un lieu pour s'organiser pour les actions de l'Assemblée générale étudiante H24

Les revendications portés au gouvernement
--------------------------------------------

- La suppression de la réforme des retraites, de la loi Darmanin ainsi
  que la loi Kasbarian Berger.

La nécessité de réelle prise en considération :

- De la précarité étudiante
- Des inégalités et violences raciste sexiste homophobe et transphobe
- De la crise environnementale
- Le respect de la volonté de la majorité de la population française et
  d'une réelle démocratie.

Les revendication portés à la présidence et aux directions de l'université :

- La banalisation des cours ou la levée d'assiduité pour tou.te.s les étudiant.e.s,
  lors des journée de manifestation d'ordre nationale, ainsi que celle organisée
  dans le cas d'une grève reconductible.
- L'interdiction de l'usage du distanciel (appliquée), qui devient un outil
  casseur de grève et de mobilisation.
- Un aménagement des partiels pour ne pas traiter les notions non abordées
  à cause des cours banalisés.
- Un soutien pour appeler à la mobilisation des étudiant.e.s et personnel.le.s via :

Le non retrait des collages d'information et des banderoles (non ostentatoires
et en lien avec la mobilisation).

L'accès aux mails étudiant.e.s pour diffuser des informations de l'avancé
du mouvement et notamment les dates de manifestations et d'AG étudiant.e.s.

Règlement
-------------

Organiser une AG chaque jour (puis suivant les besoins) pour déterminer
collectivement la suite de l'occupation, seul.e les étudiant.es sont
légitimes pour voter les décisions.

- Ne pas dégrader les locaux et le matériel.
- Ne pas consommer de drogue ou d'alcool dans l'enceinte de l'occupation. !VSS!
- Ne pas être dans un état second dans l'enceinte de l'occupation. !VSS!
- Respecter l'organisation des espaces genrées. !VSS!
- Garder l'espace propre.
- Ne pas porter d'arme dans l'enceinte de l'occupation.
- Ne pas user de violences verbales ou physiques envers les occupant.e.s. !VSS!


.. _photos_msh_2023_04_23:

Quelques images du jeudi 20 avril 2023
==========================================

.. figure:: images/msh_occupee.png
   :align: center

.. figure:: images/msh_occupee_2.png
   :align: center

.. figure:: images/msh_occupee_3.png
   :align: center


